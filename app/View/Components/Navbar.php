<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Navbar extends Component
{
    public $offers;
    public $webtypes;
    public $designtypes;
    public $marketingtypes;
    public $courses;
    public $applicationtypes;
    /**
     * Create a new component instance.
     */
    public function __construct($offers, $webtypes,$designtypes,$marketingtypes,$courses,$applicationtypes)
    {
        $this->offers = $offers;
        $this->webtypes = $webtypes;
        $this->designtypes = $designtypes;
        $this->marketingtypes = $marketingtypes;
        $this->courses = $courses;
        $this->applicationtypes = $applicationtypes;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.navbar');
    }
}
