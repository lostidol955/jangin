<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;
    protected $fillable=[
        'name','description','type','slug'
    ];
    public function scopeFilter($query, array $filters)
    {
        if($filters['search'] ?? false)
        {
            $query->where('name','like','%'.request('search').'%')->orWhere('type','like','%'.request('search').'%');
        }
    }
    public function tagline()
    {
        return $this->belongsTo(Tagline::class, 'slug', 'slug');
    }

}
