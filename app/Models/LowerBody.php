<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LowerBody extends Model
{
    use HasFactory;
    protected $fillable=[
        'title', 'description','page_id','slug','image',
     ];
     public function scopeFilter($query, array $filters)
     {
         if($filters['search'] ?? false)
         {
             $query->where('title','like','%'.request('search').'%');
         }
     }
    //Relations
    public function page()
    {
        return $this->belongsTo(Page::class);
    }
}
