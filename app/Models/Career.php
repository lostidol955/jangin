<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
    use HasFactory;
    protected $fillable = ['job_title', 'job_type', 'working_day', 'working_hour', 'image','slug'];

    public function scopeFilter($query, array $filters)
    {
        if($filters['search'] ?? false)
        {
            $query->where('job-title','like','%'.request('search').'%');
        }
    }
    public function career_details(){
        return $this->hasOne(CareerDetails::class);
   }
    public function career_booking(){
        return $this->hasMany(CareerBooking::class);
   }
}
