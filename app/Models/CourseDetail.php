<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseDetail extends Model
{
    use HasFactory;
    protected $fillable=[
         'duration',
         'format',
         'timing',
         'price',
         'emis',
         'learning_hrs',
         'assignment',
         'internship',
         'enrollment',
         'course_id',
         'slug',
     ];

    public function course(){
         return $this->belongsTo(Course::class);
    }
}
