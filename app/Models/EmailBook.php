<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmailBook extends Model
{
    use HasFactory;
    protected $fillable=[
        'email','type'
    ];
    public function scopeFilter($query, array $filters)
    {
        if($filters['search'] ?? false)
        {
            $query->where('email','like','%'.request('search').'%');
        }
    }
}
