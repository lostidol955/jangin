<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;
    protected $fillable=[
        'name','slug'
    ];
     //Relations
     public function seo()
     {
         return $this->hasMany(Page::class);
     }
     public function tagline()
     {
         return $this->hasOne(Tagline::class);
     }
     public function body()
     {
         return $this->hasOne(BodyContent::class);
     }
     public function lower_body()
     {
         return $this->hasOne(BodyContent::class);
     }
}
