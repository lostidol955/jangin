<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Syllabus extends Model
{
    use HasFactory;
    protected $fillable=[
         'file',
         'course_id',
         'slug',
     ];

    public function course(){
         return $this->belongsTo(Course::class);
    }
}
