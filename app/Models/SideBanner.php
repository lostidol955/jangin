<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SideBanner extends Model
{
    use HasFactory;
    protected $fillable=[
        'slug','image',
     ];
     public function scopeFilter($query, array $filters)
     {
         if($filters['search'] ?? false)
         {
             $query->where('slug','like','%'.request('search').'%');
         }
     }

}
