<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Story extends Model
{
    use HasFactory;
    protected $fillable=[
        'title', 'description','slug','image','link'
     ];
     public function scopeFilter($query, array $filters)
     {
         if($filters['search'] ?? false)
         {
             $query->where('title','like','%'.request('search').'%');
         }
     }
}
