<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseContent extends Model
{
    use HasFactory;
    protected $fillable=[
        'learning_points',
        'overview',
        'why_so_popular',
        'outline',
        'practicality',
        'requirements',
        'image',
        'body_image',
        'course_id',
        'slug',
    ];
    public function course(){
        return $this->belongsTo(Course::class);
    }
}
