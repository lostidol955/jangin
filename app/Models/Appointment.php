<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    use HasFactory;
    protected $fillable=[
        'name', 'email','phone','date','description'
     ];
     public function scopeFilter($query, array $filters)
     {
         if($filters['search'] ?? false)
         {
             $query->where('name','like','%'.request('search').'%')->orWhere('email','like','%'.request('search').'%')->orWhere('phone','like','%'.request('search').'%');
         }
     }
}
