<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CareerDetails extends Model
{
    use HasFactory;
    protected $fillable = ['description', 'responsibilities', 'requirements', 'expectation', 'salary','career_id','slug'];
    public function career(){
        return $this->belongsTo(Career::class);
   }
}
