<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CareerBooking extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'email', 'cv','career_id','slug'];
    public function career(){
        return $this->belongsTo(Career::class);
   }
}
