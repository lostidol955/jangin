<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Seo extends Model
{
    use HasFactory;
    protected $fillable=[
        'position','code','page_id'
    ];
    public function scopeFilter($query, array $filters)
    {
        if($filters['search'] ?? false)
        {
            $query->where('position','like','%'.request('search').'%')->orWhere(function($query) use ($filters) {
                $query->whereHas('page', function($query) use ($filters) {
                    $query->where('name', 'like', '%'.$filters['search'].'%');
                });
            });
        }
    }
    //Relations
    public function page()
    {
        return $this->belongsTo(Page::class);
    }
}
