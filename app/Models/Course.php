<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;
    protected $fillable=['name','keywords','price','description','slug'];
    public function scopeFilter($query, array $filters)
    {
        if($filters['search'] ?? false)
        {
            $query->where('name','like','%'.request('search').'%');
        }
    }
    public function offer(){
        return $this->hasOne(Offer::class);
    }
    public function syllabus(){
        return $this->hasOne(Syllabus::class);
    }
    public function booking(){
        return $this->hasMany(Booking::class);
    }
    public function tagline()
    {
        return $this->belongsTo(Tagline::class, 'slug', 'slug');
    }

}
