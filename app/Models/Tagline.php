<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tagline extends Model
{
    use HasFactory;
    protected $fillable=[
        'color','title','image', 'page_id','slug'
     ];
     public function scopeFilter($query, array $filters)
     {
         if($filters['search'] ?? false)
         {
             $query->where('title','like','%'.request('search').'%');
         }
     }
     public function page(){
        return $this->belongsTo(Page::class);
    }
    public function course()
    {
        return $this->hasOne(Course::class, 'slug', 'slug');
    }
    public function service()
    {
        return $this->hasOne(Service::class, 'slug', 'slug');
    }

}
