<?php

namespace App\Http\Controllers;

use App\Models\PrivacyPolicy;
use Illuminate\Http\Request;
use Illuminate\Validate\Rule;
use Illuminate\Support\Str;
use Illuminate\Database\QueryException;

class PrivacyPolicyController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $privacies = PrivacyPolicy::latest()->filter(request(['title','search']))->simplePaginate(5);
        return view('admin.privacy_policy.index',compact('privacies'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.privacy_policy.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $formFields = $request->validate([
                'title'=>'required',
                'description'=>'required',
            ]);
            $slug = Str::slug($request->title);
            PrivacyPolicy::create($formFields+['slug'=>$slug]);
            return redirect('/admin/privacy')->with('message','Privacy policy created successfully');

        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong');
        }

    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request, string $slug)
    {
        $privacy = PrivacyPolicy::where('slug',$slug)->first();
        return view('admin.privacy_policy.details',compact('privacy'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request, string $slug)
    {
        $privacy = PrivacyPolicy::where('slug',$slug)->first();
        return view('admin.privacy_policy.edit',compact('privacy'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $slug)
    {
        try{
            $privacy = PrivacyPolicy::where('slug',$slug)->first();
            $formFields = $request->validate([
                'title'=>'required',
                'description'=>'required',
            ]);
            $slug = Str::slug($request->title);
            $privacy->update($formFields+['slug'=>$slug]);
            return redirect('/admin/privacy')->with('message','Privacy policy updated successfully');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong');
        }

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, string $slug)
    {
        try{
            $privacy = PrivacyPolicy::where('slug',$slug)->first();
            $privacy->delete();
            return redirect('/admin/privacy')->with('message','Privacy policy deleted successfully');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong');
        }

    }
}
