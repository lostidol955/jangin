<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\QueryException;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $galleries = Gallery::latest()->filter(request(['type','search']))->simplePaginate(5);
        return view('admin.gallery.index',compact('galleries'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.gallery.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $formFields = $request->validate([
                'image'=>'required|image|mimes:jpeg,png,jpg,gif',
                'type'=>'required',
            ]);
            $slug = Str::slug($formFields['type']);
            if($request->hasFile('image')){
                $formFields['image'] = $request->file('image')->store('galleries','public');
            }
            Gallery::create($formFields+['slug'=>$slug]);
            return redirect('/admin/gallery')->with('message','Gallery created successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong..');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $gallery = Gallery::whereId($id)->first();
        return view('admin.gallery.details',compact('gallery'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $gallery = Gallery::whereId($id)->first();
        return view('admin.gallery.edit',compact('gallery'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {

        try{
            $gallery = Gallery::whereId($id)->first();
            $formFields = $request->validate([
                'type'=>'required',
                'image'=>'nullable|image|mimes:jpeg,png,jpg,gif',
            ]);
             //if admin select new image
             if($request->hasFile('image')){
                //delete existing image
                $image_path = public_path('storage/'.$gallery->image);
                if(file_exists($image_path)){
                    unlink($image_path);
                  }
                $formFields['image'] = $request->file('image')->store('galleries','public');
                $slug = Str::slug($formFields['type']);
                $gallery->update($formFields+['slug'=>$slug]);
            }
            //if admin didn't select new image
            $slug = Str::slug($formFields['type']);
            $gallery->update($formFields+['slug'=>$slug]);
            return redirect('/admin/gallery')->with('message','Gallery updated successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong..');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $gallery = Gallery::whereId($id)->first();
         //delete existing image
         $image_path = public_path('storage/'.$gallery->image);
         if(file_exists($image_path)){
             unlink($image_path);
           }
        $gallery->delete();
        return redirect('/admin/gallery')->with('message','Gallery deleted successfully');
    }
}
