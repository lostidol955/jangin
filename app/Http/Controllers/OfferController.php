<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use App\Models\Offer;
use App\Models\Course;
use Illuminate\Database\QueryException;


class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $searchQuery = request('search');
        $offers = Offer::whereHas('course', function ($query) use ($searchQuery) {
            $query->where('name', 'like', '%' . $searchQuery . '%');
        })->latest()->simplePaginate(5);
        return view('admin.offers.index',compact('offers'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $courses = Course::all();
        return view('admin.offers.create',compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $formFields = $request->validate(
                [
                    'discount'=>'required',
                    'course_id'=>'required|unique:offers,course_id',
                ],
                [
                    'course_id.unique' => 'Offer for this course is already exists.',
                ]
                );
                $course = Course::whereId($request->course_id)->first();
                $slug = Str::slug($formFields['discount'] . '-' . $course->name);
                Offer::create($formFields+['slug'=>$slug]);
                return redirect('/admin/offer')->with('message','Offer created successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'Offer for this course is already exists');
        }

    }

    /**
     * Display the specified resource.
     */
    public function show(string $slug)
    {
        $offer = Offer::where('slug',$slug)->first();
        return view('admin.offers.details',compact('offer'));

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $slug)
    {
        $offer = Offer::where('slug',$slug)->first();
        $courses = Course::all();
        return view('admin.offers.edit',compact('offer','courses'));

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $slug)
    {
        try{
            $offer = Offer::where('slug',$slug)->first();
            $formFields = $request->validate(
                [
                    'discount'=>'required',
                    'course_id'=>[
                        'required',
                        Rule::unique('offers')->ignore($offer->id),
                    ],
                ], [
                    'course_id.unique' => 'Offer for this course is already exists.',
                ]
            );
            $offer->update($formFields);
            return redirect('/admin/offer')->with('message','Offer updated successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'Offer for this course is already exists');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $slug)
    {
        $offer = Offer::where('slug',$slug)->first();
        $offer->delete();
        return redirect('/admin/offer')->with('message','Offer deleted successfully');
    }
}
