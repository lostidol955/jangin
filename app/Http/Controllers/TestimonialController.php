<?php

namespace App\Http\Controllers;

use App\Models\Testimonial;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Database\QueryException;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $testimonials= Testimonial::latest()->filter(request(['name','search']))->simplePaginate(5);
        return view('admin.testimonial.index',compact('testimonials'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.testimonial.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $formFields = $request->validate([
                'name'=>'nullable',
                'description'=>'required',
                'image'=>'required|image|mimes:jpeg,png,jpg,gif',
            ]
           );
            if($request->hasFile('image')){
              $formFields['image'] = $request->file('image')->store('testimonials','public');
            }
            Testimonial::create($formFields);
            return redirect('/admin/testimonial')->with('message','Testimonial added successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong..');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $testimonial = Testimonial::whereId($id)->first();
        return view('admin.testimonial.details',compact('testimonial'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $testimonial = Testimonial::whereId($id)->first();
        return view('admin.testimonial.edit',compact('testimonial'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try{
            $testimonial = Testimonial::whereId($id)->first();
            $formFields = $request->validate([
                'name'=>'nullable',
                'description'=>'required',
                'image'=>'nullable|image|mimes:jpeg,png,jpg,gif',
            ]
           );
            //if admin select new image
            if($request->hasFile('image')){
                //delete existing image
                $image_path = public_path('storage/'.$testimonial->image);
                if(file_exists($image_path)){
                    unlink($image_path);
                }
                $formFields['image'] = $request->file('image')->store('testimonials','public');
                $testimonial->update($formFields);
            }
            $testimonial->update($formFields);
            return redirect('/admin/testimonial')->with('message','Testimonial updated successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong..');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
       $testimonial = Testimonial::whereId($id)->first();
        //delete existing image
        if(!empty($testimonial->image))
        {
            $image_path = public_path('storage/'.$testimonial->image);
            if(file_exists($image_path)){
                unlink($image_path);
              }
        }
        $testimonial->delete();
        return redirect('/admin/testimonial')->with('message','Testimonial deleted successfully');
    }
}
