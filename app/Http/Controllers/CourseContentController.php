<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CourseContent;
use App\Models\Course;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use Illuminate\Database\QueryException;

class CourseContentController extends Controller
{
     /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $searchQuery = request('search');
        $contents = CourseContent::whereHas('course', function ($query) use ($searchQuery) {
            $query->where('name', 'like', '%' . $searchQuery . '%');
        })->latest()->simplePaginate(5);
        return view('admin.course_contents.index', compact('contents'));

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $courses = Course::all();
        return view('admin.course_contents.create',compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $formFields = $request->validate([
                'learning_points'=>'required',
                'overview'=>'required',
                'why_so_popular'=>'required',
                'outline'=>'required',
                'practicality'=>'required',
                'requirements'=>'required',
                'image' => 'nullable|image|mimes:jpeg,png,jpg,gif',
                'body_image' => 'required|image|mimes:jpeg,png,jpg,gif',
                'course_id'=>'required|unique:course_contents,course_id',
            ]
            ,
            [
                'course_id.unique' => 'The contents for this course is already exists.',
            ]
            );
            if($request->hasFile('image')){
                $formFields['image'] = $request->file('image')->store('tool','public');
            }
            if($request->hasFile('body_image')){
                $formFields['body_image'] = $request->file('body_image')->store('body','public');
            }
            $course = Course::whereId($request->course_id)->first();
            $slug = Str::slug($course->name);
            CourseContent::create($formFields+['slug'=>$slug]);
            return redirect('/admin/contents')->with('message','Contents added successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'The contents for this course is already exists.');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $slug)
    {
        $content = CourseContent::where('slug',$slug)->firstOrFail();
        return view('admin.course_contents.details',compact('content'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $slug)
    {
        $content = CourseContent::where('slug',$slug)->firstOrFail();
        $courses = Course::all();
        return view('admin.course_contents.edit',compact('content','courses'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $slug)
    {
        try{
            $content = CourseContent::where('slug',$slug)->firstOrFail();
            $formFields = $request->validate([
                'learning_points'=>'required',
                'overview'=>'required',
                'why_so_popular'=>'required',
                'outline'=>'required',
                'practicality'=>'required',
                'image' => 'nullable|image|mimes:jpeg,png,jpg,gif',
                'body_image' => 'nullable|image|mimes:jpeg,png,jpg,gif',
                'requirements'=>'required',
                'course_id' => [
                    'required',
                    Rule::unique('course_contents')->ignore($content->id),
                ],
                ], [
                    'course_id.unique' => 'Contents for this course already exists.',
                ]);
             //if admin select new image
             if($request->hasFile('image')){
               // Delete existing image if it exists
                if ($content->image) {
                    $image_path = public_path('storage/' . $content->image);
                    if (file_exists($image_path)) {
                        unlink($image_path);
                    }
                }
                $formFields['image'] = $request->file('image')->store('tool','public');
                $course = Course::whereId($request->course_id)->first();
                $slug = Str::slug($course->name);
                $content->update($formFields+['slug'=>$slug]);
            }
            elseif($request->hasFile('body_image')){
                if ($content->body_image) {
                    $path = public_path('storage/' . $content->body_image);
                    if (file_exists($path)) {
                        unlink($path);
                    }
                }
                $formFields['body_image'] = $request->file('body_image')->store('body','public');
                $course = Course::whereId($request->course_id)->first();
                $slug = Str::slug($course->name);
                $content->update($formFields+['slug'=>$slug]);
            }
            //if admin didn't select new image
            $course = Course::whereId($request->course_id)->first();
            $slug = Str::slug($course->name);
            $content->update($formFields+['slug'=>$slug]);
            return redirect('/admin/contents')->with('message','Course contents updated successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'Contents for this course already exists');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $slug)
    {
        $content = CourseContent::where('slug',$slug)->firstOrFail();
        // Delete existing image
        if(!empty($content->image))
        {
            $imagePath = public_path('storage/' . $content->image);
            if (file_exists($imagePath)) {
                unlink($imagePath);
            }
        }
        // Delete body image
        if(!empty($content->body_image))
        {
            $bodyImagePath = public_path('storage/' . $content->body_image);
            if (file_exists($bodyImagePath)) {
                unlink($bodyImagePath);
            }
        }
        $content->delete();
        return redirect('/admin/contents')->with('message','Course contents deleted successfully');
    }
}
