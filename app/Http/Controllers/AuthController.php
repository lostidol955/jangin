<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function index()
    {
        $accounts = User::latest()->filter(request(['email','search']))->simplePaginate(5);
        return view('admin.accounts.index',compact('accounts'));
    }
    //show login form
    public function login()
    {
        return view('admin.login');
    }
    public function create()
    {
        return view('admin.accounts.create');
    }

    //Account registration
    public function store(Request $request)
    {
        try{
                $formFields = $request->validate([
                    'email'=>'required|unique:users,email',
                    'password'=>'required|confirmed|min:5',
                    'password_confirmation'=>'required',
                ],
                [
                    'email.unique' => 'Account with email is already exists.',
                ]);
                $slug = Str::slug($formFields['email']);
                //To make Hash Password
                $formFields['password'] = Hash::make($request->password);
                //create user
                User::create($formFields+['slug'=>$slug]+['role'=>1]);
                return redirect('/admin/account')->with('message','Account created successfully');
            }
            catch (\Exception $e) {
                return redirect()->back()->with('error', 'Account with email is already exists');
            }
    }
    //Logout user
    public function logout(Request $request)
    {
        auth()->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/admin/login')->with('message','Logged out successfully!');
    }
    //Authenticate user
    public function authenticate(Request $request)
    {
        $formFields = $request->validate([
            'email'=>['required','email'],
            'password'=>'required'
        ]);
        if(auth()->attempt($formFields))
        {
            $request->session()->regenerate();
            return redirect('/admin/course')->with('message','Login successfully');
        }
        return back()->with('error','Invalid credentials')->onlyInput('email');

    }
    public function show(string $slug)
    {
        $account = User::where('slug',$slug)->first();
        return view('admin.accounts.details',compact('account'));
    }
    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $slug)
    {
        $account = User::where('slug',$slug)->first();
        return view('admin.accounts.edit',compact('account'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $slug)
    {
        try{
            $account = User::where('slug',$slug)->first();
            $formFields = $request->validate([
                'email'=> ['required',Rule::unique('users')->ignore($account->id),]
            ], [
                'email.unique' => 'Account with email is already exists.',
            ]);
            return redirect('/admin/body_content')->with('message','Account updated successfully');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Account with email is already exists');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $slug)
    {
        $account = User::where('slug', $slug)->where('role', 1)->first();
        if (!$account) {
            return redirect('/admin/account')->with('error', 'Unable to delete this account');
        }
        $account->delete();
        return redirect('/admin/account')->with('message', 'User deleted successfully');
    }


}
