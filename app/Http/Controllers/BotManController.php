<?php

namespace App\Http\Controllers;

use BotMan\BotMan\BotMan;
use Illuminate\Http\Request;
use BotMan\BotMan\Messages\Incoming\Answer;
use App\Models\Course;

class BotManController extends Controller
{
    public function handle(){
        $botman = app('botman');
        $botman->hears('Hi|Hello', function (BotMan $bot) {
            $bot->reply('Hello! What can I do for you?');
        });

        $botman->hears('.*course.*', function (BotMan $bot) {
            $courses = Course::pluck('name')->toArray();
            if (count($courses) > 0) {
                $bot->reply('Here are the available courses:');
                $bot->reply(implode(', ', $courses));
                $bot->reply('You can simply book your seat by visiting the individual course page.');
            } else {
                $bot->reply('There are no courses available at the moment.');
            }
        });
        $botman->listen();
    }


}
