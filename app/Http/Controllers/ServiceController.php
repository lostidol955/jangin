<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Service;
use App\Models\Page;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use Illuminate\Database\QueryException;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $services = Service::latest()->filter(request(['name','search']))->simplePaginate(5);
        return view('admin.service_type.index',compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.service_type.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        try{
            $formFields = $request->validate([
                'name'=>'required|unique:services,name',
                'description'=>'required',
                'type'=>'required'
            ] ,
            [
                'name.unique' => 'This service is already exists',
            ]);
            $slug = Str::slug($formFields['name']);
            Service::create($formFields+['slug'=>$slug]);
             //Store name to page table
             Page::create([
                'name' => $formFields['name'],
                'slug' => $slug,
            ]);
            return redirect('/admin/services')->with('message','Service created successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'Service with this name already exists.');
        }

    }

    /**
     * Display the specified resource.
     */
    public function show(string $slug)
    {
        $service = Service::where('slug',$slug)->first();
        return view('admin.service_type.details',compact('service'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $slug)
    {
        $service = Service::where('slug',$slug)->first();
        return view('admin.service_type.edit',compact('service'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $slug)
    {

        try{
            $service = Service::where('slug',$slug)->first();
            $formFields = $request->validate([
                'description'=>'required',
                'type'=>'required',
                'name' => [
                    'required',
                    Rule::unique('services')->ignore($service->id),
                ],
            ], [
                'name.unique' => 'Service with this name already exists.',
            ]
            );
            $slug = Str::slug($formFields['name']);
            $service->update($formFields+['slug'=>$slug]);
            return redirect('/admin/services')->with('message','Service updated successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'Service with this name already exists.');
        }

    }
    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $slug)
    {
        $service = Service::where('slug',$slug)->first();
        $service->delete();
        $page = Page::where('slug',$slug)->first();
        $page->delete();
        return redirect('/admin/services')->with('message','Service deleted successfully');
    }
}
