<?php

namespace App\Http\Controllers;

use App\Models\EmailBook;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Database\QueryException;

class EmailBookController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $emails = EmailBook::latest()->filter(request(['title','search']))->simplePaginate(5);
        return view('admin.emailbooks.index',compact('emails'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.emailbooks.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $formFields = $request->validate([
                'email'=>'required|unique:email_books,email',
                'type'=>'required',
            ] ,
            [
                'email.unique' => 'This email is already exists',
            ]);
            EmailBook::create($formFields);
            return redirect('/admin/email')->with('message','Email added successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'This email is already exists');
        }

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $email = EmailBook::whereId($id)->first();
        return view('admin.emailbooks.details',compact('email'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $email = EmailBook::whereId($id)->first();
        return view('admin.emailbooks.edit',compact('email'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try{
            $email = EmailBook::whereId($id)->first();
            $formFields = $request->validate([
                'type'=>'required',
                'email' => [
                    'required',
                    Rule::unique('email_books')->ignore($email->id),
                ],
            ], [
                'email.unique' => 'This email is already exists.',
            ]);
            $email->update($formFields);
            return redirect('/admin/email')->with('message','Email updated successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'This email is already exists');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $email = EmailBook::whereId($id)->first();
        $email->delete();
        return redirect('/admin/email')->with('message','Email deleted successfully');
    }
}
