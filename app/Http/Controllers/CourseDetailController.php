<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CourseDetail;
use App\Models\Course;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use Illuminate\Database\QueryException;

class CourseDetailController extends Controller
{

     /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $course_details = CourseDetail::latest()->simplePaginate(5);
        return view('admin.course_details.index',compact('course_details'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $courses = Course::all();
        return view('admin.course_details.create',compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            $formFields = $request->validate([
                'duration' => 'required',
                'format' => 'required',
                'timing' => 'required',
                'price' => 'required',
                'emis' => 'required',
                'learning_hrs' => 'required',
                'assignment' => 'required',
                'internship' => 'required',
                'enrollment' => 'required',
                'course_id' => 'required|unique:course_details,course_id',
            ],
            [
                'course_id.unique' => 'Details for this course already exists',
            ]);
            $course = Course::whereId($request->course_id)->first();
            $slug = Str::slug($course->name);
            CourseDetail::create($formFields+['slug'=>$slug]);
            return redirect('/admin/details')->with('message','Details added successfully');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Details for this course already exists');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $slug)
    {

        $detail = CourseDetail::where('slug',$slug)->firstOrFail();
        return view('admin.course_details.details',compact('detail'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $slug)
    {
        $courses = Course::all();
        $detail = CourseDetail::where('slug',$slug)->firstOrFail();
        return view('admin.course_details.edit',compact('courses','detail'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $slug)
    {
        try
        {
            $detail = CourseDetail::where('slug',$slug)->firstOrFail();
            $formFields = $request->validate([
                'duration' => 'required',
                'format' => 'required',
                'timing' => 'required',
                'price' => 'required',
                'emis' => 'required',
                'learning_hrs' => 'required',
                'assignment' => 'required',
                'internship' => 'required',
                'enrollment' => 'required',
                'course_id' => [
                    'required',
                    Rule::unique('course_details')->ignore($detail->id),
                ],
            ], [
                'course_id.unique' => 'Details for this course already exists.',
            ]);
            $course = Course::whereId($request->course_id)->first();
            $slug = Str::slug($course->name);
            $detail->update($formFields+['slug'=>$slug]);
        return redirect('/admin/details')->with('message','Details updated successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'Details for this course already exists');
        }

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $slug)
    {
        $detail = CourseDetail::where('slug',$slug)->firstOrFail();
        $detail->delete();
        return redirect('/admin/details')->with('message','Details deleted successfully');
    }
}
