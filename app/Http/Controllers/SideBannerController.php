<?php

namespace App\Http\Controllers;

use App\Models\SideBanner;
use Illuminate\Http\Request;
use App\Models\Course;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use Illuminate\Database\QueryException;


class SideBannerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $banners = SideBanner::latest()->filter(request(['title','search']))->simplePaginate(5);
        return view('admin.side_banner.index',compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $courses = Course::all();
        return view('admin.side_banner.create',compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $formFields = $request->validate([
                'image'=>'required|image',
                'slug'=>'required|unique:side_banners,slug',
            ],
            [
                'slug.unique' => 'Banner for this page already exists.',
            ]
           );
            if($request->hasFile('image')){
              $formFields['image'] = $request->file('image')->store('side_banners','public');
            }
           SideBanner::create($formFields);
           return redirect('/admin/side_banner')->with('message','Banner added successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'Banner for this page already exists');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $slug)
    {
        $banner = SideBanner::where('slug',$slug)->first();
        return view('admin.side_banner.details',compact('banner'));
    }

    public function edit(string $slug)
    {
        $banner = SideBanner::where('slug',$slug)->first();
        $pages = Course::all();
        return view('admin.side_banner.edit',compact('banner','pages'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $slug)
    {
        try{
            $banner = SideBanner::where('slug',$slug)->first();
            $formFields = $request->validate([
                 'image'=>'nullable|image|mimes:jpeg,png,jpg,gif',
                 'slug' => [
                     'required',
                     Rule::unique('side_banners')->ignore($banner->id),
                 ],
             ], [
                 'slug.unique' => 'Banner for this page already exists.',
             ]);
             //if admin select new image
             if($request->hasFile('image')){
                     //delete existing image
                     $image_path = public_path('storage/'.$banner->image);
                     if(file_exists($image_path)){
                         unlink($image_path);
                     }
                     $formFields['image'] = $request->file('image')->store('side_banners','public');
                     $banner->update($formFields);
                 }
             //if admin didn't select new image
             $banner->update($formFields);
             return redirect('/admin/side_banner')->with('message','Banner updated successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'Banner for this page already exists');
        }

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $slug)
    {
        $banner = SideBanner::where('slug',$slug)->first();
        //delete existing image
        $image_path = public_path('storage/'.$banner->image);
        if(file_exists($image_path)){
            unlink($image_path);
          }
        $banner->delete();
        return redirect('/admin/side_banner')->with('message','Banner deleted successfully');
    }
}
