<?php

namespace App\Http\Controllers;

use App\Models\CareerDetails;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Models\Career;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

class CareerDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $searchQuery = request('search');
        $career_details = CareerDetails::whereHas('career', function ($query) use ($searchQuery) {
            $query->where('job_title', 'like', '%' . $searchQuery . '%');
        })->latest()->simplePaginate(5);
        return view('admin.career_details.index',compact('career_details'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $careers = Career::all();
        return view('admin.career_details.create',compact('careers'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $formFields = $request->validate([
                'description'=>'required',
                'responsibilities'=>'required',
                'requirements'=>'required',
                'expectation'=>'required',
                'salary'=>'required',
                'career_id'=>'required|unique:career_details,career_id',
            ],
            [
                'career_id.unique' => 'Details for this job title already exists.',
            ]
        );
            $career = Career::where('id',$request->career_id)->first();
            $slug = Str::slug($career->job_title);
            CareerDetails::create($formFields+['slug'=>$slug]);
            return redirect('/admin/career_details')->with('message','Content added successfully');
        } catch (\Exception $e) {
                return redirect()->back()->with('error', 'Details for this job title already exists');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $slug)
    {
        $details = CareerDetails::where('slug',$slug)->first();
        return view('admin.career_details.details',compact('details'));
    }
    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $slug)
    {
        $details = CareerDetails::where('slug',$slug)->first();
        $careers = Career::all();
        return view('admin.career_details.edit',compact('details','careers'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $slug)
    {
        try{
            $details = CareerDetails::where('slug',$slug)->first();
            $formFields = $request->validate([
                    'description'=>'required',
                    'responsibilities'=>'required',
                    'requirements'=>'required',
                    'expectation'=>'required',
                    'salary'=>'required',
                    'career_id' => [
                        'required',
                        Rule::unique('career_details')->ignore($details->id),
                    ],
                ], [
                    'career_id.unique' => 'Details for this job title already exists.',
                ]);
                $career = Career::where('id',$request->career_id)->first();
                $slug = Str::slug($career->job_title);
                $details->update($formFields+['slug'=>$slug]);
                return redirect('/admin/career_details')->with('message','Details updated successfully');
        } catch (\Exception $e) {
                return redirect()->back()->with('error', 'Details for this job title already exists');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $slug)
    {
        $details = CareerDetails::where('slug',$slug)->first();
        $details->delete();
        return redirect('/admin/career_details')->with('message','Details deleted successfully');
    }
}
