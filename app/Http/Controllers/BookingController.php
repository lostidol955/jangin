<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Database\QueryException;


class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $bookings = Booking::latest()->filter(request(['name','search']))->simplePaginate(5);
        return view('admin.bookings.index',compact('bookings'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $courses = Course::all();
        return view('admin.bookings.create',compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $formFields = $request->validate([
                'name' => 'required',
                'email' => 'required|unique:bookings,email,NULL,id,course_id,' . $request->input('course_id'),
                'phone' => 'required',
                'address' => 'required',
                'qualification' => 'required',
                'date' => 'required',
                'time' => 'required',
                'course_id' => 'required',
            ], [
                'email.unique' => 'Booking for this email is already exists.',
            ]);
            $slug = Str::slug($formFields['email'].$formFields['course_id']);
            Booking::create($formFields+['slug'=>$slug]);
            return redirect('/admin/bookings')->with('message','Booking created successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'Booking for this email is already exists');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $slug)
    {
        $booking = Booking::where('slug',$slug)->firstOrFail();
        return view('admin.bookings.details', compact('booking'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $slug)
    {
        $courses = Course::all();
        $booking = Booking::where('slug',$slug)->firstOrFail();
        return view('admin.bookings.edit', compact('booking','courses'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $slug)
    {
        try{
            $booking = Booking::where('slug',$slug)->firstOrFail();
            $formFields = $request->validate([
                'name' => 'required',
                'email' => 'required',
                'phone' => 'required',
                'address' => 'required',
                'qualification' => 'required',
                'date' => 'required',
                'time' => 'required',
                'course_id' => 'required',
            ], [
                'email.unique' => 'Booking for this email is already exists.',
            ]);

            $slug = Str::slug($formFields['email'].$formFields['course_id']);
            $booking->update($formFields+['slug'=>$slug]);
            return redirect('/admin/bookings')->with('message','Booking updated successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'Booking for this email is already exists');
        }

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $slug)
    {
        $booking = Booking::where('slug',$slug)->firstOrFail();
        $booking->delete();
        return redirect('/admin/bookings')->with('message','Booking deleted successfully');
    }
}
