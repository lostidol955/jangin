<?php

namespace App\Http\Controllers;

use App\Models\Career;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class CareerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $careers = Career::latest()->filter(request(['title','search']))->simplePaginate(5);
        return view('admin.career.index',compact('careers'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.career.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $formFields = $request->validate([
                'job_title'=>'required|unique:careers,job_title',
                'job_type'=>'required',
                'working_day'=>'required',
                'working_hour'=>'required',
                'image'=>'required|image|mimes:jpeg,png,jpg,gif',
            ] ,
            [
                'job_title.unique' => 'This job title is already exists',
            ]
        );
            $slug = Str::slug($formFields['job_title']);
            if($request->hasFile('image')){
                $formFields['image'] = $request->file('image')->store('career','public');
            }
            Career::create($formFields+['slug'=>$slug]);
            return redirect('/admin/career')->with('message','Career added successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'This job title is already exists');
        }

    }

    /**
     * Display the specified resource.
     */
    public function show(string $slug)
    {
        $career = Career::where('slug',$slug)->firstOrFail();
        return view('admin.career.details',compact('career'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $slug)
    {
        $career = Career::where('slug',$slug)->firstOrFail();
        return view('admin.career.edit',compact('career'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $slug)
    {
        try{
            $career = Career::where('slug',$slug)->firstOrFail();
            $formFields = $request->validate([
                'job_type'=>'required',
                'working_day'=>'required',
                'working_hour'=>'required',
                'image'=>'nullable|image|mimes:jpeg,png,jpg,gif',
                'job_title' => [
                    'required',
                    Rule::unique('careers')->ignore($career->id),
                ],
            ], [
                'job_title.unique' => 'Career with this title already exists.',
            ]
        );
             //if admin select new image
             if($request->hasFile('image')){
                //delete existing image
                $image_path = public_path('storage/'.$career->image);
                if(file_exists($image_path)){
                    unlink($image_path);
                  }
                $formFields['image'] = $request->file('image')->store('career','public');
                $slug = Str::slug($formFields['job_title']);
                $career->update($formFields+['slug'=>$slug]);
            }
            //if admin didn't select new image
            $slug = Str::slug($formFields['job_title']);
            $career->update($formFields+['slug'=>$slug]);
            return redirect('/admin/career')->with('message','Career updated successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'Career with this title already exists');
        }

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $slug)
    {
        $career= Career::where('slug',$slug)->firstOrFail();
         //delete existing image
         $image_path = public_path('storage/'.$career->image);
         if(file_exists($image_path)){
             unlink($image_path);
           }
        $career->delete();
        return redirect('/admin/career')->with('message','Career deleted successfully');
    }
}
