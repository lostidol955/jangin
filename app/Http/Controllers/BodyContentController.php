<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use App\Models\BodyContent;
use Illuminate\Http\Request;
use App\Models\Page;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

class BodyContentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $contents = BodyContent::latest()->filter(request(['title','search']))->simplePaginate(5);
        return view('admin.body_content.index',compact('contents'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $pages = Page::all();
        return view('admin.body_content.create',compact('pages'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $formFields = $request->validate([
                'title'=>'required',
                'description'=>'required',
                'image'=>'nullable|image|mimes:jpeg,png,jpg,gif',
                'page_id'=>'required|unique:body_contents,page_id',
            ],
            [
                'course_id.unique' => 'Body content for this page already exists.',
            ]
        );
            if($request->hasFile('image')){
            $formFields['image'] = $request->file('image')->store('body','public');
            }
            $page = Page::where('id',$request->page_id)->first();
            $slug = Str::slug($page->name);
            BodyContent::create($formFields+['slug'=>$slug]);
            return redirect('/admin/body_content')->with('message','Content added successfully');
        } catch (\Exception $e) {
                return redirect()->back()->with('error', 'Body content for this page already exists');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $slug)
    {
        $content = BodyContent::where('slug',$slug)->first();
        return view('admin.body_content.details',compact('content'));
    }
    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $slug)
    {
        $content = BodyContent::where('slug',$slug)->first();
        $pages = Page::all();
        return view('admin.body_content.edit',compact('content','pages'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $slug)
    {
        try{
            $content = BodyContent::where('slug',$slug)->first();
            $formFields = $request->validate([
                    'title'=>'required',
                    'description'=>'required',
                    'image'=>'nullable|image|mimes:jpeg,png,jpg,gif',
                    'page_id' => [
                        'required',
                        Rule::unique('body_contents')->ignore($content->id),
                    ],
                ], [
                    'page_id.unique' => 'Body content for this page already exists.',
                ]);
                //if admin select new image
                if($request->hasFile('image')){
                        //delete existing image
                        $image_path = public_path('storage/'.$content->image);
                        if(file_exists($image_path)){
                            unlink($image_path);
                        }
                        $formFields['image'] = $request->file('image')->store('body','public');
                        $page = Page::where('id',$request->page_id)->first();
                        $slug = Str::slug($page->name);
                        $content->update($formFields+['slug'=>$slug]);
                    }
                //if admin didn't select new image
                $page = Page::where('id',$request->page_id)->first();
                $slug = Str::slug($page->name);
                $content->update($formFields+['slug'=>$slug]);
                return redirect('/admin/body_content')->with('message','Content updated successfully');
        } catch (\Exception $e) {
                return redirect()->back()->with('error', 'Body content for this page already exists');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $slug)
    {
        $content = BodyContent::where('slug',$slug)->first();
        //delete existing image
        $image_path = public_path('storage/'.$content->image);
        if(file_exists($image_path)){
            unlink($image_path);
          }
        $content->delete();
        return redirect('/admin/body_content')->with('message','Content deleted successfully');
    }
}
