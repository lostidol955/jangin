<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Database\QueryException;
use App\Models\Page;
use App\Models\Seo;


class SeoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $seos = Seo::latest()->filter(request(['position','search']))->simplePaginate(5);
        return view('admin.seo.index',compact('seos'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $pages = Page::all();
        return view('admin.seo.create',compact('pages'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $formFields = $request->validate([
                'position'=>'required',
                'code'=>'required',
                'page_id'=>'required',
           ]);
           Seo::create($formFields);
            return redirect('/admin/seo')->with('message','SEO added successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong..');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $seo = Seo::whereId($id)->first();
        return view('admin.seo.details',compact('seo'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $pages = Page::all();
        $seo = Seo::whereId($id)->first();
        return view('admin.seo.edit',compact('seo','pages'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {

        try{
            $seo = Seo::whereId($id)->first();
            $formFields = $request->validate([
                'position'=>'required',
                'code'=>'required',
                'page_id'=>'required',
           ]);
           $seo->update($formFields);
            return redirect('/admin/seo')->with('message','SEO updated successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong..');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $seo = Seo::whereId($id)->first();
        $seo->delete();
        return redirect('/admin/seo')->with('message','SEO deleted successfully');
    }
}
