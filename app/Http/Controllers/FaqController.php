<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validate\Rule;
use App\Models\Faq;
use Illuminate\Support\Str;
use Illuminate\Database\QueryException;


class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $faqs = Faq::latest()->filter(request(['title','search']))->simplePaginate(5);
        return view('admin.faq.index',compact('faqs'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.faq.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $formFields = $request->validate([
                'title'=>'required',
                'description'=>'required',
           ]);
           $slug =Str::slug($formFields['title']);
           Faq::create($formFields+['slug'=>$slug]);
            return redirect('/admin/faq')->with('message','Faq added successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong..');
        }

    }

    /**
     * Display the specified resource.
     */
    public function show(string $slug)
    {
        $faq = Faq::where('slug',$slug)->firstOrFail();
        return view('admin.faq.details',compact('faq'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $slug)
    {
        $faq = Faq::where('slug',$slug)->firstOrFail();
        return view('admin.faq.edit',compact('faq'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $slug)
    {
        try{
            $faq = Faq::where('slug',$slug)->firstOrFail();
            $formFields = $request->validate([
                'title'=>'required',
                'description'=>'required',
           ]);
            $slug =Str::slug($formFields['title']);
            $faq->update($formFields+['slug'=>$slug]);
            return redirect('/admin/faq')->with('message','Faq updated successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong..');
        }

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $slug)
    {
        $faq = Faq::where('slug',$slug)->firstOrFail();
        $faq->delete();
        return redirect('/admin/faq')->with('message','Faq deleted successfully');
    }
}
