<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Syllabus;
use App\Models\Course;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Database\QueryException;


class SyllabusController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $syllabus = Syllabus::latest()->simplePaginate(5);
        return view('admin.syllabus.index',compact('syllabus'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $courses = Course::all();
        return view('admin.syllabus.create',compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $formFields = $request->validate([
                'course_id'=>'required|unique:syllabi,course_id',
                'file'=>'required|file|mimes:pdf,ppt,pptx',
            ],
            [
                'course_id.unique' => 'Syllabus for this course already exists.',
            ]);
            $course = Course::whereId($request->course_id)->first();
            $slug = Str::slug($course->name);
            if($request->hasFile('file')){
                $formFields['file'] = $request->file('file')->store('syllabus','public');
            }
            Syllabus::create($formFields+['slug'=>$slug]);
            return redirect('/admin/syllabus')->with('message','Syllabus added successfully');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Syllabus for this course already exists');
        }

    }

    /**
     * Display the specified resource.
     */
    public function show($slug)
    {
        $courses = Course::all();
        $syllabus = Syllabus::where('slug',$slug)->first();
        return view('admin.syllabus.details',compact('courses','syllabus'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit( $slug)
    {
        $courses = Course::all();
        $syllabus = Syllabus::where('slug',$slug)->first();
        return view('admin.syllabus.edit',compact('courses','syllabus'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request,$slug)
    {
        try
        {
            $syllabus = Syllabus::where('slug',$slug)->first();
            $formFields = $request->validate([
                    'file'=>'nullable|file|mimes:pdf,ppt,pptx',
                    'course_id' => [
                        'required',
                        Rule::unique('syllabi')->ignore($syllabus->id),
                    ],
                ], [
                    'course_id.unique' => 'Syllabus for this course already exists.',
                ]
            );
             //if admin select new file
             if($request->hasFile('file')){
                //delete existing file
                $file_path = public_path('storage/'.$syllabus->file);
                if(file_exists($file_path)){
                    unlink($file_path);
                  }
                $formFields['file'] = $request->file('file')->store('syllabus','public');
                $slug = Str::slug($formFields['title']);
                $syllabus->update($formFields+['slug'=>$slug]);
            }
            //if admin didn't select new image
            $course = Course::whereId($request->course_id)->first();
            $slug = Str::slug($course->name);
            $syllabus->update($formFields+['slug'=>$slug]);
            return redirect('/admin/syllabus')->with('message','Syllabus added successfully');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Syllabus for this course already exists');
        }

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($slug)
    {
        $syllabus = Syllabus::where('slug',$slug)->first();
         //delete existing file
         $file_path = public_path('storage/'.$syllabus->file);
         if(file_exists($file_path)){
             unlink($file_path);
           }
        $syllabus->delete();
        return redirect('/admin/syllabus')->with('message','Syllabus deleted successfully');
    }
}
