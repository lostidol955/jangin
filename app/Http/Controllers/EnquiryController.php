<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\Enquiry;
use Illuminate\Database\QueryException;

class EnquiryController extends Controller
{

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $enquiries= Enquiry::latest()->filter(request(['name','search']))->simplePaginate(5);
        return view('admin.enquiry.index',compact('enquiries'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.enquiry.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $formFields = $request->validate([
                'name'=>'required',
                'email'=>'required',
                'phone'=>'required',
                'type'=>'required',
                'description'=>'required',
            ]);
            Enquiry::create($formFields);
            return redirect('/admin/enquiry')->with('message','Enquiry created successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong..');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $enquiry = Enquiry::whereId($id)->firstOrFail();
        return view('admin.enquiry.details',compact('enquiry'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $enquiry = Enquiry::whereId($id)->firstOrFail();
        return view('admin.enquiry.edit',compact('enquiry'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try{
            $enquiry = Enquiry::whereId($id)->firstOrFail();
            $formFields = $request->validate([
                'name'=>'required',
                'email'=>'required',
                'phone'=>'required',
                'type'=>'required',
                'description'=>'required',
            ]);
            $enquiry->update($formFields);
            return redirect('/admin/enquiry')->with('message','Enquiry updated successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong..');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $enquiry = Enquiry::whereId($id)->firstOrFail();
        $enquiry->delete();
        return redirect('/admin/enquiry')->with('message','Enquiry deleted successfully');
    }
}
