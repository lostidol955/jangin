<?php

namespace App\Http\Controllers;

use App\Models\CareerBooking;
use Illuminate\Http\Request;

class CareerBookingController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $searchQuery = request('search');
        $bookings = CareerBooking::whereHas('career', function ($query) use ($searchQuery) {
            $query->where('job_title', 'like', '%' . $searchQuery . '%')->orWhere('name', 'like', '%' . $searchQuery . '%')->orWhere('email', 'like', '%' . $searchQuery . '%');
        })->latest()->simplePaginate(5);
        return view('admin.career_bookings.index',compact('bookings'));
    }


    /**
     * Display the specified resource.
     */

    public function show(string $slug)
    {
        $booking = CareerBooking::where('slug',$slug)->first();
        return view('admin.career_bookings.details',compact('booking'));
    }
    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $slug)
    {
        $booking = CareerBooking::where('slug',$slug)->firstOrFail();
        // Delete existing image
        if(!empty($booking->cv))
        {
            $fileath = public_path('storage/' . $booking->cv);
            if (file_exists($fileath)) {
                unlink($fileath);
            }
        }
        // Delete body image
        if(!empty($booking->additional_file))
        {
            $bodyImagePath = public_path('storage/' . $booking->additional_file);
            if (file_exists($bodyImagePath)) {
                unlink($bodyImagePath);
            }
        }
        $booking->delete();
        return redirect('/admin/career_booking')->with('message','Career booking deleted successfully');
    }
}
