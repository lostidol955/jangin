<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CourseLearning;
use App\Models\Course;
use Illuminate\Validation\Rule;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;

class CourseLearningController extends Controller
{
/**
     * Display a listing of the resource.
     */
    public function index()
    {
        $learnings = CourseLearning::latest()->simplePaginate(5);
        return view('admin.course_learnings.index',compact('learnings'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $courses = Course::all();
        return view('admin.course_learnings.create',compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $formFields = $request->validate([
                    'description' => 'required',
                    'course_id' => 'required|unique:course_learnings,course_id',
                ],
                [
                    'course_id.unique' => 'The learning contents for this course already exists',
                ]
            );
            $course = Course::whereId($request->course_id)->first();
            $slug = Str::slug($course->name);
            CourseLearning::create($formFields + ['slug' => $slug]);
            return redirect('/admin/learnings')->with('message', 'Learning contents added successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'Learning contents for this course already exists');
        }

    }

    /**
     * Display the specified resource.
     */
    public function show(string $slug)
    {
        $learning = CourseLearning::where('slug',$slug)->firstOrFail();
        return view('admin.course_learnings.details',compact('learning'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $slug)
    {
        $learning = CourseLearning::where('slug',$slug)->firstOrFail();
        $courses = Course::all();
        return view('admin.course_learnings.edit',compact('learning','courses'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $slug)
    {
        try{
            $learning = CourseLearning::where('slug',$slug)->firstOrFail();
            $formFields = $request->validate([
                'description' => 'required',
                'course_id' => [
                    'required',
                    Rule::unique('course_learnings')->ignore($learning->id),
                ],
            ], [
                'course_id.unique' => 'Learning contents for this course already exists.',
            ]);
            $learning->update($formFields);
            return redirect('/admin/learnings')->with('message','Learning contents updated successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'Learning contents for this course already exists');
        }


    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $slug)
    {
        $learning = CourseLearning::where('slug',$slug)->firstOrFail();
        $learning->delete();
        return redirect('/admin/learnings')->with('message','Learning contents deleted successfully');
    }
}
