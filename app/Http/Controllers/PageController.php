<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Models\Course;
use App\Models\Enquiry;
use App\Models\EmailBook;
use App\Models\Seo;
use App\Models\Faq;
use App\Models\Story;
use App\Models\Service;
use App\Models\Career;
use App\Models\CareerDetails;
use App\Models\CareerBooking;
use App\Models\Offer;
use App\Models\Tagline;
use App\Models\Page;
use App\Models\CourseContent;
use App\Models\ContactEmail;
use App\Models\CourseDetail;
use App\Models\CourseLearning;
use App\Models\Syllabus;
use App\Models\ContactInfo;
use App\Models\Gallery;
use App\Models\BodyContent;
use App\Models\LowerBody;
use App\Models\SideBanner;
use App\Models\Booking;
use App\Models\Appointment;
use App\Models\PrivacyPolicy;
use App\Models\Testimonial;
use Spatie\Newsletter\Facades\Newsletter;

class PageController extends Controller
{
    private function getDataForView()
    {
        $courses = Course::all();
        $faqs = Faq::all();
        $navs = Service::all();
        $offers = Offer::all();
        $stories = Story::all();
        $contactInfo = ContactInfo::first();
        //To display body content acc to page
        $webcontent = BodyContent::where('slug','web')->first();
        $designcontent = BodyContent::where('slug','design')->first();
        $marketingcontent = BodyContent::where('slug','marketing')->first();
        $applicationcontent = BodyContent::where('slug','application')->first();
        $programcontent = BodyContent::where('slug','program')->first();

        //To display lower body content acc to page
        $indexLowerContent = LowerBody::where('slug','index')->first();
        $webLowerContent = LowerBody::where('slug','web')->first();
        $designLowerContent = LowerBody::where('slug','design')->first();
        $marketingLowerContent = LowerBody::where('slug','marketing')->first();
        $applicationLowerContent = LowerBody::where('slug','application')->first();
        $programLowerContent = LowerBody::where('slug','program')->first();

        //To display content acc to page
        $webtypes = Service::where('type','web')->get();
        $designtypes = Service::where('type','design')->get();
        $marketingtypes = Service::where('type','marketing')->get();
        $applicationtypes = Service::where('type','application')->get();

        //To display tagline acc to page
        $indextagline = Tagline::where('slug','index')->first();
        $webtagline = Tagline::where('slug','web')->first();
        $designtagline = Tagline::where('slug','design')->first();
        $marketingtagline = Tagline::where('slug','marketing')->first();
        $applicationtagline = Tagline::where('slug','application')->first();
        $programtagline = Tagline::where('slug','program')->first();


        return compact('courses', 'faqs','navs', 'offers','stories','webtypes','designtypes','marketingtypes',
        'applicationtypes' , 'navs','webtagline','designtagline','marketingtagline','applicationtagline',
        'programtagline','contactInfo','indexLowerContent','webLowerContent','designLowerContent',
        'marketingLowerContent','applicationLowerContent','programLowerContent',
        'indextagline', 'webcontent','applicationcontent','marketingcontent','programcontent','designcontent');
    }
    public function index()
    {
        try{

            $seoheader = Seo::where(function ($query) {
                $query->where('position', 'header')
                    ->where('page_id', 1);
            })->orWhere(function ($query) {
                $query->where('position', 'header')
                    ->where('page_id', 2);
            })->get();

            $seofooter = Seo::where(function ($query) {
                $query->where('position', 'footer')
                    ->where('page_id', 1);
            })->orWhere(function ($query) {
                $query->where('position', 'footer')
                    ->where('page_id', 2);
            })->get();
            //To display side banner acc to page
            $indexBanner = SideBanner::where('slug','index')->first();
            $testimonials = Testimonial::all();
            return view('index',$this->getDataForView(),compact('seoheader','seofooter','indexBanner','testimonials'));
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong..');
        }
    }
    public function websites()
    {
        try{
            $seoheader = Seo::where(function ($query) {
                $query->where('position', 'header')
                    ->where('page_id', 1);
            })->orWhere(function ($query) {
                $query->where('position', 'header')
                    ->where('page_id',3);
            })->get();

            $seofooter = Seo::where(function ($query) {
                $query->where('position', 'footer')
                    ->where('page_id', 1);
            })->orWhere(function ($query) {
                $query->where('position', 'footer')
                    ->where('page_id', 3);
            })->get();
            return view('websites',$this->getDataForView(),compact('seoheader','seofooter'));
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong..');
        }
    }
    public function designs()
    {
        try{

            $seoheader = Seo::where(function ($query) {
                $query->where('position', 'header')
                    ->where('page_id', 1);
            })->orWhere(function ($query) {
                $query->where('position', 'header')
                    ->where('page_id', 4);
            })->get();

            $seofooter = Seo::where(function ($query) {
                $query->where('position', 'footer')
                    ->where('page_id', 1);
            })->orWhere(function ($query) {
                $query->where('position', 'footer')
                    ->where('page_id', 4);
            })->get();
            return view('design',$this->getDataForView(),compact('seoheader','seofooter'));
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong..');
        }
    }
    public function marketings()
    {
        try
        {
            $seoheader = Seo::where(function ($query) {
                $query->where('position', 'header')
                    ->where('page_id', 1);
            })->orWhere(function ($query) {
                $query->where('position', 'header')
                    ->where('page_id', 5);
            })->get();

            $seofooter = Seo::where(function ($query) {
                $query->where('position', 'footer')
                    ->where('page_id', 1);
            })->orWhere(function ($query) {
                $query->where('position', 'footer')
                    ->where('page_id', 5);
            })->get();
            return view('marketing',$this->getDataForView(),compact('seoheader','seofooter'));
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong..');
        }
    }
    public function programs()
    {
        try{
            $seoheader = Seo::where(function ($query) {
                $query->where('position', 'header')
                    ->where('page_id', 1);
            })->orWhere(function ($query) {
                $query->where('position', 'header')
                    ->where('page_id', 6);
            })->get();

            $seofooter = Seo::where(function ($query) {
                $query->where('position', 'footer')
                    ->where('page_id', 1);
            })->orWhere(function ($query) {
                $query->where('position', 'footer')
                    ->where('page_id', 6);
            })->get();

            return view('programs',$this->getDataForView(),compact('seoheader','seofooter'));
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong..');
        }
    }
    public function application()
    {
        try{

            $seoheader = Seo::where(function ($query) {
                $query->where('position', 'header')
                    ->where('page_id', 1);
            })->orWhere(function ($query) {
                $query->where('position', 'header')
                    ->where('page_id', 7);
            })->get();

            $seofooter = Seo::where(function ($query) {
                $query->where('position', 'footer')
                    ->where('page_id', 1);
            })->orWhere(function ($query) {
                $query->where('position', 'footer')
                    ->where('page_id', 7);
            })->get();
            return view('application',$this->getDataForView(),compact('seoheader','seofooter'));
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong..');
        }
    }
    public function courses(string $slug)
    {
        try {
            //To display content acc to course
            $courseLearning = CourseLearning::where('slug', $slug)->get();
            $formattedItems = [];
            foreach ($courseLearning as $item) {
                $description = $item->description;
                // preg_match_all('/<p>(.*?)<\/p>(.*?)<p>(.*?)<\/p>/', $description, $matches, PREG_SET_ORDER);
                preg_match_all('/<p>(.*?)<br>(.*?)<\/p>/', $description, $matches, PREG_SET_ORDER);
                foreach ($matches as $match) {
                    $title = trim(strip_tags($match[1]));
                    $description = trim($match[2]);
                    $formattedItems[] = [
                        'title' => $title,
                        'description' => $description,
                    ];
                }
            }
            $tagline = Tagline::where('slug', $slug)->first();
            $contents = CourseContent::where('slug', $slug)->first();
            $details = CourseDetail::where('slug', $slug)->first();
            $syllabus = Syllabus::where('slug', $slug)->first();
            //To display side banner acc to page
            $sideBanner = SideBanner::where('slug',$slug)->first();
            //Render SEO
            $page = Page::where('slug',$slug)->first();
            $seoheader = Seo::where(function ($query) {
                $query->where('position', 'header')
                    ->where('page_id', 1);
            })->orWhere(function ($query) use ($slug) {
                $query->where('position', 'header')
                    ->whereHas('page', function ($query) use ($slug) {
                        $query->where('slug', $slug);
                    });
            })->get();
            $seofooter = Seo::where(function ($query) {
                $query->where('position', 'footer')
                    ->where('page_id', 1);
            })->orWhere(function ($query) use ($slug) {
                $query->where('position', 'footer')
                    ->whereHas('page', function ($query) use ($slug) {
                        $query->where('slug', $slug);
                    });
            })->get();
            return view('course', $this->getDataForView(), compact('tagline','contents', 'details', 'syllabus','sideBanner', 'formattedItems','seoheader','seofooter'));
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Please wait a moment.');;
        }
    }
    //Store business enquiry form
    public function storeEnquiry(Request $request)
    {
        try{
            $formFields = $request->validate([
                'name'=>'required',
                'email'=>'required',
                'phone'=>'required',
                'type'=>'required',
                'description'=>'required',
            ]);
            Enquiry::create($formFields);
            //Store data for email book
            $fields = $request->validate([
                'email'=>'required|unique:email_books,email',
            ]);
            EmailBook::create($fields+[ 'type' => 'business']);
            return back()->with('message','Message sent successfully');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Message sent successfully');
        }
    }
    //Store visitors contact form
    public function storeContact(Request $request)
    {
        try{

            $formFields = $request->validate([
                'name'=>'required',
                'email'=>'required|unique:contact_emails,email',
                'phone'=>'required',
                'type'=>'required',
            ], [
                'email.unique' => 'Please select another email',
            ]);
            $slug = Str::slug($formFields['name']);
            ContactEmail::create($formFields+['slug'=>$slug]);
            //Store data for email book
            $fields = $request->validate([
                'email'=>'required|unique:email_books,email',
                'type'=>'required',
            ]);
            EmailBook::create($fields);
            return back()->with('message', 'Contact sent successfully');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Please select another email');
        }
    }
    //Store visitors contact form
    public function sendMessage(Request $request)
    {
        try{
            $formFields = $request->validate([
                'name'=>'required',
                'email'=>'required',
                'phone'=>'required',
                'address'=>'required',
                'description'=>'required',
                'type'=>'required',
            ]);
            Enquiry::create($formFields);
            //Store data for email book
            $fields = $request->validate([
                'email'=>'required|unique:email_books,email',
            ]);
            EmailBook::create($fields+[ 'type' => 'visitor']);
            return back()->with('message', 'Message sent successfully');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Message sent successfully');
        }
    }
    public function contacts()
    {
        try{
            $seoheader = Seo::where(function ($query) {
                $query->where('position', 'header')
                    ->where('page_id', 1);
            })->orWhere(function ($query) {
                $query->where('position', 'header')
                    ->where('page_id', 8);
            })->get();

            $seofooter = Seo::where(function ($query) {
                $query->where('position', 'footer')
                    ->where('page_id', 1);
            })->orWhere(function ($query) {
                $query->where('position', 'footer')
                    ->where('page_id', 8);
            })->get();
            return view('contacts',$this->getDataForView(),compact('seoheader','seofooter'));
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong..');
        }
    }
    public function appointment()
    {
        try{
            $seoheader = Seo::where(function ($query) {
                $query->where('position', 'header')
                    ->where('page_id', 1);
            })->orWhere(function ($query) {
                $query->where('position', 'header')
                    ->where('page_id', 13);
            })->get();

            $seofooter = Seo::where(function ($query) {
                $query->where('position', 'footer')
                    ->where('page_id', 1);
            })->orWhere(function ($query) {
                $query->where('position', 'footer')
                    ->where('page_id', 13);
            })->get();
            return view('appointment',$this->getDataForView(),compact('seoheader','seofooter'));
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong..');
        }
    }
    public function bookAppointment(Request $request)
    {
        try{
            $formFields = $request->validate([
                'name'=>'required',
                'email'=>'required|unique:appointments,email,NULL,id,date,' . $request->input('date'),
                'phone'=>'required',
                'address'=>'required',
                'date' => [
                    'required',
                    function ($attribute, $value, $fail) {
                        $selectedDate = Carbon::parse($value)->startOfDay(); // Convert to start of the day for accurate comparison

                        $existingAppointments = Appointment::whereDate('date', $selectedDate)
                            ->where('email', request()->input('email'))
                            ->count();

                        if ($existingAppointments > 0) {
                            $fail('You have already booked an appointment for this day.');
                        }
                    },
                ],
                'description'=>'required',
            ], [
                'email.unique' => 'Booking for this email already exists.',
            ]);
            Appointment::create($formFields);
            return back()->with('message', 'Appontment booked successfully');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'You have already booked an appointment for this day.');
        }
    }
    public function about()
    {
        try{
            $tagline = Tagline::where('slug','about')->first();
            $seoheader = Seo::where(function ($query) {
                $query->where('position', 'header')
                    ->where('page_id', 1);
            })->orWhere(function ($query) {
                $query->where('position', 'header')
                    ->where('page_id', 9);
            })->get();

            $seofooter = Seo::where(function ($query) {
                $query->where('position', 'footer')
                    ->where('page_id', 1);
            })->orWhere(function ($query) {
                $query->where('position', 'footer')
                    ->where('page_id', 9);
            })->get();
            $events = Gallery::where('type','events')->get();
            $office = Gallery::where('type','office')->get();
            $products = Gallery::where('type','products')->get();
            $team = Gallery::where('type','team')->get();
            return view('about',$this->getDataForView(),compact('seoheader','seofooter','events','office','products','team','tagline'));
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong..');
        }
    }

    public function search(Request $request)
    {
        try{
            $searchTerm = $request->input('search');
            // Check if there is an exact match for the search term
            $exactCourse = Course::where('name', $searchTerm)->first();
            $exactService = Service::where('name', $searchTerm)->first();
            if ($exactCourse) {
                // Exact match found, retrieve the necessary data
                $seoheader = Seo::where(function ($query) use ($exactCourse) {
                    $query->where('position', 'header')
                        ->where('page_id', $exactCourse->id);
                })->orWhere(function ($query) use ($exactCourse) {
                    $query->where('position', 'header')
                        ->whereHas('page', function ($query) use ($exactCourse) {
                            $query->where('slug', $exactCourse->slug);
                        });
                })->get();

                $seofooter = Seo::where(function ($query) use ($exactCourse) {
                    $query->where('position', 'footer')
                        ->where('page_id', $exactCourse->id);
                })->orWhere(function ($query) use ($exactCourse) {
                    $query->where('position', 'footer')
                        ->whereHas('page', function ($query) use ($exactCourse) {
                            $query->where('slug', $exactCourse->slug);
                        });
                })->get();
                $courseLearning = CourseLearning::where('slug', $exactCourse->slug)->get();
                $formattedItems = [];
                foreach ($courseLearning as $item) {
                    $description = $item->description;
                    preg_match_all('/<p>(.*?)<br>(.*?)<\/p>/', $description, $matches, PREG_SET_ORDER);
                    foreach ($matches as $match) {
                        $title = trim(strip_tags($match[1]));
                        $description = trim($match[2]);
                        $formattedItems[] = [
                            'title' => $title,
                            'description' => $description,
                        ];
                    }
                }
                $tagline = Tagline::where('slug', $exactCourse->slug)->first();
                $contents = CourseContent::where('slug', $exactCourse->slug)->first();
                $details = CourseDetail::where('slug', $exactCourse->slug)->first();
                $learnings = CourseLearning::where('slug', $exactCourse->slug)->get();
                $syllabus = Syllabus::where('slug', $exactCourse->slug)->first();

                return view('course',$this->getDataForView(), compact('contents','tagline', 'details', 'learnings', 'syllabus', 'formattedItems', 'seoheader', 'seofooter'));
            }
            if ($exactService) {
                // Exact match found in services, redirect to the service page

                $tagline = Tagline::where('slug', $exactService->slug)->first();
                $bodycontent = BodyContent::where('slug',$exactService->slug)->first();
                $lowerBody = LowerBody::where('slug', $exactService->slug)->first();
                //To display side banner acc to page
                $sideBanner = SideBanner::where('slug',$exactService->slug)->first();
                //Render seo
                $seoheader = Seo::where(function ($query) {
                    $query->where('position', 'header')
                        ->where('page_id', 1);
                })->orWhere(function ($query) use ($exactService) {
                    $query->where('position', 'header')
                        ->whereHas('page', function ($query) use ($exactService) {
                            $query->where('slug', $exactService->slug);
                        });
                })->get();

                $seofooter = Seo::where(function ($query) {
                    $query->where('position', 'footer')
                        ->where('page_id', 1);
                })->orWhere(function ($query) use ($exactService) {
                    $query->where('position', 'footer')
                        ->whereHas('page', function ($query) use ($exactService) {
                            $query->where('slug', $exactService->slug);
                        });
                })->get();
                //Render paragraph and store it variables
                $firstParagraph = null;
                $secondParagraph = null;
                $thirdParagraph = null;
                $paragraphArray = [];
                if ($bodycontent && !empty($bodycontent->description)) {
                    // Split the paragraph into separate paragraphs
                    $paragraphs = explode("\n", $bodycontent->description);
                    // Store paragraphs in separate variables
                    $firstParagraph = isset($paragraphs[0]) ? $paragraphs[0] : null;
                    $secondParagraph = isset($paragraphs[2]) ? $paragraphs[2] : null;
                    $thirdParagraph = isset($paragraphs[4]) ? $paragraphs[4] : null;

                    foreach ($paragraphs as $index => $paragraph) {
                        $paragraphArray["paragraph_" . ($index + 1)] = $paragraph;
                    }
                }
                return view('service',$this->getDataForView(), compact('seoheader','seofooter','tagline','bodycontent','sideBanner','lowerBody', 'firstParagraph','secondParagraph','thirdParagraph'));
            }
            // No exact match, search for similar courses starting with the same letter
            // Search for courses based on name or keywords
            $similarCourses = Course::where('name', 'LIKE', $searchTerm[0] . '%')
            ->orWhere('keywords', 'LIKE', '%' . $searchTerm . '%')->with('tagline')
            ->get();
            $similarServices = Service::where('name', 'LIKE', $searchTerm[0] . '%')->with('tagline') ->get();
            if ($similarCourses->isEmpty() || $similarServices->isEmpty()) {
                // No similar courses found, redirect to a page with search results
                return view('search-results',$this->getDataForView(),compact('searchTerm', 'similarCourses','similarServices'));
            }
            // Similar courses found, redirect to the search results page
            return view('search-results', $this->getDataForView(),compact('searchTerm', 'similarCourses','similarServices'));
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong..');
        }
    }

    public function showSearchResults(Request $request)
    {
        try
        {
            $searchTerm = $request->input('searchTerm');
            $similarCourses = Course::where('name', 'LIKE', $searchTerm[0] . '%')
            ->orWhere('keywords', 'LIKE', '%' . $searchTerm . '%')->with('tagline')
            ->get();
            $similarServices = Course::where('name', 'LIKE', $searchTerm[0] . '%')->with('tagline') ->get();
            return view('search-results', compact('searchTerm', 'similarCourses','similarServices'));
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong..');
        }
    }

    //Store course bookings
    public function book(Request $request)
    {
        try{
            $formFields = $request->validate([
                'name' => 'required',
                'email' => 'required|unique:bookings,email,NULL,id,course_id,' . $request->input('course_id'),
                'phone' => 'required',
                'address' => 'required',
                'qualification' => 'required',
                'date' => 'required',
                'time' => 'required',
                'course_id' => 'required',
            ], [
                'email.unique' => 'Booking for this email already exists.',
            ]);
            $slug = Str::slug($formFields['email']);
            Booking::create($formFields+['slug'=>$slug]);
             //Store data for email book
             $fields = $request->validate([
                'email'=>'required|unique:email_books,email',
            ]);
            EmailBook::create($fields+[ 'type' => 'student']);
            return back()->with('message','Course booked successfully');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Booking for this email already exists');
        }
    }
    public function privacy()
    {
        try{
            $seoheader = Seo::where(function ($query) {
                $query->where('position', 'header')
                    ->where('page_id', 1);
            })->orWhere(function ($query) {
                $query->where('position', 'header')
                    ->where('page_id', 15);
            })->get();

            $seofooter = Seo::where(function ($query) {
                $query->where('position', 'footer')
                    ->where('page_id', 1);
            })->orWhere(function ($query) {
                $query->where('position', 'footer')
                    ->where('page_id', 15);
            })->get();
            $privacies = PrivacyPolicy::all();
            $latestUpdatedAt = PrivacyPolicy::latest('updated_at')->value('updated_at');
            $privacyUpdatedDate = Carbon::parse($latestUpdatedAt);
            return view('privacy',$this->getDataForView(),compact('privacies','privacyUpdatedDate','seoheader','seofooter'));
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong..');
        }
    }
    public function accessibility()
    {
        $seoheader = Seo::where(function ($query) {
            $query->where('position', 'header')
                ->where('page_id', 1);
        })->orWhere(function ($query) {
            $query->where('position', 'header')
                ->where('page_id', 18);
        })->get();

        $seofooter = Seo::where(function ($query) {
            $query->where('position', 'footer')
                ->where('page_id', 1);
        })->orWhere(function ($query) {
            $query->where('position', 'footer')
                ->where('page_id', 18);
        })->get();
        return view('accessibility',$this->getDataForView(),compact('seoheader','seofooter'));
    }
    public function disclaimer()
    {
        $seoheader = Seo::where(function ($query) {
            $query->where('position', 'header')
                ->where('page_id', 1);
        })->orWhere(function ($query) {
            $query->where('position', 'header')
                ->where('page_id', 17);
        })->get();

        $seofooter = Seo::where(function ($query) {
            $query->where('position', 'footer')
                ->where('page_id', 1);
        })->orWhere(function ($query) {
            $query->where('position', 'footer')
                ->where('page_id', 17);
        })->get();
        return view('disclaimer',$this->getDataForView(),compact('seoheader','seofooter'));
    }
    public function termsOfUse()
    {
        $seoheader = Seo::where(function ($query) {
            $query->where('position', 'header')
                ->where('page_id', 1);
        })->orWhere(function ($query) {
            $query->where('position', 'header')
                ->where('page_id', 16);
        })->get();

        $seofooter = Seo::where(function ($query) {
            $query->where('position', 'footer')
                ->where('page_id', 1);
        })->orWhere(function ($query) {
            $query->where('position', 'footer')
                ->where('page_id', 16);
        })->get();
        return view('terms-of-use',$this->getDataForView(),compact('seoheader','seofooter'));
    }
    public function siteMap()
    {
        $seoheader = Seo::where(function ($query) {
            $query->where('position', 'header')
                ->where('page_id', 1);
        })->orWhere(function ($query) {
            $query->where('position', 'header')
                ->where('page_id', 14);
        })->get();

        $seofooter = Seo::where(function ($query) {
            $query->where('position', 'footer')
                ->where('page_id', 1);
        })->orWhere(function ($query) {
            $query->where('position', 'footer')
                ->where('page_id', 14);
        })->get();
        return view('site-map',$this->getDataForView(),compact('seoheader','seofooter'));
    }
    public function helpCenter()
    {
        $seoheader = Seo::where(function ($query) {
            $query->where('position', 'header')
                ->where('page_id', 1);
        })->orWhere(function ($query) {
            $query->where('position', 'header')
                ->where('page_id', 12);
        })->get();

        $seofooter = Seo::where(function ($query) {
            $query->where('position', 'footer')
                ->where('page_id', 1);
        })->orWhere(function ($query) {
            $query->where('position', 'footer')
                ->where('page_id', 12);
        })->get();
        return view('help-center',$this->getDataForView(),compact('seoheader','seofooter'));
    }
    public function career()
    {
        $careers = Career::all();
        $seoheader = Seo::where(function ($query) {
            $query->where('position', 'header')
                ->where('page_id', 1);
        })->orWhere(function ($query) {
            $query->where('position', 'header')
                ->where('page_id', 11);
        })->get();

        $seofooter = Seo::where(function ($query) {
            $query->where('position', 'footer')
                ->where('page_id', 1);
        })->orWhere(function ($query) {
            $query->where('position', 'footer')
                ->where('page_id', 11);
        })->get();
        return view('career',$this->getDataForView(),compact('careers','seoheader','seofooter'));
    }
    public function careerDetails(string $slug)
    {
        $career = Career::where('slug',$slug)->first();
        $contacts = ContactInfo::first();
        $details = CareerDetails::where('slug',$slug)->first();
        $seoheader = Seo::where(function ($query) {
            $query->where('position', 'header')
                ->where('page_id', 1);
        })->orWhere(function ($query) {
            $query->where('position', 'header')
                ->where('page_id', 11);
        })->get();

        $seofooter = Seo::where(function ($query) {
            $query->where('position', 'footer')
                ->where('page_id', 1);
        })->orWhere(function ($query) {
            $query->where('position', 'footer')
                ->where('page_id', 11);
        })->get();
        return view('career-details',$this->getDataForView(),compact('career','seoheader','seofooter','details','contacts'));
    }
    public function bookCareer(Request $request)
    {
        try{
            $formFields = $request->validate([
                'name'=>'required',
                'email'=>'required|unique:career_bookings,email,NULL,id,career_id,' . $request->input('career_id'),
                'cv'=>'required',
                'career_id'=>'required',
            ], [
                'email.unique' => 'Please select another email',
            ]);
            $slug = Str::slug($formFields['email']);
            if($request->hasFile('cv')){
                $formFields['cv'] = $request->file('cv')->store('cv','public');
            }
            CareerBooking::create($formFields+['slug'=>$slug]);
            return back()->with('message', 'Your request is submitted successfully');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Your have already submitted request using this email.');
        }
    }
    public function service(string $slug)
    {
        try{
            $tagline = Tagline::where('slug', $slug)->first();
            $bodycontent = BodyContent::where('slug',$slug)->first();
            $lowerBody = LowerBody::where('slug', $slug)->first();
            //To display side banner acc to page
            $sideBanner = SideBanner::where('slug',$slug)->first();
            //Render seo
            $page = Page::where('slug',$slug)->first();
            $seoheader = Seo::where(function ($query) {
                $query->where('position', 'header')
                    ->where('page_id', 1);
            })->orWhere(function ($query) use ($slug) {
                $query->where('position', 'header')
                    ->whereHas('page', function ($query) use ($slug) {
                        $query->where('slug', $slug);
                    });
            })->get();
            $seofooter = Seo::where(function ($query) {
                $query->where('position', 'footer')
                    ->where('page_id', 1);
            })->orWhere(function ($query) use ($slug) {
                $query->where('position', 'footer')
                    ->whereHas('page', function ($query) use ($slug) {
                        $query->where('slug', $slug);
                    });
            })->get();
            //Render paragraph and store it variables
            $firstParagraph = null;
            $secondParagraph = null;
            $thirdParagraph = null;
            $paragraphArray = [];
            if ($bodycontent && !empty($bodycontent->description)) {
                // Split the paragraph into separate paragraphs
                $paragraphs = explode("\n", $bodycontent->description);
                 // Store paragraphs in separate variables
                $firstParagraph = isset($paragraphs[0]) ? $paragraphs[0] : null;
                $secondParagraph = isset($paragraphs[2]) ? $paragraphs[2] : null;
                $thirdParagraph = isset($paragraphs[4]) ? $paragraphs[4] : null;

                foreach ($paragraphs as $index => $paragraph) {
                    $paragraphArray["paragraph_" . ($index + 1)] = $paragraph;
                }
            }
            return view('service',$this->getDataForView(), compact('seoheader','seofooter','tagline','bodycontent','sideBanner','lowerBody', 'firstParagraph','secondParagraph','thirdParagraph'));
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Please wait a moment.');
        }
    }
    public function subscribe(Request $request){
        try{
            if( Newsletter::isSubscribed($request->email) ){
                return redirect()->back()->with('error','Email already subscribed');
            }
            Newsletter::subscribeOrUpdate($request->email, ['tags' => $request->tag]);
            return redirect()->back()->with('message','Thankyou for subscribing to our newsletter');
        }
        catch(\Exception $e){
            return redirect()->back()->with('error','Something went wrong..');
        }
    }


}
