<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Course;
use App\Models\Page;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use Illuminate\Database\QueryException;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $courses = Course::latest()->filter(request(['name','search']))->simplePaginate(5);
        return view('admin.courses.index',compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.courses.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $formFields = $request->validate([
                    'name'=>'required|unique:courses,name',
                    'price'=>'required',
                    'description'=>'required',
                    'keywords'=>'required',
                ]
                ,
                [
                    'name.unique' => 'This course is already exists.',
                ]
            );
                $slug =Str::slug($formFields['name']);
                Course::create($formFields+['slug'=>$slug]);
                //Store name to page table
                Page::create([
                    'name' => $formFields['name'],
                    'slug' => $slug,
                ]);
                return redirect('/admin/course')->with('message','Course created successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'This course is already exists.');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $slug)
    {
        $course = Course::where('slug',$slug)->firstOrFail();
        return view('admin.courses.details',compact('course'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $slug)
    {

        $course = Course::where('slug',$slug)->firstOrFail();
        return view('admin.courses.edit',compact('course'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $slug)
    {

        try{
            $course = Course::where('slug',$slug)->firstOrFail();
            $formFields = $request->validate([
                'name' => [
                    'required',
                    Rule::unique('courses')->ignore($course->id),
                ],
                'price'=>'required',
                'description'=>'required',
                'keywords'=>'required',
            ], [
                'name.unique' => 'This course is already exists.',
            ]);
            $slug =Str::slug($formFields['name']);
            $course->update($formFields+['slug'=>$slug]);
            return redirect('/admin/course')->with('message','Course updated successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'This course is already exists');
        }

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $slug)
    {
        $course = Course::where('slug',$slug)->firstOrFail();
        $course->delete();
        $page = Page::where('slug',$slug)->first();
        $page->delete();
        return redirect('/admin/course')->with('message','Course deleted successfully');
    }
}
