<?php

namespace App\Http\Controllers;

use App\Models\ContactInfo;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Database\QueryException;

class ContactInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $contactInfos = ContactInfo::latest()->get();
        return view('admin.contact_info.index',compact('contactInfos'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $contactInfo = ContactInfo::first();
        if ($contactInfo) {
            return redirect('/admin/contactInfo')->with('message','Contact information already exists');
        }
        return view('admin.contact_info.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $formFields = $request->validate([
                'mobile_no' =>'required|unique:contact_infos,mobile_no',
                'description'=>'required|unique:contact_infos,description',
                'location'=>'required|unique:contact_infos,location',
                'map_link'=>'required|unique:contact_infos,map_link',
                ]
                , [
                    'mobile_no.unique' => 'Contact details already exists.',
                ]
            );
            ContactInfo::create($formFields);
            return redirect('/admin/contactInfo')->with('message','Contact details added successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'Contact details already exists');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $contactInfo = ContactInfo::whereId($id)->first();
        return view('admin.contact_info.details',compact('contactInfo'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $contactInfo = ContactInfo::whereId($id)->first();
        return view('admin.contact_info.edit',compact('contactInfo'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try{
            $contactInfo = ContactInfo::whereId($id)->first();
            $formFields = $request->validate([
                'mobile_no' =>'required',
                'description'=>'required',
                'location'=>'required',
                'map_link'=>'required',
                ]
            );
            $contactInfo->update($formFields);
            return redirect('/admin/contactInfo')->with('message','Contact details updated successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong...');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $contactInfo = ContactInfo::whereId($id)->first();
        $contactInfo->delete();
        return redirect('/admin/contactInfo')->with('message','Contact details deleted successfully');
    }
}
