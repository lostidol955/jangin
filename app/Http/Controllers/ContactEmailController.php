<?php

namespace App\Http\Controllers;

use App\Models\ContactEmail;
use App\Models\EmailBook;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use Illuminate\Database\QueryException;

class ContactEmailController extends Controller
{
   /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $contacts= ContactEmail::latest()->filter(request(['name','search']))->simplePaginate(5);
        return view('admin.contacts.index',compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.contacts.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $formFields = $request->validate([
                'name'=>'required',
                'email'=>'required|unique:contact_emails,email',
                'phone'=>'required',
                'type'=>'required',
            ], [
                'email.unique' => 'This email is already exists.',
            ]);
            $slug = Str::slug($formFields['name']);
            ContactEmail::create($formFields+['slug'=>$slug]);
            //Store data for email book
            $fields = $request->validate([
                'email'=>'required|unique:email_books,email',
                'type'=>'required',
            ]);
            EmailBook::create($fields);
            return redirect('/admin/contacts')->with('message','Contacts created successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'This email is already exists');
        }

    }

    /**
     * Display the specified resource.
     */
    public function show(string $slug)
    {
        $contact = ContactEmail::where('slug',$slug)->firstOrFail();
        return view('admin.contacts.details',compact('contact'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $slug)
    {
        $contact = ContactEmail::where('slug',$slug)->firstOrFail();
        return view('admin.contacts.edit',compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $slug)
    {
        try{
            $contact = ContactEmail::where('slug',$slug)->firstOrFail();
            $formFields = $request->validate([
                'name'=>'required',
                'phone'=>'required',
                'type'=>'required',
                'description'=>'required',
                'email' => [
                    'required',
                    Rule::unique('contact_emails')->ignore($contact->id),
                ],
            ], [
                'email.unique' => 'This email is already exists.',
            ]);
            $slug =Str::slug($formFields['name']);
            $contact->update($formFields+['slug'=>$slug]);
            return redirect('/admin/contacts')->with('message','Contacts updated successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'This email is already exists');
        }

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $slug)
    {
        $contact = ContactEmail::where('slug',$slug)->firstOrFail();
        $contact->delete();
        return redirect('/admin/contacts')->with('message','Contacts deleted successfully');
    }
}
