<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use App\Models\Page;
use App\Models\Tagline;
use Illuminate\Database\QueryException;

class TaglineController extends Controller
{
     /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $taglines = Tagline::latest()->filter(request(['tagline','search']))->simplePaginate(5);
        return view('admin.taglines.index',compact('taglines'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $pages = Page::all();
        return view('admin.taglines.create',compact('pages'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $formFields = $request->validate([
                'color'=>'nullable',
                'title'=>'required',
                'image'=>'nullable|image|mimes:jpeg,png,jpg,gif',
                'page_id' => 'required|unique:taglines,page_id',
            ],
            [
                'page_id.unique' => 'Tagline for this page already exists',
            ]
           );
            if($request->hasFile('image')){
              $formFields['image'] = $request->file('image')->store('overview','public');
            }
           $page = Page::where('id',$request->page_id)->first();
           $slug = Str::slug($page->name);
           Tagline::create($formFields+['slug'=>$slug]);
            return redirect('/admin/tagline')->with('message','Tagline added successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'Tagline for this page already exists');
        }

    }

    /**
     * Display the specified resource.
     */
    public function show(string $slug)
    {
        $tagline = Tagline::where('slug',$slug)->first();
        return view('admin.taglines.details',compact('tagline'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $slug)
    {
        $pages = Page::all();
        $tagline = Tagline::where('slug',$slug)->first();
        return view('admin.taglines.edit',compact('tagline','pages'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $slug)
    {
        try{
            $tagline = Tagline::where('slug',$slug)->first();
            $formFields = $request->validate([
                'color'=>'nullable',
                'title'=>'required',
                'image'=>'nullable|image|mimes:jpeg,png,jpg,gif',
                'page_id' => [
                    'required',
                    Rule::unique('taglines')->ignore($tagline->id),
                ],
                ], [
                    'page_id.unique' => 'Tagline for this page already exists.',
                ]);
            //if admin select new image
            if($request->hasFile('image')){
                //delete existing image
                $image_path = public_path('storage/'.$tagline->image);
                if(file_exists($image_path)){
                    unlink($image_path);
                }
                $formFields['image'] = $request->file('image')->store('overview','public');
                $page = Page::where('id',$request->page_id)->first();
                $slug = Str::slug($page->name);
                $tagline->update($formFields+['slug'=>$slug]);
            }
            $page = Page::where('id',$request->page_id)->first();
            $slug = Str::slug($page->name);
            $tagline->update($formFields+['slug'=>$slug]);
            return redirect('/admin/tagline')->with('message','Tagline updated successfully');
        }
        catch (\Exception $e) {
            return redirect()->back()->with('error', 'Tagline for this page already exists');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $slug)
    {
       $tagline = Tagline::where('slug',$slug)->first();
        //delete existing image
        if(!empty($tagline->image))
        {
            $image_path = public_path('storage/'.$tagline->image);
            if(file_exists($image_path)){
                unlink($image_path);
              }
        }
        $tagline->delete();
        return redirect('/admin/tagline')->with('message','Tagline deleted successfully');
    }
}
