<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validate\Rule;
use App\Models\Story;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class StoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $stories = Story::latest()->filter(request(['title','search']))->simplePaginate(5);
        return view('admin.stories.index',compact('stories'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.stories.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $formFields = $request->validate([
            'title'=>'required',
            'description'=>'required',
            'image'=>'required|image|mimes:jpeg,png,jpg,gif',
            'link'=>'required',
        ]);
        $slug = Str::slug($formFields['title']);
        if($request->hasFile('image')){
            $formFields['image'] = $request->file('image')->store('stories','public');
        }
        Story::create($formFields+['slug'=>$slug]);
        return redirect('/admin/stories')->with('message','Story created successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $slug)
    {
        $story = Story::where('slug',$slug)->firstOrFail();
        return view('admin.stories.details',compact('story'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $slug)
    {
        $story = Story::where('slug',$slug)->firstOrFail();
        return view('admin.stories.edit',compact('story'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $slug)
    {
        $story = Story::where('slug',$slug)->firstOrFail();
        $formFields = $request->validate([
            'title'=>'required',
            'description'=>'required',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif',
            'link'=>'required',
        ]);
         //if admin select new image
         if($request->hasFile('image')){
            //delete existing image
            $image_path = public_path('storage/'.$story->image);
            if(file_exists($image_path)){
                unlink($image_path);
              }
            $formFields['image'] = $request->file('image')->store('stories','public');
            $slug = Str::slug($formFields['title']);
            $story->update($formFields+['slug'=>$slug]);
        }
        //if admin didn't select new image
        $slug = Str::slug($formFields['title']);
        $story->update($formFields+['slug'=>$slug]);
        return redirect('/admin/stories')->with('message','Story updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $slug)
    {
        $story= Story::where('slug',$slug)->firstOrFail();
         //delete existing image
         $image_path = public_path('storage/'.$story->image);
         if(file_exists($image_path)){
             unlink($image_path);
           }
        $story->delete();
        return redirect('/admin/stories')->with('message','Story deleted successfully');
    }
}
