$(document).ready(function() {
    $('#searchForm').submit(function(event) {
        // Prevent form submission
        event.preventDefault(); // Prevent the form from submitting
        // Clear previous error messages
        $('.input-area').removeClass('error');
        // Validate the form fields
        var isValid = true;
        // Validate searchbar
        var search = $('#searchBar').val().trim();
        if (search === '') {
            $('#searchBar').addClass('error');
            $('#searchBar').attr('placeholder', 'Enter some text..');
            isValid = false;
        }
        // Submit the form if valid
        if (isValid) {
            this.submit();
        } else {
            console.log('Form is not valid. Submission prevented.');
        }
    });
});
