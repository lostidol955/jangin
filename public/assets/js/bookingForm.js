$(document).ready(function() {
    // Booking Form submission
    $('#bookingForm').submit(function(event) {
        event.preventDefault(); // Prevent the form from submitting
        // Clear previous error messages and red color
        $('.input-field input').removeClass('error');
        $('.input-field select').removeClass('error');
        $('.input-field input[type="date"]').removeClass('error');
        $('.input-field input[type="time"]').removeClass('error');

        // Validate the form fields
        var isValid = true;
        // Validate Full Name
        var name = $('#name').val().trim();
        if (name === '') {
            $('#name').addClass('error');
            $('#name').attr('placeholder', 'Full Name* (required)');
            isValid = false;
        }

        // Validate Email
        var email = $('#email').val().trim();
        if (email === '') {
            $('#email').addClass('error');
            $('#email').attr('placeholder', 'Email* (required)');
            isValid = false;
        } else if (!isValidEmail(email)) {
            $('#email').addClass('error');
            $('#email').val('');
            $('#email').attr('placeholder', 'Invalid Email');
            isValid = false;
        }

        // Validate Mobile/Phone
        var phone = $('#phone').val().trim();
        if (phone === '') {
            $('#phone').addClass('error');
            $('#phone').attr('placeholder', 'Mobile/Phone* (required)');
            isValid = false;
        }

        // Validate Address
        var address = $('#address').val().trim();
        if (address === '') {
            $('#address').addClass('error');
            $('#address').attr('placeholder', 'Address* (required)');
            isValid = false;
        }

        // Validate Qualification
        var qualification = $('#qualification').val();
        if (qualification === '') {
            $('#qualification').addClass('error');
            $('#qualification').attr('placeholder', 'Qualification* (required)');
            isValid = false;
        }

        // Validate Start Date
        var startDate = $('#date').val().trim();
        if (startDate === '') {
            $('#date').addClass('error');
            $('#date').attr('placeholder', 'Start Date* (required)');
            isValid = false;
        }

        // Validate Prefer Time
        var preferTime = $('#time').val().trim();
        if (preferTime === '') {
            $('#time').addClass('error');
            $('#time').attr('placeholder', 'Prefer time* (required)');
            isValid = false;
        }

        // Submit the form if valid
        if (isValid) {
            this.submit();
        }
    });
    $('#subscribeForm').submit(function(event) {
        event.preventDefault(); // Prevent the form from submitting
        // Clear previous error messages and red color
        $('.subscribe-input').removeClass('error');

        // Validate the form fields
        var isValid = true;


        // Validate Email
        var email = $('#subscriber_email').val().trim();
        if (email === '') {
            $('#subscriber_email').addClass('error');
            $('#subscriber_email').attr('placeholder', 'Email* (required)');
            isValid = false;
        } else if (!isValidEmail(email)) {
            $('#subscriber_email').addClass('error');
            $('#subscriber_email').val('');
            $('#subscriber_email').attr('placeholder', 'Invalid Email');
            isValid = false;
        }
        // Submit the form if valid
        if (isValid) {
            this.submit();
        }
    });
    // Function to validate email format
    function isValidEmail(email) {
        var emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return emailRegex.test(email);
    }

});
