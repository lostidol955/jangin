function updateTime() {
    const now = new Date();
  let hours = now.getHours();
  let minutes = now.getMinutes();
  const amPm = hours >= 12 ? 'PM' : 'AM';

  // Convert to 12-hour format
  hours = hours % 12 || 12;

  // Pad the minutes and seconds with leading zeros
  minutes = minutes < 10 ? '0' + minutes : minutes;

  // Construct the time string
  const timeString = `${hours}:${minutes} ${amPm}`;

    document.getElementById('currentime').textContent = timeString;
  }

  setInterval(updateTime, 1000);

  var currentYear = new Date().getFullYear();

  document.getElementById("currentYear").textContent = currentYear;
