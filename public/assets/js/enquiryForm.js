$(document).ready(function() {
    // Set the maximum character count
    var maxCharCount = 500;

    // Update character count on input
    $('#description').on('input', function() {
        var currentCharCount = $(this).val().length;
        var remainingCharCount = maxCharCount - currentCharCount;

        // Update the character count display
        $('#charCount').text('Max character ' + remainingCharCount);

        // Restrict text input if the character count reaches 0
        if (remainingCharCount <= 0) {
            $(this).val(function(_, val) {
                return val.substr(0, maxCharCount);
            });
        }
    });

    // Form validation and submission
    $('#enquiryForm').submit(function(event) {
        // Clear previous error messages
        $('#enquiryForm .input-field input').removeClass('error');
        $('#enquiryForm .input-field textarea').removeClass('error');

        // Check if character count is less than 0
        var currentCharCount = $('#description').val().length;
        var remainingCharCount = maxCharCount - currentCharCount;
        if (remainingCharCount < 0) {
            event.preventDefault();
            return; // Stop further execution
        }

        // Validate the form fields
        var isValid = true;

        // Validate Full Name
        var fullName = $('#full_name').val().trim();
        if (fullName === '') {
            $('#full_name').addClass('error');
            $('#full_name').attr('placeholder', 'Full Name*');
            isValid = false;
        }

        // Validate Email
        var emailAddress = $('#email_address').val().trim();
        if (emailAddress === '') {
            $('#email_address').addClass('error');
            $('#email_address').attr('placeholder', 'Email*');
            isValid = false;
        } else if (!isValidEmail(emailAddress)) {
            $('#email_address').addClass('error');
            $('#email_address').val('');
            $('#email_address').attr('placeholder', 'Invalid Email');
            isValid = false;
        }

        // Validate Mobile/Phone
        var phoneNumber = $('#phone_number').val().trim();
        if (phoneNumber === '') {
            $('#phone_number').addClass('error');
            $('#phone_number').attr('placeholder', 'Mobile/Phone*');
            $('#phone_number').addClass('red-placeholder');
            isValid = false;
        }

        // Validate Address
        var address = $('#country').val().trim();
        if (address === '') {
            $('#country').addClass('error');
            $('#country').attr('placeholder', 'Address*');
            isValid = false;
        }

        // Validate Description
        var description = $('#description').val().trim();
        if (description === '') {
            $('#description').addClass('error');
            $('#description').attr('placeholder', 'Explain the project you want*');
            isValid = false;
        }

        // Validate Terms and Conditions checkbox
        var termsChecked = $('#terms_condition').is(':checked');
        if (!termsChecked) {
            $('#terms_condition').addClass('error');
            isValid = false;
        }

        // Prevent the form from submitting if there are validation errors
        if (!isValid) {
            event.preventDefault();
        }
    });

    // Function to validate email format
    function isValidEmail(email) {
        var emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return emailRegex.test(email);
    }
});
