$(document).ready(function(){
    $('#contactForm').submit(function(event) {
        event.preventDefault(); // Prevent the form from submitting

        // Clear previous error messages
        $('.form-input').removeClass('error');
        $('.error-message').remove();

        // Validate the form fields
        var isValid = true;

        // Validate Full Name
        var name = $('#fname').val().trim();
        if (name === '') {
            $('#fname').addClass('error');
            $('#fname').attr('placeholder', 'Full Name*');
            isValid = false;
        }

        // Validate Mobile
        var phone = $('#phone_no').val().trim();
        if (phone === '') {
            $('#phone_no').addClass('error');
            $('#phone_no').attr('placeholder', 'Mobile Number*');
            isValid = false;
        }

        // Validate Email
        var email = $('#user_email').val().trim();
        if (email === '') {
            $('#user_email').addClass('error');
            $('#user_email').attr('placeholder', 'Email Addess*');
            isValid = false;
        } else if (!isValidEmail(email)) {
            $('#user_email').addClass('error');
            $('#user_email').val('');
            $('#user_email').attr('placeholder', 'Invalid Email');
            isValid = false;
        }

        // Submit the form if valid
        if (isValid) {
            this.submit();
        } else {
            console.log('Form is not valid. Submission prevented.');
        }
    });
     // Function to validate email format
     function isValidEmail(email) {
        var emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return emailRegex.test(email);
    }
});
