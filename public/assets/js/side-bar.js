
$(document).ready(function (){
    $(".toggle_text").text("See more");
    $(".start-btn,#start-btn-2, #enroll-button,.apply").click(function(){
        $('.sidebar').addClass('show-sidebar');
        $('.sidebar .right-section').addClass('show-right-img');
    });
    $("#close-btn").click(function() {
        $('.sidebar').removeClass('show-sidebar');
        $('.sidebar .right-section').removeClass('show-right-img');
    });
    $('.card-header .title').click(function(){
        $(this).find('.fa-chevron-down').toggleClass('rotate');
    });
    $("#bar-btn").click(function() {
        $('.mobile-links').toggleClass('ham-show');
    });
    $(".see-more button").click(function() {
        $('.course-summary .row .col.learning-points').toggleClass('height-630');
        $('.course-summary .row .col.learning-points .learning-accordions').toggleClass('height-95');
        $('.course-summary .row .see-more').toggleClass('see-less');
        $(this).find('.fa-arrow-down').toggleClass('rotate');
        var toggleText = $(this).closest('.see-more').find('.toggle_text');
        if (toggleText.text() === 'See less') {
            toggleText.text('Show more');
        } else {
            toggleText.text('See less');
        }
    });
    $(".learning-accordions #accordion .card .card-header").click(function() {
        $('.course-summary .row .col.learning-points').toggleClass('height-630');
        $('.course-summary .row .col.learning-points .learning-accordions').toggleClass('height-95');
        $(this).find('.fa-arrow-down').toggleClass('rotate');
    });

});

