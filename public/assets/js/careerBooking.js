$(document).ready(function(){
    $('#career-form').submit(function(e) {
        e.preventDefault(); // Prevent form submission

        // Clear previous error messages
        $('.error').text('');

        // Get form values
        var name = $('#career_name').val();
        var email = $('#career_email').val();
        var cvFile = $('#cv').val();

        // Validation logic
        var isValid = true;

        if (name.trim() === '') {
            $('#career_name').next('.error').text('Please enter your name.');
            isValid = false;
        }

        if (email.trim() === '') {
            $('#career_email').next('.error').text('Please enter your email.');
            isValid = false;
        } else if (!isValidEmail(email)) {
            $('#career_email').next('.error').text('Please enter a valid email address.');
            isValid = false;
        }

        if (cvFile.trim() === '') {
            $('#cv').next('.error').text('Please upload your CV.');
            isValid = false;
        } else {
            // Get the file extension
            var fileExtension = cvFile.split('.').pop().toLowerCase();

            // Allowed file extensions
            var allowedExtensions = ['jpg', 'jpeg', 'png', 'pdf', 'doc', 'docx'];

            // Check if the file extension is allowed
            if (!allowedExtensions.includes(fileExtension)) {
                $('#cv').next('.error').text('Please upload an image, PDF, or Word file.');
                isValid = false;
            }
        }

        if (isValid) {
            this.submit();
        }
    });
     // Function to validate email format
     function isValidEmail(email) {
        var emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return emailRegex.test(email);
    }
});
