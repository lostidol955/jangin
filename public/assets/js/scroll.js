//To enable scroll up using arrow button
const button = document.querySelector('.scroll-btn');
button.addEventListener('click', () => {
  window.scrollTo({top: 0, behavior: 'smooth'});
});
window.addEventListener("scroll", stickyTop, { passive: false });
//Enable sticky navbar

function stickyTop() {
    var sticky = document.querySelectorAll(".sticky");

    for (var i = 0; i < sticky.length; i++) {
      var windowHeight = window.innerHeight;
      var elementTop = sticky[i].getBoundingClientRect().top;
      var elementVisible = 10;

      if (elementTop < windowHeight - elementVisible && window.scrollY > 0) {
        sticky[i].classList.add("stick");
        sticky[i].style.transition = "all 0.3s ease";
      } else {
        sticky[i].classList.remove("stick");
        sticky[i].style.transition = "all 0.3s ease";
      }
    }
  }



//For menu section
menu = document.querySelectorAll(".nav-link a i.fa-sharp.fa-solid.fa-chevron-down");
menu.forEach(function (t) {
    t.addEventListener('mouseup', function (e) {
        menu.forEach(function (e) {
            e !== t || e.classList.contains("rotate")
                ? e.classList.remove("rotate")
                : e.classList.add("rotate");
        });
    });
});
//For FAQ section
items = document.querySelectorAll(".faq-section .faq-accordions .title");
items.forEach(function (t) {
    t.addEventListener("click", function (e) {
        items.forEach(function (e) {
            e !== t || e.classList.contains("open")
                ? e.classList.remove("open")
                : e.classList.add("open");
        });
    });
});
//For number increasing animation
runAnimation();
window.addEventListener('scroll', function() {
  let valueDisplays = document.querySelectorAll('.num');

  valueDisplays.forEach((valueDisplay) => {
    let rect = valueDisplay.getBoundingClientRect();
    let isVisible = rect.top < window.innerHeight && rect.bottom >= 30;

    if (isVisible && !valueDisplay.classList.contains('animated')) {
      valueDisplay.classList.add('animated');
      runAnimation();
    }
  });
});

function runAnimation() {
  let valueDisplays = document.querySelectorAll('.num');
  let interval = 5000;

  valueDisplays.forEach((valueDisplay) => {
    let startValue = 0;
    let endValue = parseInt(valueDisplay.getAttribute('data-val'));
    let duration = Math.floor(interval / endValue);
    let counter = setInterval(function() {
      startValue += 1;
      valueDisplay.textContent = startValue;
      if (startValue == endValue) {
        clearInterval(counter);
      }
    }, duration);
  });
}

//scroll to top
document.addEventListener("DOMContentLoaded", function() {
    // Get the scroll button element
    var scrollButton = document.querySelector(".scroll-btn");

    // Show the scroll button when the screen is scrolled
    window.addEventListener("scroll", function() {
      // Check if the scroll position is greater than 0
      if (window.pageYOffset > 0) {
        // Display the scroll button
        scrollButton.style.display = "block";
      } else {
        // Hide the scroll button
        scrollButton.style.display = "none";
      }
    });
  });




