$(document).ready(function(){
     // Set the maximum character count
     var maxCharCount = 500;

     // Update character count on input
     $('#description').on('input', function() {
         var currentCharCount = $(this).val().length;
         var remainingCharCount = maxCharCount - currentCharCount;

         // Update the character count display
         $('#charCount').text('Max character ' + remainingCharCount);

         // Restrict text input if the character count reaches 0
         if (remainingCharCount <= 0) {
             $(this).val(function(_, val) {
                 return val.substr(0, maxCharCount);
             });
         }
     });
    $('#contactUsForm').submit(function(event) {
        event.preventDefault(); // Prevent the form from submitting
        // Clear previous error messages
        $('#contactUsForm .input-field input').removeClass('error');
        $('#contactUsForm .input-field textarea').removeClass('error');

        // Check if character count is less than 0
        var currentCharCount = $('#description').val().length;
        var remainingCharCount = maxCharCount - currentCharCount;
        if (remainingCharCount < 0) {
            event.preventDefault();
            return; // Stop further execution
        }
        // Validate the form fields
        var isValid = true;


        // Validate Full Name
        var name = $('#contact_name').val().trim();
        if (name === '') {
            $('#contact_name').addClass('error');
            $('#contact_name').attr('placeholder', 'Full Name*');
            isValid = false;
        }

        // Validate Mobile
        var phone = $('#contact_phone').val().trim();
        if (phone === '') {
            $('#contact_phone').addClass('error');
            $('#contact_phone').attr('placeholder', 'Mobile Number*');
            isValid = false;
        }
        // Validate Address
        var address = $('#contact_address').val().trim();
        if (address === '') {
            $('#contact_address').addClass('error');
            $('#contact_address').attr('placeholder', 'Address*');
            isValid = false;
        }

        // Validate Email
        var email = $('#contact_email').val().trim();
        if (email === '') {
            $('#contact_email').addClass('error');
            $('#contact_email').attr('placeholder', 'Email Addess*');
            isValid = false;
        } else if (!isValidEmail(email)) {
            $('#contact_email').addClass('error');
            $('#contact_email').val('');
            $('#contact_email').attr('placeholder', 'Invalid Email');
            isValid = false;
        }
        // Validate Description
        var description = $('#description').val().trim();
        if (description === '') {
            $('#description').addClass('error');
            $('#description').attr('placeholder', 'Type your message here*');
            isValid = false;
        }

        // Validate Terms and Conditions checkbox
        var termsChecked = $('#terms').is(':checked');
        if (!termsChecked) {
            $('#terms').addClass('error');
            isValid = false;
}
        // Submit the form if valid
        if (isValid) {
            this.submit();
        } else {
            console.log('Form is not valid. Submission prevented.');
        }
    });
    $('#appointmentForm').submit(function(event) {
        event.preventDefault(); // Prevent the form from submitting
        // Clear previous error messages
        $('#contactUsForm .input-field input').removeClass('error');
        $('#contactUsForm .input-field textarea').removeClass('error');

        // Check if character count is less than 0
        var currentCharCount = $('#description').val().length;
        var remainingCharCount = maxCharCount - currentCharCount;
        if (remainingCharCount < 0) {
            event.preventDefault();
            return; // Stop further execution
        }
        // Validate the form fields
        var isValid = true;


        // Validate Full Name
        var name = $('#contact_name').val().trim();
        if (name === '') {
            $('#contact_name').addClass('error');
            $('#contact_name').attr('placeholder', 'Full Name*');
            isValid = false;
        }

        // Validate Mobile
        var phone = $('#contact_phone').val().trim();
        if (phone === '') {
            $('#contact_phone').addClass('error');
            $('#contact_phone').attr('placeholder', 'Mobile Number*');
            isValid = false;
        }
        // Validate Address
        var address = $('#contact_address').val().trim();
        if (address === '') {
            $('#contact_address').addClass('error');
            $('#contact_address').attr('placeholder', 'Address*');
            isValid = false;
        }

        // Validate Email
        var email = $('#contact_email').val().trim();
        if (email === '') {
            $('#contact_email').addClass('error');
            $('#contact_email').attr('placeholder', 'Email Addess*');
            isValid = false;
        } else if (!isValidEmail(email)) {
            $('#contact_email').addClass('error');
            $('#contact_email').val('');
            $('#contact_email').attr('placeholder', 'Invalid Email');
            isValid = false;
        }
        // Validate Description
        var description = $('#description').val().trim();
        if (description === '') {
            $('#description').addClass('error');
            $('#description').attr('placeholder', 'Type your message here*');
            isValid = false;
        }
        // Validate date
        var date = $('#date').val().trim();
        if (date === '') {
            $('#date').addClass('error');
            $('#date').attr('placeholder', 'Date*');
            isValid = false;
        }

        // Validate Terms and Conditions checkbox
        var termsChecked = $('#terms').is(':checked');
        if (!termsChecked) {
            $('#terms').addClass('error');
            isValid = false;
}
        // Submit the form if valid
        if (isValid) {
            this.submit();
        } else {
            console.log('Form is not valid. Submission prevented.');
        }
    });
     // Function to validate email format
     function isValidEmail(email) {
        var emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return emailRegex.test(email);
    }
});
