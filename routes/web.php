<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\SeoController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\FaqController;
use App\Http\Controllers\StoryController;
use App\Http\Controllers\EnquiryController;
use App\Http\Controllers\ContactEmailController;
use App\Http\Controllers\OfferController;
use App\Http\Controllers\TaglineController;
use App\Http\Controllers\CourseDetailController;
use App\Http\Controllers\CourseLearningController;
use App\Http\Controllers\CourseContentController;
use App\Http\Controllers\SyllabusController;
use App\Http\Controllers\ContactInfoController;
use App\Http\Controllers\GalleryController;
use App\Http\Controllers\BodyContentController;
use App\Http\Controllers\LowerBodyController;
use App\Http\Controllers\SideBannerController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\PrivacyPolicyController;
use App\Http\Controllers\BotManController;
use App\Http\Controllers\CareerController;
use App\Http\Controllers\CareerDetailsController;
use App\Http\Controllers\CareerBookingController;
use App\Http\Controllers\EmailBookController;
use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\TestimonialController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::prefix('/admin')->group(function(){
    Route::get('/login',[AuthController::class,'login'])->name('login');
    Route::post('/authenticate',[AuthController::class,'authenticate'])->name('verify');
    Route::post('/logout',[AuthController::class,'logout'])->name('logout');
    //Routes for accounts
    Route::resource('/account',AuthController::class)->middleware('auth');;
    //Routes for courses
    Route::resource('/course',CourseController::class)->middleware('auth');;
    //Routes for SEO
    Route::resource('/seo',SeoController::class)->middleware('auth');;
    //Routes for services
    Route::resource('/services',ServiceController::class)->middleware('auth');;
    //Routes for Faq
    Route::resource('/faq',FaqController::class)->middleware('auth');;
    //Routes for Stories
    Route::resource('/stories',StoryController::class)->middleware('auth');;
    //Routes for Enquiry
    Route::resource('/enquiry',EnquiryController::class)->middleware('auth');;
    //Routes for Enquiry
    Route::resource('/offer',OfferController::class)->middleware('auth');;
    //Routes for Page Tagline
    Route::resource('/tagline',TaglineController::class)->middleware('auth');;
    //Routes for Course details
    Route::resource('/details',CourseDetailController::class)->middleware('auth');;
    //Routes for Course contents
    Route::resource('/contents',CourseContentController::class)->middleware('auth');;
    //Routes for Course learnings
    Route::resource('/learnings',CourseLearningController::class)->middleware('auth');;
    //Routes for Course syllabus
    Route::resource('/syllabus',SyllabusController::class)->middleware('auth');;
    //Routes for Contact Details
    Route::resource('/contactInfo',ContactInfoController::class)->middleware('auth');;
    //Routes for Gallery
    Route::resource('/gallery',GalleryController::class)->middleware('auth');;
    //Routes for Body Content
    Route::resource('/body_content',BodyContentController::class)->middleware('auth');;
    //Routes for Lower Body
    Route::resource('/lower_body',LowerBodyController::class)->middleware('auth');;
    //Routes for Bookings
    Route::resource('/bookings',BookingController::class)->middleware('auth');;
    //Routes for Side Banner
    Route::resource('/side_banner',SideBannerController::class)->middleware('auth');;
    //Routes for Privacy policy
    Route::resource('/privacy',PrivacyPolicyController::class)->middleware('auth');;
    //Routes for Contacts
    Route::resource('/contacts',ContactEmailController::class)->middleware('auth');;
    //Routes for Career
    Route::resource('/career',CareerController::class)->middleware('auth');;
    //Routes for Career
    Route::resource('/career_details',CareerDetailsController::class)->middleware('auth');;
    //Routes for Career bookings
    Route::resource('/career_booking',CareerBookingController::class)->middleware('auth');;
    //Routes for email book
    Route::resource('/email',EmailBookController::class)->middleware('auth');;
    //Routes for appointment
    Route::resource('/appointment',AppointmentController::class)->middleware('auth');;
    //Routes for testimonial
    Route::resource('/testimonial',TestimonialController::class)->middleware('auth');;
});

// Routes for user activities
Route::get('/', [PageController::class,'index'])->name('home');
Route::get('/web', [PageController::class,'websites'])->name('web');
Route::get('/design', [PageController::class,'designs'])->name('design');
Route::get('/marketing', [PageController::class,'marketings'])->name('marketing');
Route::get('/programs', [PageController::class,'programs'])->name('programs');
Route::get('/application', [PageController::class,'application'])->name('application');
Route::get('/programs/{slug}', [PageController::class,'courses']);

// Store business enquiry form
Route::post('/enquiry',[PageController::class,'storeEnquiry'])->name('enquiry.submit');
Route::post('/storecontact',[PageController::class,'storeContact'])->name('message.submit');
Route::post('/sendmessage',[PageController::class,'sendMessage'])->name('message.send');
Route::get('/contacts',[PageController::class,'contacts'])->name('contacts');
Route::get('/about',[PageController::class,'about'])->name('about');

//Book course
Route::post('/book',[PageController::class,'book'])->name('course.book');

//Search feature for course
Route::get('/search', [PageController::class, 'search'])->name('course.search');
Route::get('/search-results', [PageController::class, 'showSearchResults'])->name('search-results');




//Privacy
Route::get('/privacy',[PageController::class,'privacy'])->name('privacy');
//Accessbility
Route::get('/accessibility',[PageController::class,'accessibility'])->name('accessibility');
//Disclaimer
Route::get('/disclaimer',[PageController::class,'disclaimer'])->name('disclaimer');
//Terms of use
Route::get('/terms-of-use',[PageController::class,'termsOfUse'])->name('terms-of-use');
//Services
Route::get('/service/{slug}',[PageController::class,'service'])->name('service');
//Site map
Route::get('/site-map',[PageController::class,'siteMap'])->name('site-map');
//Help Center
Route::get('/help-center',[PageController::class,'helpCenter'])->name('help-center');
//Career
Route::get('/career',[PageController::class,'career'])->name('career');
Route::get('/career/{slug}',[PageController::class,'careerDetails'])->name('career-details');
//Book Career
Route::post('/book-career',[PageController::class,'bookCareer'])->name('career.book');
//Appointment
Route::get('/appointment',[PageController::class,'appointment'])->name('appointment');
Route::post('/book-appointment',[PageController::class,'bookAppointment'])->name('appointment.book');

//Chatbot
Route::match(['get', 'post'], 'botman', [BotManController::class, 'handle']);
//Subscribe
Route::post('/subscribe',[PageController::class,'subscribe'])->name('subscribe');
