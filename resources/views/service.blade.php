<x-homelayout>
    @slot('headerSeo')
        @if (empty($seoheader))
        @else
            @foreach ($seoheader as $header)
                {!! $header->code !!}
            @endforeach
        @endif
    @endslot
    <x-sweetalert />
    <x-navbar :webtypes="$webtypes" :designtypes="$designtypes" :marketingtypes="$marketingtypes" :courses="$courses" :applicationtypes="$applicationtypes"
        :offers="$offers" />
    @if (!empty($tagline))
        <!-- Overview section -->
        <section id="overview-body">
            <div class="overview-body mt-3" style="background: {{ $tagline->color ?? '' }}">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-6 left-section mt-5">
                        <div class="left-content py-5 pl-5" data-aos="fade-right" data-aos-anchor-placement="top-bottom">
                            <h5 class="pr-3 text-white slideInLeft">Overview</h5>
                            <h1 class="mt-4 mb-5  text-white text-capitalize pr-5">
                                {{ $tagline->title ?? '' }}
                            </h1>
                            <div class="number">
                                <p class="text-white">Call us today at <span
                                        class="tel d-block text-white">+977-9802821147</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-6 right-section p-0" data-aos="fade-left"
                        data-aos-anchor-placement="top-bottom"
                        style="background: url({{ asset('storage/' . ($tagline ? $tagline->image : '')) }}) no-repeat;
                        background-position: center;
                        background-size: cover;">
                    </div>

                </div>
                <!-- scroll up button -->
                <div class="scroll">
                    <button class="scroll-btn">
                        <i class="fa-solid fa-chevron-up"></i>
                    </button>
                </div>
                <!-- scroll up button -->
            </div>
        </section>
        <!-- Overview section -->
        <!-- section weboption -->
        @if (!empty($bodycontent))
            <section id="weboption">
                <div class="weboption">
                    <div class="heading" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                        <h1>{{ $bodycontent->title ?? '' }} </h1>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-6 left-body px-5" data-aos="slide-up"
                            data-aos-anchor-placement="top-bottom">
                            <p class="description mb-2">{{ $firstParagraph ?? '' }}</p>
                            <img src="{{ asset('storage/' . ($bodycontent ? $bodycontent->image : '')) }} "
                                class="w-100" alt="service-image">
                        </div>
                        <div class="col-12 col-md-12 col-lg-6 right-body" data-aos="slide-up"
                            data-aos-anchor-placement="top-bottom">
                            <p class="description first mb-4">
                                {{ $firstParagraph ?? '' }}
                                <span class="d-block mt-4"> {{ $secondParagraph ?? '' }}</span>
                            </p>
                        </div>
                        <div class="bottom-content px-3" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                            <p class="description mt-5 mb-4">{{ $thirdParagraph ?? '' }}</p>
                        </div>
                    </div>
                </div>
            </section>
        @else
        @endif
        <!-- section weboption -->
        <!-- Business need section -->
        @if (!empty($lowerBody))
            <section id="business">
                <div class="business web mt-4">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-5 left-section">
                            <div class="left-content" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                                <h1 class="pr-3 text-white revealX">{{ $lowerBody->title ?? '' }}</h1>
                                <p class="mt-4 mb-2 description text-white revealX">
                                    {{ $lowerBody->description ?? '' }}</p>
                                {{-- <div class="start-project mb-3">
                                    <button class="start-btn" id="start-btn" style="background: #e71b45">Start a
                                        Project</button>
                                </div> --}}
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-7 right-section" data-aos="slide-up"
                            data-aos-anchor-placement="top-bottom"
                            style="background: url({{ asset('storage/' . ($lowerBody ? $lowerBody->image : '')) }}) no-repeat;
                            background-position: center;
                            background-size: cover;">
                        </div>
                    </div>
                </div>
            </section>
        @else
        @endif
        <!-- section sidebar -->
        <section id="sidebar">
            <div class="sidebar">
                <div class="row enquiry-form d-flex flex-row-reverse">
                    <div class="col-12 col-md-12 col-lg-6 left-section pb-5">
                        <div class="left-content mt-5">
                            <div class="header pl-5 d-flex justify-content-between align-items-center">
                                <img src="{{ asset('assets/images/logo.png') }}" alt="company-logo" width="200">
                                <button class="close-btn mr-3" id="close-btn"><i
                                        class="fa-solid fa-circle-xmark"></i></button>
                            </div>
                            <form action="{{ route('enquiry.submit') }}" method="POST" id="enquiryForm"
                                class="mt-5 px-4">
                                @csrf
                                <div class="input-field">
                                    <input class="w-100" type="text" name="name" id="full_name"
                                        placeholder="Full Name*" />
                                </div>
                                <div class="input-field">
                                    <input class="w-100" type="text" name="email" id="email_address"
                                        placeholder="Email*" />
                                </div>
                                <div class="input-field">
                                    <input class="w-100" type="text" name="phone" id="phone_number"
                                        placeholder="Mobile/Phone*" />
                                    <input type="hidden" name="type" id="type" value="business" />
                                </div>
                                <div class="input-field">
                                    <input class="w-100" type="text" name="country" id="country"
                                        placeholder="Address*" />
                                </div>
                                <div class="input-field">
                                    <textarea class="w-100 position-relative" name="description" id="description" rows="5"
                                        placeholder="Explain the project you want*"></textarea>
                                    <p id="charCount" class="text-black d-flex justify-content-end">Max character 500
                                    </p>
                                </div>
                                <div class="form-check mt-3">
                                    <input type="checkbox" class="form-check-input mt-1" id="terms_condition">
                                    <label class="form-check-label terms text-black" for="terms_condition">I have read
                                        Privacy
                                        Policy
                                        and
                                        agree
                                        to
                                        the
                                        terms and conditions.*</label>
                                </div>
                                <button type="submit" class="submit-btn w-100">Submit</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-6 right-section">
                        <img src="{{ $sideBanner ? asset('storage/' . $sideBanner->image) : '' }}" alt="banner"
                            class="w-100">
                    </div>
                </div>
            </div>
        </section>
        <!-- section sidebar -->
        <!-- Business need section -->
        <!-- section contact -->
        <section id="contact">
            <div class="contact bg-white">
                <div class="heading text-center" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                    <h1><b>Need help creating your website?</b></h1>
                    <p class="mt-2 mb-5">Go pro services and support from our team.</p>
                </div>
                <div class="form mb-5" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                    <form action="{{ route('message.submit') }}" id="contactForm" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-auto mb-4">
                                <input type="text" placeholder="Full Name*" class="form-input" name="name"
                                    id="fname">
                            </div>
                            <div class="col-auto mb-4">
                                <input type="text" placeholder="Mobile*" class="form-input" name="phone"
                                    id="phone_no">
                            </div>
                            <div class="col-auto mb-4">
                                <input type="text" placeholder="Email*" class="form-input" name="email"
                                    id="user_email">
                                <input type="hidden" name="type" id="type" value="business">
                            </div>
                            <div class="col-auto sumit-btn">
                                <button class="submit" type="submit">Let's Talk</button>
                            </div>
                        </div>
                    </form>
                </div>
                <h5 class="bottom-text text-center">Fill out our contact form, and we’ll give you a call.</h5>
            </div>
        </section>
        <!-- section contact -->
        <!-- section stories -->
        <x-story :stories="$stories" />
        <!-- section stories -->
    @else
        <div class="container py-5 my-5">
            <div class="title">
                <h1> This page is under maintainance...</h1>
            </div>
            <div class="description">
                <h5>Please be patience</h5>
            </div>
        </div>
    @endif
    <!--  section Footer -->
    <x-footer :courses="$courses" />
    @slot('footerSeo')
        @if (empty($seofooter))
        @else
            @foreach ($seofooter as $footer)
                {!! $footer->code !!}
            @endforeach
        @endif
    @endslot
</x-homelayout>
