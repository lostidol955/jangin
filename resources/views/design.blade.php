<x-homelayout>
    @slot('headerSeo')
        @if (empty($seoheader))
        @else
            @foreach ($seoheader as $header)
                {!! $header->code !!}
            @endforeach
        @endif
    @endslot
    <x-sweetalert />
    <x-navbar :webtypes="$webtypes" :designtypes="$designtypes" :marketingtypes="$marketingtypes" :courses="$courses" :applicationtypes="$applicationtypes"
        :offers="$offers" />
    <!-- Overview section -->
    <section id="overview-body">
        <div class="overview-body design-overview mt-3">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-6 left-section mt-5">
                    <div class="left-content py-5" data-aos="fade-right" data-aos-anchor-placement="top-bottom">
                        <h5 class="pr-3 text-white">Overview</h5>
                        <h1 class="mt-4 mb-5  text-white text-capitalize drop-in">
                            {{ $designtagline->title ?? '' }}
                        </h1>
                        <div class="number">
                            <p class="text-white">Call us today at <span class="tel d-block">+977-9802821147</span></p>
                        </div>
                    </div>
                </div>
                @if (!empty($designtagline))
                    <div class="col-12 col-md-12 col-lg-6 right-section p-0" data-aos="fade-left"
                        data-aos-anchor-placement="top-bottom"
                        style="background: url({{ asset('storage/' . ($designtagline ? $designtagline->image : '')) }}) no-repeat;
                        background-position: center;
                        background-size: cover;">
                    </div>
                @else
                @endif
            </div>
            <!-- scroll up button -->
            <div class="scroll">
                <button class="scroll-btn">
                    <i class="fa-solid fa-chevron-up"></i>
                </button>
            </div>
            <!-- scroll up button -->
        </div>
    </section>
    <!-- Overview section -->
    <!-- section weboption -->
    <section id="weboption">
        <div class="weboption graphics-title">
            <div class="heading">
                <h1 data-aos="slide-up" data-aos-anchor-placement="top-bottom">What Rangin is offering as <span
                        class="mid-text d-block">a graphic
                        design
                        service?</span> </h1>
                <p class="graphics-paragraph" data-aos="slide-up" data-aos-anchor-placement="top-bottom">We offer
                    top-notch graphic design services
                    to transform your brand's visual presence.
                    From captivating logos to engaging
                    marketing materials, our skilled team
                    combines creativity and technical expertise
                    to deliver stunning designs that
                    resonate with your target audience.</p>
            </div>
        </div>
        <div class="webtypes graphic-service">
            <div class="row">
                @if ($designtypes->count() == 0)
                    <div class="pb-5 px-3" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                        <h5>No design types found </h5>
                    </div>
                @else
                    @foreach ($designtypes as $designtype)
                        <div class="col-12 col-md-12 col-lg-6 columns" data-aos="slide-up"
                            data-aos-anchor-placement="top-bottom">
                            <h1 class="title ">{{ $designtype->name }}</h1>
                            <div class="content">
                                <p class="description mt-4">{!! strip_tags($designtype->description) !!}</p>
                            </div>
                            <div class="learn mt-4">
                                <a href="/service/{{ $designtype->slug }}">
                                    <button class="learn-btn">Learn More</button>
                                </a>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
    <!-- Business need section -->
    <section id="business">
        <div class="business design-overview bg-white mt-2">
            <div class="row" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                @if (!empty($designcontent))
                    <div class="col-12 col-md-12 col-lg-7 right-section p-0"
                        style="background: url({{ asset('storage/' . ($designcontent ? $designcontent->image : '')) }}) no-repeat;
                background-position: center;
                background-size: cover;">
                    @else
                @endif
            </div>
            <div class="col-12 col-md-12 col-lg-5 left-section">
                <div class="left-content">
                    <h1 class="pr-3 revealX">{{ $designcontent->title ?? '' }}</h1>
                    <div class="list-items">
                        @if (!empty($designcontent->description))
                            @foreach (preg_split("/\r\n|\n|\r/", $designcontent->description) as $points)
                                <p class="lists"><img src="assets/images/tick.png" width="30" class="mr-2"
                                        alt="tick">
                                    {{ trim($points) }}
                                </p>
                            @endforeach
                        @else
                        @endif
                        <p class="my-4 description">Overall, investing in graphic design can help
                            businesses create a strong and consistent brand
                            identity, effectively communicate their message,
                            and ultimately drive success and growth.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Business need section -->
    {{-- Design lower content --}}
    @if (!empty($designLowerContent))
        <section id="business">
            <div class="business web mt-4">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-5 left-section">
                        <div class="left-content">
                            <h1 class="pr-3 text-white revealX">{{ $designLowerContent->title ?? '' }}</h1>
                            <p class="my-4 description text-white revealX">
                                {{ $designLowerContent->description ?? '' }}</p>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-7 right-image"
                        style="background: url({{ asset('storage/' . ($designLowerContent ? $designLowerContent->image : '')) }}) no-repeat;
                        background-position: center;
                        background-size: cover;">
                    </div>
                </div>
            </div>
        </section>
    @else
    @endif
    {{-- Design lower content --}}
    <!-- section contact -->
    <section id="contact">
        <div class="contact bg-white">
            <div class="heading text-center" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                <h1><b>Need help creating your website?</b></h1>
                <p class="mt-2 mb-5">Go pro services and support from our team.</p>
            </div>
            <div class="form mb-5" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                <form action="{{ route('message.submit') }}" id="contactForm" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-auto mb-4">
                            <input type="text" placeholder="Full Name*" class="form-input" name="name"
                                id="fname">
                        </div>
                        <div class="col-auto mb-4">
                            <input type="text" placeholder="Mobile*" class="form-input" name="phone"
                                id="phone_no">
                        </div>
                        <div class="col-auto mb-4">
                            <input type="text" placeholder="Email*" class="form-input" name="email"
                                id="user_email">
                            <input type="hidden" name="type" id="type" value="visitor">
                        </div>
                        <div class="col-auto sumit-btn">
                            <button class="submit" type="submit">Let's Talk</button>
                        </div>
                    </div>
                </form>
            </div>
            <h5 class="bottom-text text-center">Fill out our contact form, and we’ll give you a call.</h5>
        </div>
    </section>
    <!-- section contact -->
    <!-- section stories -->
    <x-story :stories="$stories" />
    <!-- section stories -->
    <!--  section Footer -->
    <x-footer :courses="$courses" />
    @slot('footerSeo')
        @if (empty($seofooter))
        @else
            @foreach ($seofooter as $footer)
                {!! $footer->code !!}
            @endforeach
        @endif
    @endslot
</x-homelayout>
