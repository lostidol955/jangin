<x-layout>
    <div class="pc-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <x-breadcrumbs>
                <li class="breadcrumb-item">Booking</li>
                <li class="breadcrumb-item">Edit</li>
            </x-breadcrumbs>
            <!-- [ breadcrumb ] end -->
            <x-card>
                <div class="card-body d-flex justify-content-between pb-0">
                    <h6 class="card-title add-page"><b>Edit Booking</b></h6>
                    <a href="/admin/bookings" class="text-bold text-uppercase fw-bold"> <svg
                            xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                            class="bi bi-arrow-left-circle-fill" viewBox="0 0 16 16">
                            <path
                                d="M8 0a8 8 0 1 0 0 16A8 8 0 0 0 8 0zm3.5 7.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z" />
                        </svg> Back</a>
                </div>
                <div class="container p-4">
                    @if ($errors->has('course_id'))
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            {{ $errors->first('course_id') }}
                        </div>
                    @endif
                    <form method="POST" action="/admin/bookings/{{ $booking->slug }}">
                        @csrf
                        @method('PUT')
                        <div class="form-outline">
                            <label class="form-label" for="course_id">Course</label>
                            <select class="form-control" name="course_id" id="course_id" role="button">
                                <option value="{{ $booking->course_id }}">{{ $booking->course->name }}
                                </option>
                                @foreach ($courses as $course)
                                    <option value="{{ $course->id }}">{{ $course->name }}
                                    </option>
                                @endforeach
                            </select>
                            @error('position')
                                <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                            @enderror
                            <div class="form-outline my-4">
                                <label class="form-label" for="name">Name</label>
                                <input type="text" id="name" name="name" class="form-control"
                                    value="{{ $booking->name }}" />
                                @error('name')
                                    <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-outline mb-4">
                                <label class="form-label" for="email">Email</label>
                                <input type="text" id="email" name="email" class="form-control"
                                    value="{{ $booking->email }}" />
                                @error('email')
                                    <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-outline mb-4">
                                <label class="form-label" for="address">Address</label>
                                <input type="text" id="address" name="address" class="form-control"
                                    value="{{ $booking->address }}" />
                                @error('address')
                                    <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-outline mb-4">
                                <label class="form-label" for="phone">Phone</label>
                                <input type="text" id="phone" name="phone" class="form-control"
                                    value="{{ $booking->phone }}" />
                                @error('phone')
                                    <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-outline mb-4">
                                <label class="form-label" for="qualification">Qualification</label>
                                <select class="form-control" name="qualification" id="qualification" role="button">
                                    <option value="{{$booking->qualification}}" class="text-capitalize">{{$booking->qualification}}</option>
                                    <option value="see" class="text-capitalize">SEE Level</option>
                                    <option value="+2" class="text-capitalize">+2 Level</option>
                                    <option value="bachelor" class="text-capitalize">Bachelor Level</option>
                                </select>
                                @error('qualification')
                                    <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-outline mb-4">
                                <label class="form-label" for="date">Date</label>
                                <input type="date" id="date" name="date" class="form-control"
                                    value="{{ $booking->date }}" />
                                @error('date')
                                    <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-outline mb-4">
                                <label class="form-label" for="time">Time</label>
                                <input type="time" id="time" name="time" class="form-control"
                                    value="{{ $booking->time }}" />
                                @error('time')
                                    <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-primary btn-block mb-4">Update</button>
                    </form>
                </div>
            </x-card>
            <!-- [ Main Content ] end -->
        </div>
    </div>
</x-layout>
