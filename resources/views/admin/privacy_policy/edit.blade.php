<x-layout>
    <div class="pc-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <x-breadcrumbs>
                <li class="breadcrumb-item">Privacy Policy</li>
                <li class="breadcrumb-item">Edit</li>
            </x-breadcrumbs>
            <!-- [ breadcrumb ] end -->
            <x-card>
                <div class="card-body d-flex justify-content-between pb-0">
                    <h6 class="card-title add-page"><b>Edit</b></h6>
                    <a href="/admin/privacy" class="text-bold text-uppercase fw-bold"> <svg
                            xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                            class="bi bi-arrow-left-circle-fill" viewBox="0 0 16 16">
                            <path
                                d="M8 0a8 8 0 1 0 0 16A8 8 0 0 0 8 0zm3.5 7.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z" />
                        </svg> Back</a>
                </div>
                <div class="container p-4">
                    <form method="post" action="/admin/privacy/{{ $privacy->slug }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-outline mb-4">
                            <label class="form-label" for="title">Title</label>
                            <input type="text" id="title" name="title" class="form-control"
                                value="{{ $privacy->title }}" />
                            @error('title')
                                <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="form-outline mb-4">
                            <label class="form-label" for="description">Description</label>
                            <textarea class="form-control" id="descriptions" name="description" rows="6">{{ $privacy->description }}</textarea>
                            @error('description')
                                <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary btn-block mb-4">Update</button>
                    </form>
                </div>
            </x-card>
            <!-- [ Main Content ] end -->
        </div>
    </div>
</x-layout>
