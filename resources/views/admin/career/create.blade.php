<x-layout>
    <div class="pc-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <x-breadcrumbs>
                <li class="breadcrumb-item">Career</li>
                <li class="breadcrumb-item">Create</li>
            </x-breadcrumbs>
            <!-- [ breadcrumb ] end -->
            <x-card>
                <div class="card-body d-flex justify-content-between pb-0">
                    <h6 class="card-title add-page"><b>Create</b></h6>
                    <a href="/admin/career" class="text-bold text-uppercase fw-bold"> <svg
                            xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                            class="bi bi-arrow-left-circle-fill" viewBox="0 0 16 16">
                            <path
                                d="M8 0a8 8 0 1 0 0 16A8 8 0 0 0 8 0zm3.5 7.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z" />
                        </svg> Back</a>
                </div>
                <div class="container p-4">
                    <form method="post" action="/admin/career" enctype="multipart/form-data">
                        @csrf
                        <div class="form-outline mb-4">
                            <label class="form-label" for="job_title">Title</label>
                            <input type="text" id="job_title" name="job_title" class="form-control"
                                value="{{ old('job_title') }}" />
                            @error('job_title')
                                <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="form-outline mb-4">
                            <label class="form-label" for="job_type">Type</label>
                            <select class="form-control" name="job_type" id="job_type">
                                <option value="FullTime">Full Time</option>
                                <option value="Part Time">Part Time</option>
                            </select>
                            @error('job_type')
                                <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="form-outline mb-4">
                            <label class="form-label" for="working_day">Working Day</label>
                            <input type="text" id="working_day" name="working_day" class="form-control"
                                value="{{ old('working_day') }}" />
                            @error('working_day')
                                <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="form-outline mb-4">
                            <label class="form-label" for="working_hour">Working Hour</label>
                            <input type="text" id="working_hour" name="working_hour" class="form-control"
                                value="{{ old('working_hour') }}" />
                            @error('working_hour')
                                <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="image-area mb-4"><img id="imageResult"
                                src="http://127.0.0.1:8000/admin/assets/images/preview.png" width="150"></div>
                        <div class="form-row mb-4">
                            <label for="image" class="form-label">Upload image</label>
                            <input class="form-control" type="file" id="image" name="image" multiple
                                value="{{ old('image') }}" />
                            @error('image')
                                <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary btn-block mb-4">Create</button>
                    </form>
                </div>
            </x-card>
            <!-- [ Main Content ] end -->
        </div>
    </div>
</x-layout>
