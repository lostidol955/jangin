<x-layout>
    <div class="pc-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <x-breadcrumbs>
                <li class="breadcrumb-item">Details</li>
                <li class="breadcrumb-item">Edit</li>
            </x-breadcrumbs>
            <!-- [ breadcrumb ] end -->
            <x-card>
                <div class="card-body d-flex justify-content-between pb-0">
                    <h6 class="card-title add-page"><b>Edit Details</b></h6>
                    <a href="/admin/details" class="text-bold text-uppercase fw-bold"> <svg
                            xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                            class="bi bi-arrow-left-circle-fill" viewBox="0 0 16 16">
                            <path
                                d="M8 0a8 8 0 1 0 0 16A8 8 0 0 0 8 0zm3.5 7.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z" />
                        </svg> Back</a>
                </div>
                <div class="container p-4">
                    <form method="POST" action="/admin/details/{{ $detail->slug }}">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-12 col-sm-6 col-lg-6">
                                <div class="form-outline mb-3">
                                    <label class="form-label" for="course_id">Course</label>
                                    <select class="form-control" name="course_id" id="course_id" role="button">
                                        <option value="{{ $detail->course_id }}">{{ $detail->course->name }}</option>
                                        @if ($courses->count() == 0)
                                            <option value="#">--No course found--
                                            </option>
                                        @else
                                            @foreach ($courses as $course)
                                                <option value="{{ $course->id }}" class="text-capitalize">
                                                    {{ $course->name }}
                                                </option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @error('course_id')
                                        <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-6">
                                <div class="form-outline mb-3">
                                    <label class="form-label" for="duration">Duration</label>
                                    <input class="form-control" id="duration" name="duration"
                                        value="{{ $detail->duration }}" />
                                    @error('duration')
                                        <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-6 col-lg-6">
                                <div class="form-outline mb-3">
                                    <label class="form-label" for="format">Format</label>
                                    <input class="form-control" id="format" name="format"
                                        value="{{ $detail->format }}" />
                                    @error('format')
                                        <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-6">
                                <div class="form-outline mb-3">
                                    <label class="form-label" for="timing">Timing</label>
                                    <input class="form-control" id="timing" name="timing"
                                        value="{{ $detail->timing }}" />
                                    @error('timing')
                                        <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-6 col-lg-6">
                                <div class="form-outline mb-3">
                                    <label class="form-label" for="price">Price</label>
                                    <input class="form-control" id="price" name="price"
                                        value="{{ $detail->price }}" />
                                    @error('price')
                                        <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-6">
                                <div class="form-outline mb-3">
                                    <label class="form-label" for="emis">Emis</label>
                                    <input class="form-control" id="emis" name="emis"
                                        value="{{ $detail->emis }}" />
                                    @error('emis')
                                        <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-6 col-lg-6">
                                <div class="form-outline mb-3">
                                    <label class="form-label" for="learning_hrs">Learning Hours</label>
                                    <input class="form-control" id="learning_hrs" name="learning_hrs"
                                        value="{{ $detail->learning_hrs }}" />
                                    @error('learning_hrs')
                                        <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-6">
                                <div class="form-outline mb-3">
                                    <label class="form-label" for="assignment">Assignment</label>
                                    <input class="form-control" id="assignment" name="assignment"
                                        value="{{ $detail->assignment }}" />
                                    @error('assignment')
                                        <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-6 col-lg-6">
                                <div class="form-outline mb-3">
                                    <label class="form-label" for="internship">Internship</label>
                                    <input class="form-control" id="internship" name="internship"
                                        value="{{ $detail->internship }}" />
                                    @error('internship')
                                        <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-6">
                                <div class="form-outline mb-3">
                                    <label class="form-label" for="enrollment">Enrollment</label>
                                    <input class="form-control" id="enrollment" name="enrollment"
                                        value="{{ $detail->enrollment }}" />
                                    @error('enrollment')
                                        <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary btn-block mb-4">Update</button>
                    </form>
                </div>
            </x-card>
            <!-- [ Main Content ] end -->
        </div>
    </div>
</x-layout>
