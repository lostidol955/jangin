<x-layout>
    <div class="pc-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <x-breadcrumbs>
                <li class="breadcrumb-item">Syllabus</li>
                <li class="breadcrumb-item">Index</li>
            </x-breadcrumbs>
            <!-- [ breadcrumb ] end -->
            <x-card>
                <div class="card-body">
                    <h6 class="card-title"><b>Syllabus</b></h6>
                    <div class="row mt-4">
                        <x-search />
                        <div
                            class="col-12 col-sm-6 col-lg-8 col-md-8 d-flex justify-content-end align-items-center end-bars">
                            <div class="add mr-4">
                                <a href="/admin/syllabus/create" class="text-bold text-uppercase fw-bold"> <svg
                                        xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                        fill="currentColor" class="bi bi-plus-circle-fill mr-1" viewBox="0 0 16 16">
                                        <path
                                            d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3v-3z" />
                                    </svg> Create</a>
                            </div>

                        </div>
                    </div>
                    <x-table>
                        <thead>
                            <tr>
                                <th scope="col">S.N.</th>
                                <th scope="col">Course</th>
                                <th scope="col">File</th>
                                <th scope="col">Published at</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ($syllabus->count() == 0)
                                <tr>
                                    <td colspan="5" class="text-center py-5">
                                        No data found
                                    </td>
                                </tr>
                            @else
                                @foreach ($syllabus as $syllabi)
                                    <tr>
                                        <th scope="row">{{ $loop->iteration }}</th>
                                        <td>{{ $syllabi->course->name }}</td>
                                        <td>{{ $syllabi->file }}</td>
                                        <td>{{ $syllabi->created_at }}</td>
                                        <td><a href="/admin/syllabus/{{ $syllabi->slug }}"
                                                class="d-flex align-items-center"><svg
                                                    xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                                    fill="currentColor" class="bi bi-eye-fill mr-2" viewBox="0 0 16 16">
                                                    <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z" />
                                                    <path
                                                        d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z" />
                                                </svg> Show</a></td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </x-table>
                </div>
                <div class="pagination d-flex justify-content-end mb-3 mr-4">
                    <ul class="pagination">
                        <li class="page-item">
                            {{ $syllabus->links('vendor.pagination.simple-tailwind') }}
                        </li>
                    </ul>
                </div>
            </x-card>
            <!-- [ Main Content ] end -->
        </div>
    </div>
</x-layout>
