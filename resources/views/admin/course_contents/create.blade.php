<x-layout>
    <div class="pc-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <x-breadcrumbs>
                <li class="breadcrumb-item">Contents</li>
                <li class="breadcrumb-item">Create</li>
            </x-breadcrumbs>
            <!-- [ breadcrumb ] end -->
            <x-card>
                <div class="card-body d-flex justify-content-between pb-0">
                    <h6 class="card-title add-page"><b>Contents</b></h6>
                    <a href="/admin/contents" class="text-bold text-uppercase fw-bold"> <svg
                            xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                            class="bi bi-arrow-left-circle-fill" viewBox="0 0 16 16">
                            <path
                                d="M8 0a8 8 0 1 0 0 16A8 8 0 0 0 8 0zm3.5 7.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z" />
                        </svg> Back</a>
                </div>
                <div class="container p-4">
                    <form method="POST" action="/admin/contents" enctype="multipart/form-data">
                        @csrf
                        <div class="form-outline my-4">
                            <label class="form-label" for="course_id">Course</label>
                            <select class="form-control" name="course_id" id="course_id" role="button">
                                @if ($courses->count() == 0)
                                    <option value="#">--No course found--
                                    </option>
                                @else
                                    @foreach ($courses as $course)
                                        <option value="{{ $course->id }}" class="text-capitalize">
                                            {{ $course->name }}
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                            @error('course_id')
                                <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="form-outline mb-4">
                            <label class="form-label" for="learning_points">Learning points</label>
                            <textarea class="form-control" id="learning_points" name="learning_points" rows="6">{{ old('learning_points') }}</textarea>
                            @error('learning_points')
                                <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="form-outline mb-4">
                            <label class="form-label" for="overview">Overview</label>
                            <textarea class="form-control" id="overview" name="overview" rows="4">{{ old('overview') }}</textarea>
                            @error('overview')
                                <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="form-outline mb-4">
                            <label class="form-label" for="why_so_popular">Why so popular?</label>
                            <textarea class="form-control" id="why_so_popular" name="why_so_popular" rows="4">{{ old('why_so_popular') }}</textarea>
                            @error('why_so_popular')
                                <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="form-outline mb-4">
                            <label class="form-label" for="outline">Outline</label>
                            <textarea class="form-control" id="outline" name="outline" rows="4">{{ old('outline') }}</textarea>
                            @error('outline')
                                <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="form-outline mb-4">
                            <label class="form-label" for="practicality">Practicality</label>
                            <textarea class="form-control" id="practicality" name="practicality" rows="4">{{ old('practicality') }}</textarea>
                            @error('practicality')
                                <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="form-outline mb-4">
                            <label class="form-label" for="requirements">Requirements</label>
                            <textarea class="form-control" id="requirements" name="requirements" rows="4">{{ old('requirements') }}</textarea>
                            @error('requirements')
                                <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="image-area mb-4"><img id="imagePreview"
                                src="http://127.0.0.1:8000/admin/assets/images/preview.png" width="150"></div>
                        <div class="form-row mb-4">
                            <label for="body_image" class="form-label">Body Image</label>
                            <input class="form-control" type="file" id="body_image" name="body_image" multiple
                                value="{{ old('body_image') }}" />
                            @error('body_image')
                                <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="image-area mb-4"><img id="imageResult"
                                src="http://127.0.0.1:8000/admin/assets/images/preview.png" width="150"></div>
                        <div class="form-row mb-4">
                            <label for="image" class="form-label">Tools you'll learn</label>
                            <input class="form-control" type="file" id="image" name="image" multiple
                                value="{{ old('image') }}" />
                        </div>
                        <button type="submit" class="btn btn-primary btn-block mb-4">Create</button>
                    </form>
                </div>
            </x-card>
            <!-- [ Main Content ] end -->
        </div>
    </div>
</x-layout>
