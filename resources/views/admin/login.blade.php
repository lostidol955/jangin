<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- External stylesheet -->
    <link rel="stylesheet" href=" http://127.0.0.1:8000/admin/assets/css/login.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>

<body>
    <section id="admin-login">
        <div class="login-body">
            <div class="left-section">
                <div class="left-content mt-3">
                    <div class="contact-info">
                        <h1 class="title text-center text-dark mt-3 mb-5">Login</h1>
                        @if (session()->has('error'))
                            <h5 class="text-center text-danger">
                                {{ session('error') }}
                            </h5>
                        @endif
                    </div>
                    <form action="{{ route('verify') }}" method="POST" data-aos="fade-left"
                        data-aos-anchor-placement="top-bottom">
                        @csrf
                        <div class="input-field">
                            <input class="w-100" type="text" name="email" id="email" placeholder="Email*" />
                        </div>
                        @error('email')
                            <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                        @enderror
                        <div class="input-field">
                            <input class="w-100" type="password" name="password" id="password"
                                placeholder="Password*" />
                        </div>
                        @error('password')
                            <p class="d-flex justify-content-start text-danger mt-2">{{ $message }}</p>
                        @enderror
                        <button type="submit" class="submit mt-4 w-100">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- jquery -->
    {{-- script to hide flash message --}}
    <script>
        window.addEventListener('DOMContentLoaded', function() {
            var flashError = document.getElementById('flash-error');
            // hide the flashError div after 2 seconds
            if (flashError !== null) {
                setTimeout(function() {
                    flashError.style.display = 'none';
                }, 2000);
            }
        });
    </script>
    {{-- script to hide flash message --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <!-- Include js plugin -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

</body>

</html>
