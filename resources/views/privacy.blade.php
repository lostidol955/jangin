<x-homelayout>
    @slot('headerSeo')
        @if (empty($seoheader))
        @else
            @foreach ($seoheader as $header)
                {!! $header->code !!}
            @endforeach
        @endif
    @endslot
    <x-navbar :webtypes="$webtypes" :designtypes="$designtypes" :marketingtypes="$marketingtypes" :courses="$courses" :applicationtypes="$applicationtypes"
        :offers="$offers" />
    <section id="body-content">
        <div class="privacy body-content">
            <div class="title mt-4">
                <h1 class="text-center font-weight-bold" data-aos="fade-right">Privacy Statement</h1>
            </div>
            <div class="privacy-description px-5 mt-5">
                <div class="effective-date d-flex">
                    <div class="side-box"></div>
                    <div class="title ml-4 pt-3">
                        <h3 data-aos="fade-right">Effective Date</h3>
                        <p data-aos="fade-right">{{ $privacyUpdatedDate->format('Y-m-d') ?? '' }}</p>
                    </div>
                </div>
                <div class="description my-5">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                                aria-orientation="vertical">
                                @foreach ($privacies as $index => $policy)
                                    <a class="heading mb-4{{ $index === 0 ? ' active' : '' }}"
                                        id="v-pills-{{ $policy->slug }}-tab" data-toggle="pill"
                                        href="#v-pills-{{ $policy->slug }}" role="tab"
                                        aria-controls="v-pills-{{ $policy->slug }}"
                                        aria-selected="{{ $index === 0 ? 'true' : 'false' }}" data-aos="fade-right"
                                        data-aos-anchor-placement="top-bottom">{{ $policy->title }}</a>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="tab-content" id="v-pills-tabContent">
                                @foreach ($privacies as $index => $policy)
                                    <div class="tab-pane fade{{ $index === 0 ? ' show active' : '' }}"
                                        id="v-pills-{{ $policy->slug }}" role="tabpanel"
                                        aria-labelledby="v-pills-{{ $policy->slug }}-tab">
                                        <p>{!! nl2br(e($policy->description)) !!}</p>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <!-- scroll up button -->
        <div class="scroll">
            <button class="scroll-btn">
                <i class="fa-solid fa-chevron-up"></i>
            </button>
        </div>
        <!-- scroll up button -->
    </section>
    <!-- section stories -->
    <x-story :stories="$stories" />
    <!-- section stories -->
    <!--  section Footer -->
    <x-footer :courses="$courses" />
    @slot('footerSeo')
        @if (empty($seofooter))
        @else
            @foreach ($seofooter as $footer)
                {!! $footer->code !!}
            @endforeach
        @endif
    @endslot
</x-homelayout>
