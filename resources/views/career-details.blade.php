<x-homelayout>
    @slot('headerSeo')
        @if (empty($seoheader))
        @else
            @foreach ($seoheader as $header)
                {!! $header->code !!}
            @endforeach
        @endif
    @endslot
    <x-sweetalert />
    <x-navbar :webtypes="$webtypes" :designtypes="$designtypes" :marketingtypes="$marketingtypes" :courses="$courses" :applicationtypes="$applicationtypes"
        :offers="$offers" />
    <!-- Search section -->
    <x-searchbar />
    <section id="body-content">
        <div class="body-content career">
            <div class="title pl-5 mt-4">
                <h1 class="font-weight-bold ml-1" data-aos="fade-up" data-aos-anchor-placement="top-bottom">
                    {{ $career->job_title }}
                </h1>
            </div>
            <div class="career-description px-5 mt-5">
                <div class="description my-5" data-aos="fade-up" data-aos-anchor-placement="top-bottom">
                    <p><span class="font-weight-bold">Job Category:</span> {{ $career->job_title ?? '' }}</p>
                    <p><span class="font-weight-bold">Organization:</span> Rangin Technology</p>
                    <p><span class="font-weight-bold">Job Type:</span> {{ $career->job_type ?? '' }}</p>
                    <p><span class="font-weight-bold">Location:</span> {{ $contacts->location ?? '' }}</p>
                    <p class="mt-5 mb-0">
                        {{ $details->description ?? '' }}
                    </p>
                </div>
            </div>
        </div>
        <!-- scroll up button -->
        <div class="scroll">
            <button class="scroll-btn">
                <i class="fa-solid fa-chevron-up"></i>
            </button>
        </div>
        <!-- scroll up button -->
    </section>
    @if (!empty($details))
        <!-- scroll up button -->
        <div class="scroll">
            <button class="scroll-btn">
                <i class="fa-solid fa-chevron-up"></i>
            </button>
        </div>
        <!-- scroll up button -->
        <section id="courses-info">
            <div class="career courses-info">
                <div class="courses-overview">
                    <h5 class="font-weight-bold" data-aos="fade-up" data-aos-anchor-placement="top-bottom">
                        Responsibilities
                    </h5>
                    @if (!empty($details->responsibilities))
                        <div class="description mt-4" data-aos="fade-up" data-aos-anchor-placement="top-bottom">
                            @foreach (preg_split("/\r\n|\n|\r/", $details->responsibilities) as $points)
                                <li class="mb-1"> {{ trim($points) }}</li>
                            @endforeach
                        </div>
                    @else
                    @endif
                </div>
                <div class="courses-overview mt-5">
                    <h5 class="font-weight-bold " data-aos="fade-up" data-aos-anchor-placement="top-bottom">Job
                        Requirements
                    </h5>
                    @if (!empty($details->requirements))
                        <div class="description mt-4" data-aos="fade-up" data-aos-anchor-placement="top-bottom">
                            @foreach (preg_split("/\r\n|\n|\r/", $details->requirements) as $points)
                                <li class="mb-1"> {{ trim($points) }}</li>
                            @endforeach
                        </div>
                    @else
                    @endif
                </div>
                <div class="courses-overview mt-5">
                    <h5 class="font-weight-bold " data-aos="fade-up" data-aos-anchor-placement="top-bottom">Expectation
                        in
                        the first week
                    </h5>
                    @if (!empty($details->expectation))
                        <div class="description mt-4" data-aos="fade-up" data-aos-anchor-placement="top-bottom">
                            @foreach (preg_split("/\r\n|\n|\r/", $details->expectation) as $points)
                                <li class="mb-1"> {{ trim($points) }}</li>
                            @endforeach
                        </div>
                    @else
                    @endif
                </div>
                <div class="courses-overview mt-5">
                    <h5 class="font-weight-bold " data-aos="fade-up" data-aos-anchor-placement="top-bottom">Apply
                        procedure
                    </h5>
                    <p data-aos="fade-up" data-aos-anchor-placement="top-bottom">Always opening for an intern/Trainee.
                    </p>
                    <div class="description mt-3" data-aos="fade-up" data-aos-anchor-placement="top-bottom">
                        <li class="mb-1">If you’re interested in this role, send us your resume at <a
                                href="mailto:therangin@gmail.com">therangin@gmail.com</a>.</li>
                        <li class="mb-1">You can fill the given form below.</li>
                    </div>
                    <p data-aos="fade-up" data-aos-anchor-placement="top-bottom"><span class="normal-font">If you know
                            someone talented who might be a good fit, refer them to us.
                        </span>For any queries you
                        have,
                        <span class="normal-font"><i>{{ $contacts->mobile_no ?? '' }}</i></span>
                    </p>
                    <div class="working-details">
                        <p class="m-0 font-weight-bold" data-aos="fade-up" data-aos-anchor-placement="top-bottom">
                            Salary:
                            {{ $details->salary }} Monthly</p>
                        <p class="m-0 font-weight-bold" data-aos="fade-up" data-aos-anchor-placement="top-bottom">
                            Working
                            Day: {{ $career->working_day }}</p>
                        <p class="m-0 font-weight-bold" data-aos="fade-up" data-aos-anchor-placement="top-bottom">
                            Working
                            Hour: {{ $career->working_hour }}</h5>
                    </div>
                    <div class="form-container w-50 my-5" data-aos="fade-up" data-aos-anchor-placement="top-bottom">
                        <form action="{{ route('career.book') }}" method="POST" id="career-form"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="name">Your Name</label>
                                <input type="text" class="form-control bg-light" id="career_name" name="name"
                                    placeholder="Full Name">
                                <span class="error text-danger"></span>
                            </div>
                            <input type="hidden" name="career_id" id="career_id" value="{{ $career->id }}">
                            <div class="form-group">
                                <label for="email">Your Email</label>
                                <input type="text" class="form-control bg-light" id="career_email" name="email"
                                    placeholder="Email">
                                <span class="error text-danger"></span>
                            </div>
                            <div class="form-group my-3">
                                <label for="cv">Documents (CV)</label>
                                <input type="file" class="d-block" id="cv" name="cv">
                                <span class="error text-danger"></span>
                            </div>
                            <input type="submit" class="apply-btn text-white mt-2 px-4 py-2" value="Apply Now">
                        </form>
                    </div>
                </div>

            </div>
        </section>
        <!--  section Footer -->
    @else
    @endif

    <x-footer :courses="$courses" />
    @slot('footerSeo')
        @if (empty($seofooter))
        @else
            @foreach ($seofooter as $footer)
                {!! $footer->code !!}
            @endforeach
        @endif
    @endslot
</x-homelayout>
