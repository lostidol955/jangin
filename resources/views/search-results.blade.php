<x-homelayout>
    @slot('headerSeo')
        @if (empty($seoheader))
        @else
            @foreach ($seoheader as $header)
                {!! $header->code !!}
            @endforeach
        @endif
    @endslot
    <x-navbar :webtypes="$webtypes" :designtypes="$designtypes" :marketingtypes="$marketingtypes" :courses="$courses" :applicationtypes="$applicationtypes"
        :offers="$offers" />
    <!-- Search section -->
    <x-searchbar />
    <!-- Search section -->
    <section id="search-results">
        <div class="search-results">
            <div class="results">
                @if (!empty($similarCourses) || !empty($similarServices))
                    <h2 class="title">Search results for: <span class="text-danger">{{ $searchTerm }}</span></h2>
                    <p class="result-count">Total Results: {{ count($similarCourses) + count($similarServices) }}</p>
                    <ul class="list-group list-group-flush">
                        @foreach ($similarCourses as $course)
                            <li class="list-group-item px-0"><a href="programs/{{ $course->slug }}">{{ $course->name }} -
                                    IT Course</a>
                                <p class="link">https://www.rangin.com.np/programs/{{ $course->slug }}</p>
                                {{-- <p class="tagline">{{ $course->tagline->title ?? '' }}</p> --}}
                            </li>
                        @endforeach
                        @foreach ($similarServices as $service)
                            <li class="list-group-item px-0"><a href="service/{{ $service->slug }}">{{ $service->name }}
                                    - IT Service</a>
                                <p class="link">https://www.rangin.com.np/service/{{ $service->slug }}</p>
                                <p class="tagline">{{ $service->tagline->title ?? '' }}</p>
                            </li>
                        @endforeach
                    </ul>
                @else
                    <p class="unknown-result">No search results for: {{ $searchTerm }}</p>
                @endif
            </div>
        </div>
        <!-- scroll up button -->
        <div class="scroll">
            <button class="scroll-btn">
                <i class="fa-solid fa-chevron-up"></i>
            </button>
        </div>
        <!-- scroll up button -->
    </section>
    <x-footer :courses="$courses" />
    @slot('footerSeo')
        @if (empty($seofooter))
        @else
            @foreach ($seofooter as $footer)
                {!! $footer->code !!}
            @endforeach
        @endif
    @endslot
</x-homelayout>
