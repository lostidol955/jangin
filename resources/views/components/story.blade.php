 <!-- section stories -->
 <section id="stories">
     <div class="stories">
         <div class="stories-content">
             <div class="heading" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                 <h1><b>Stories</b> from rangin</h1>
             </div>
             <div class="row gx-5 owl-carousel owl-theme mt-5 revealX">
                 @if ($stories->count() == 0)
                     <div class="pb-5 px-3">
                         <h5 data-aos="slide-up" data-aos-anchor-placement="top-bottom">No stories found </h5>
                     </div>
                 @else
                     @foreach ($stories as $story)
                         <a href="{{ $story->link }}" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                             <div class="col" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                                 <div class="card">
                                     <img src="{{ asset('storage/' . $story->image) }}" class="card-img-top"
                                         alt="story">
                                     <div class="card-body">
                                         <h5 class="card-title"><b>{{ $story->title }}</b></h5>
                                         <p class="card-text">{{ $story->description }}</p>
                                     </div>
                                 </div>
                             </div>
                         </a>
                     @endforeach
                 @endif
             </div>
         </div>
     </div>
 </section>
 <!-- section stories -->
