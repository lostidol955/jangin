<!DOCTYPE html>
<html lang="en">

<head>
    <title>Rangin | Dashboard</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Favicon icon -->
    <link rel="icon" href="{{ asset('admin/assets/images/favicon.svg') }}" type="image/x-icon">

    <!-- font css -->

    <!-- Option 1: Include in HTML -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

    <!-- vendor css -->
    <link rel="stylesheet" href="{{ asset('admin/assets/css/style.css') }}" id="main-style-link">
    <script src="https://cdn.ckeditor.com/ckeditor5/38.0.1/classic/ckeditor.js"></script>
</head>

<body>
    <!-- [ Mobile header ] start -->
    <div class="pc-mob-header pc-header">
        <div class="pcm-logo">
            <img src="{{ asset('admin/assets/images/logo-2.png') }}" alt="logo" class="logo logo-lg">
        </div>
        <div class="pcm-toolbar">
            <a href="#!" class="pc-head-link" id="mobile-collapse">
                <div class="hamburger hamburger--arrowturn">
                    <div class="hamburger-box">
                        <div class="hamburger-inner"></div>
                    </div>
                </div>
            </a>

            <a href="#!" class="pc-head-link" id="header-collapse">
                <i data-feather="more-vertical"></i>
            </a>
        </div>
    </div>
    <!-- [ Mobile header ] End -->

    <!-- [ navigation menu ] start -->
    <nav class="pc-sidebar ">
        <div class="navbar-wrapper">
            <div class="m-header">
                <a href="#" class="b-brand">
                    <!-- ========   logo  ============ -->
                    <img src="{{ asset('admin/assets/images/logo-2.png') }}" alt="logo" class="w-50">
                    <img src="{{ asset('admin/assets/images/logo-2.png') }}" alt="logo" class="logo logo-sm">
                </a>
            </div>
            <div class="navbar-content">
                <ul class="pc-navbar">
                    <li class="pc-item pc-caption">
                        <label>Navigation</label>
                    </li>
                    <li class="pc-item {{ request()->segment(2) == 'index' ? 'active' : '' }}">
                        <a href="#" class="pc-link d-flex align-items-center d-flex align-items-center"><span
                                class="pc-micon"><i class="bi bi-grid-fill mr-2"></i> Dashboard</span></a>
                    </li>
                    <li class="pc-item pc-hasmenu {{ request()->segment(2) == 'career' ? 'active' : '' }}">
                        <a href="#" class="pc-link d-flex align-items-center justify-content-between "><span
                                class="pc-micon"> <i class="bi bi-bar-chart-steps mr-2"></i> Career</span><span
                                class="pc-arrow"><i data-feather="chevron-right"></i></span></a>
                        <ul class="pc-submenu">
                            <li class="pc-item"><a class="pc-link" href="/admin/career">Index</a></li>
                            <li class="pc-item"><a class="pc-link" href="/admin/career_details">Details</a></li>
                            <li class="pc-item"><a class="pc-link" href="/admin/career_booking">Bookings</a></li>
                        </ul>
                    </li>
                    <li class="pc-item pc-caption">
                        <label>Elements</label>
                        <span>UI Components</span>
                    </li>
                    <li class="pc-item pc-hasmenu {{ request()->segment(2) == 'course' ? 'active' : '' }}">
                        <a href="#" class="pc-link d-flex align-items-center justify-content-between "><span
                                class="pc-micon"> <i class="bi bi-book-fill mr-2"></i> Courses</span><span
                                class="pc-arrow"><i data-feather="chevron-right"></i></span></a>
                        <ul class="pc-submenu">
                            <li class="pc-item"><a class="pc-link" href="/admin/course">Index</a></li>
                            <li class="pc-item"><a class="pc-link" href="/admin/bookings">Bookings</a></li>
                            <li class="pc-item"><a class="pc-link" href="/admin/details">Details</a></li>
                            <li class="pc-item"><a class="pc-link" href="/admin/contents">Contents</a></li>
                            <li class="pc-item"><a class="pc-link" href="/admin/learnings">Learnings</a></li>
                            <li class="pc-item"><a class="pc-link" href="/admin/syllabus">Syllabus</a></li>
                        </ul>
                    </li>
                    <li class="pc-item {{ request()->segment(2) == 'offer' ? 'active' : '' }}">
                        <a href="/admin/offer" class="pc-link d-flex align-items-center "><span class="pc-micon"> <i
                                    class="bi bi-percent mr-2"></i> Offers</span></a>
                    </li>
                    <li class="pc-item pc-hasmenu {{ request()->segment(2) == 'services' ? 'active' : '' }}">
                        <a href="#" class="pc-link d-flex align-items-center justify-content-between "><span
                                class="pc-micon"> <i class="bi bi-file-earmark-fill mr-2"></i> Pages</span><span
                                class="pc-arrow"><i data-feather="chevron-right"></i></span></a>
                        <ul class="pc-submenu">
                            <li class="pc-item"><a class="pc-link" href="/admin/services">Services</a></li>
                            <li class="pc-item"><a class="pc-link" href="/admin/tagline">Taglines</a></li>
                            <li class="pc-item"><a class="pc-link" href="/admin/body_content">Body Content</a></li>
                            <li class="pc-item"><a class="pc-link" href="/admin/lower_body">Lower Body</a></li>
                            <li class="pc-item"><a class="pc-link" href="/admin/side_banner">Side Banner</a></li>
                        </ul>
                    </li>
                    <li class="pc-item pc-caption">
                        <label>Accounts</label>
                    </li>
                    <li class="pc-item pc-hasmenu {{ request()->segment(2) == 'account' ? 'active' : '' }}">
                        <a href="/admin/account" class="pc-link"><span class="pc-micon"><i
                                    class="bi bi-people-fill mr-2"></i>
                                Accounts</span><span class="pc-arrow d-block"></span></a>
                    </li>
                    <li class="pc-item pc-caption">
                        <label>SEO</label>
                    </li>
                    <li class="pc-item {{ request()->segment(2) == 'seo' ? 'active' : '' }}">
                        <a href="/admin/seo" class="pc-link d-flex align-items-center "><span class="pc-micon"><i
                                    class="bi bi-search mr-2"></i> SEO</span></a>
                    </li>
                    <li class="pc-item pc-caption">
                        <label>Messages</label>
                        <span>Messages sent by users</span>
                    </li>
                    <li class="pc-item {{ request()->segment(2) == 'message' ? 'active' : '' }}">
                        <a href="/admin/enquiry" class="pc-link d-flex align-items-center "><span class="pc-micon"><i
                                    class="bi bi-chat-dots-fill mr-2"></i> Messages</span></a>
                    <li class="pc-item {{ request()->segment(2) == 'appointment' ? 'active' : '' }}">
                        <a href="/admin/appointment" class="pc-link d-flex align-items-center "><span
                                class="pc-micon"><i class="bi bi-calendar-check-fill mr-2"></i>
                                Appointments</span></a>
                    </li>
                    <li class="pc-item {{ request()->segment(2) == 'email' ? 'active' : '' }}">
                        <a href="/admin/email" class="pc-link d-flex align-items-center "><span class="pc-micon"><i
                                    class="bi bi-envelope-fill mr-2"></i> Email Notebook</span></a>
                    </li>
                    <li class="pc-item pc-caption">
                        <label>FAQs</label>
                        <span>FAQs for your websites</span>
                    </li>
                    <li class="pc-item {{ request()->segment(2) == 'faq' ? 'active' : '' }}">
                        <a href="/admin/faq" class="pc-link d-flex align-items-center"><span class="pc-micon"><i
                                    class="bi bi-chat-right-dots-fill mr-2"></i> FAQs</span></a>

                    </li>
                    <li class="pc-item pc-caption">
                        <label>Other</label>
                        <span>Extra more things</span>
                    </li>
                    <li class="pc-item {{ request()->segment(2) == 'contactInfo' ? 'active' : '' }}"><a
                            href="/admin/contactInfo" class="pc-link d-flex align-items-center "><span
                                class="pc-micon"><i class="bi bi-telephone-fill mr-2"></i> Contact
                                Info</span></a>
                    </li>
                    <li class="pc-item {{ request()->segment(2) == 'contacts' ? 'active' : '' }}"><a
                            href="/admin/contacts" class="pc-link d-flex align-items-center "><span
                                class="pc-micon"><i class="bi bi-person-lines-fill mr-2"></i> Contacts
                            </span></a>
                    </li>
                    <li class="pc-item {{ request()->segment(2) == 'stories' ? 'active' : '' }}"><a
                            href="/admin/stories" class="pc-link d-flex align-items-center "><span
                                class="pc-micon"><i class="bi bi-plus-circle-dotted mr-2"></i> Stories</span></a>
                    </li>
                    <li class="pc-item {{ request()->segment(2) == 'gallery' ? 'active' : '' }}"><a
                            href="/admin/gallery" class="pc-link d-flex align-items-center "><span
                                class="pc-micon"><i class="bi bi-image-fill mr-2"></i> Gallery</span></a>
                    </li>
                    <li class="pc-item {{ request()->segment(2) == 'privacy' ? 'active' : '' }}"><a
                            href="/admin/privacy" class="pc-link d-flex align-items-center "><span
                                class="pc-micon"><i class="bi bi-lock-fill mr-2"></i> Privacy</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- [ navigation menu ] end -->
    <!-- [ Header ] start -->
    <header class="pc-header ">
        <div class="header-wrapper">
            @if (session()->has('message'))
                <div class="flash-message position-fixed p-5" id="flash-message">
                    <h5 class="text-center"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                            fill="#000" class="bi bi-check-circle-fill" viewBox="0 0 16 16">
                            <path
                                d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z" />
                        </svg> {{ session('message') }}</h5>
                </div>
            @endif
            @if (session()->has('error'))
                <div class="flash-error position-fixed p-5" id="flash-error" style="background:#ED2B2A;">
                    <h5 class="text-center text-white">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                            class="bi bi-exclamation-circle-fill" viewBox="0 0 16 16">
                            <path
                                d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8 4a.905.905 0 0 0-.9.995l.35 3.507a.552.552 0 0 0 1.1 0l.35-3.507A.905.905 0 0 0 8 4zm.002 6a1 1 0 1 0 0 2 1 1 0 0 0 0-2z" />
                        </svg> {{ session('error') }}
                    </h5>
                </div>
            @endif
            <div class="ml-auto">
                <ul class="list-unstyled">
                    <li class="dropdown pc-h-item">
                        <a class="pc-head-link dropdown-toggle arrow-none mr-0" data-toggle="dropdown" href="#"
                            role="button" aria-haspopup="false" aria-expanded="false">
                            <i class="material-icons-two-tone"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right pc-h-dropdown drp-search">
                            <form class="px-3">
                                <div class="form-group mb-0 d-flex align-items-center">
                                    <i data-feather="search"></i>
                                    <input type="search" class="form-control border-0 shadow-none"
                                        placeholder="Search here. . .">
                                </div>
                            </form>
                        </div>
                    </li>
                    <li class="dropdown pc-h-item">
                        <a class="pc-head-link dropdown-toggle arrow-none mr-0" data-toggle="dropdown" href="#"
                            role="button" aria-haspopup="false" aria-expanded="false">
                            <img src="{{ asset('assets/images/favicon.png') }}" alt="user-image"
                                class="avatar mr-2" width="30">
                            <span>
                                <span class="user-name">Rangin Tech</span>
                                <span class="user-desc">Administrator</span>
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right pc-h-dropdown">
                            <form method="POST" action="{{ route('logout') }}" class="dropdown-item">
                                @csrf
                                <button class="btn bg-none"> <i class="bi bi-box-arrow-right"></i> Logout</button>
                            </form>
                        </div>
                    </li>
                </ul>
            </div>

        </div>
    </header>
    <!-- [ Header ] end -->
    {{ $slot }}
    <!-- Required Js -->
    {{-- script to hide flash message --}}
    <script>
        window.addEventListener('DOMContentLoaded', function() {
            var flashSuccess = document.getElementById('flash-message');
            var flashError = document.getElementById('flash-error');

            // hide the flashSuccess div after 2 seconds
            if (flashSuccess !== null) {
                setTimeout(function() {
                    flashSuccess.style.display = 'none';
                }, 2000);
            }

            // hide the flashError div after 2 seconds
            if (flashError !== null) {
                setTimeout(function() {
                    flashError.style.display = 'none';
                }, 2000);
            }
        });
    </script>
    {{-- script to hide flash message --}}
    {{-- script to display selected image --}}
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            var imageElement = document.getElementById("image");
            if (imageElement !== null) {
                imageElement.addEventListener("change", function() {
                    var input = this;
                    var url = input.value;
                    var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
                    if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" ||
                            ext == "jpg")) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            document.getElementById("imageResult").setAttribute("src", e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                    } else {
                        document.getElementById("imageResult").setAttribute("src",
                            "{{ asset('admin/assets/images/preview.png') }}");
                    }
                });
            }

            var bodyImageElement = document.getElementById("body_image");
            if (bodyImageElement !== null) {
                bodyImageElement.addEventListener("change", function() {
                    var input = this;
                    var url = input.value;
                    var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
                    if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" ||
                            ext == "jpg")) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            document.getElementById("imagePreview").setAttribute("src", e.target
                                .result);
                        };
                        reader.readAsDataURL(input.files[0]);
                    } else {
                        document.getElementById("imagePreview").setAttribute("src",
                            "{{ asset('admin/assets/images/preview.png') }}");
                    }
                });
            }
        });
    </script>
    {{-- script to enable password inspect --}}
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var showHidePasswordLink = document.querySelector("#show_hide_password a");
            var passwordInput = document.querySelector('#show_hide_password input');
            var eyeIcon = document.querySelector('#show_hide_password i');

            showHidePasswordLink.addEventListener('click', function(event) {
                event.preventDefault();

                if (passwordInput.getAttribute("type") === "text") {
                    passwordInput.setAttribute('type', 'password');
                    eyeIcon.classList.add("bi-eye-fill");
                    eyeIcon.classList.remove("bi-eye-slash-fill");
                } else if (passwordInput.getAttribute("type") === "password") {
                    passwordInput.setAttribute('type', 'text');
                    eyeIcon.classList.remove("bi-eye-fill");
                    eyeIcon.classList.add("bi-eye-slash-fill");
                }
            });
            var showHidePassword = document.querySelector("#show_hide_con_password a");
            var passwordInputs = document.querySelector('#show_hide_con_password input');
            var eyeIcons = document.querySelector('#show_hide_con_password i');

            showHidePassword.addEventListener('click', function(event) {
                event.preventDefault();

                if (passwordInputs.getAttribute("type") === "text") {
                    passwordInputs.setAttribute('type', 'password');
                    eyeIcons.classList.add("bi-eye-fill");
                    eyeIcons.classList.remove("bi-eye-slash-fill");
                } else if (passwordInputs.getAttribute("type") === "password") {
                    passwordInputs.setAttribute('type', 'text');
                    eyeIcons.classList.remove("bi-eye-fill");
                    eyeIcons.classList.add("bi-eye-slash-fill");
                }
            });
        });
    </script>
    <script>
        ClassicEditor
            .create(document.querySelector('#description'));
    </script>

    <script src="{{ asset('admin/assets/js/vendor-all.min.js') }}"></script>
    <script src="{{ asset('admin/assets/js/plugins/bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin/assets/js/plugins/feather.min.js') }}"></script>
    <script src="{{ asset('admin/assets/js/pcoded.min.js') }}"></script>
</body>

</html>
