@if (session()->has('message'))
    <div class="flash-message position-fixed p-5" id="flash-message">
        <h5 class="text-center"><i class="fa-solid fa-check"></i> {{ session('message') }}</h5>
    </div>
@endif
@if (session()->has('error'))
    <div class="flash-error position-fixed p-5" id="flash-error">
        <h5 class="text-center text-white"><i class="fa-solid fa-circle-exclamation"></i> {{ session('error') }}</h5>
    </div>
@endif
@if ($errors->has('email'))
    <div class="flash-error position-fixed p-5" id="flash-error">
        <h5 class="text-center text-white"><i class="fa-solid fa-circle-exclamation"></i> {{ $errors->first('email') }}
        </h5>
    </div>
@endif
