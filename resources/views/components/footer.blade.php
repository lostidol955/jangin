@slot('footer')
    <section id="footer">
        <div class="footer">
            <div class="row align-items-center">
                <div class="col-12 col-md-12 col-lg-6">
                    <h1><b>Subscribe to our newsletter</b></h1>
                </div>
                <div class="col-12 col-md-12 col-lg-6 subscribe">
                    <form action="{{ route('subscribe') }}" method="POST" id="subscribeForm">
                        @csrf
                        <div class="input-area position-relative">
                            <input type="text" id="subscriber_email" name="email" placeholder="Email"
                                class="subscribe-input addBorder" style="outline: none !important">
                        </div>
                        <button type="submit" class="subscribe-btn position-absolute">Subscribe</button>
                    </form>
                </div>
            </div>
            <div class="bottom-links mt-5">
                <div class="row">
                    <div class="col">
                        <div class="heading mb-4">
                            <h4><a href="{{ route('web') }}" class="text-white text-decoration-none">Expert Service</a>
                            </h4>
                        </div>
                        <div class="links">
                            <p><a href="{{ route('web') }}">Custom Websites</a></p>
                            <p><a href="{{ route('application') }}">Web Applications</a></p>
                            <p><a href="{{ route('application') }}">Mobile Websites</a></p>
                            <p><a href="{{ route('marketing') }}">Digital Marketing</a></p>
                            <p><a href="{{ route('design') }}">UI Design</a></p>
                            <p><a href="{{ route('design') }}">Logo Animation</a></p>
                        </div>
                    </div>
                    <div class="col">
                        <div class="heading mb-3">
                            <h4><a href="{{ route('programs') }}" class="text-white text-decoration-none">Programs</a></h4>
                        </div>
                        <div class="links">
                            @if (!empty($courses))
                                @foreach ($courses as $course)
                                    <p><a href="/programs/{{ $course->slug }}">{{ $course->name }}</a></p>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="col">
                        <div class="heading mb-3">
                            <h4><a href="{{ route('site-map') }}" class="text-white text-decoration-none">Resources</a>
                            </h4>
                        </div>
                        <div class="links">
                            <p><a href="#">Blogs</a></p>
                            <p><a href="{{ route('appointment') }}">Book an appointment</a></p>
                            <p><a href="{{ route('help-center') }}">Help Center</a></p>
                            <p><a href="{{ route('site-map') }}">Sitemap</a></p>
                        </div>
                    </div>
                    <div class="col">
                        <div class="heading mb-3">
                            <h4><a href="{{ route('privacy') }}" class="text-white text-decoration-none">Legal</a></h4>
                        </div>
                        <div class="links">
                            <p><a href="{{ route('privacy') }}">Privacy Policy</a></p>
                            <p><a href="{{ route('terms-of-use') }}">Terms of Use</a></p>
                            <p><a href="{{ route('disclaimer') }}">Disclaimer</a></p>
                            <p><a href="{{ route('accessibility') }}">Accessibility</a></p>
                        </div>
                    </div>
                    <div class="col">
                        <div class="heading mb-3">
                            <h4><a href="{{ route('about') }}" class="text-white text-decoration-none">About</a></h4>
                        </div>
                        <div class="links">
                            <p><a href="{{ route('about') }}">About</a></p>
                            <p><a href="{{ route('career') }}">Career</a></p>
                            <p><a href="{{ route('about') }}">Vision</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-footer">
            <div class="contents d-flex justify-content-between align-items-center">
                <div class="left-content d-flex align-items-center">
                    <img src="{{ asset('assets/images/logo.png') }}" width="150" alt="company-logo">
                    <span class="copyright ml-4">© <span id="currentYear"></span></span>
                </div>
                <div class="right-content d-flex align-items-center">
                    <h5 class="follow-text d-inline mr-4">Follow us on social networks.</h5>
                    <div class="links d-flex align-items-center">
                        <a href="https://www.facebook.com/the.rangin" target="_blank" class="social-logo mr-4"><i
                                class="fa-brands fa-facebook-f"></i></a>
                        <a href="https://www.instagram.com/the.rangin/" target="_blank" class="social-logo mr-4"><i
                                class="fa-brands fa-instagram"></i></a>
                        <a href="https://www.linkedin.com/company/rangin/?originalSubdomain=np" target="_blank"
                            class="social-logo mr-4"><i class="fa-brands fa-linkedin-in"></i></a>
                        <a href="#" class="social-logo mr-4 behance"><i class="fa-brands fa-behance"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endslot
