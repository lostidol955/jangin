<div class="col-12 col-sm-6 col-lg-4 col-md-4">
    <form method="/">
        <div class="form-group mb-0 d-flex align-items-center box-shadow">
            <input type="search" class="form-control pr-5 border-0 shadow-none" placeholder="Search here. . ."
                name="search">
            <button class="mr-3"><i data-feather="search"></i></button>
        </div>
    </form>
</div>
