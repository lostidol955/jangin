@slot('offers')
    <div class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            @if ($offers->count() == 0)
                <div class="carousel-item active">
                    <p>Stay tuned for offers</p>
                </div>
            @else
                @foreach ($offers as $index => $offer)
                    <div class="carousel-item {{ $index === 0 ? 'active' : '' }}">
                        <h5 class="text-white">{{ $offer->discount }}% OFF- {{ $offer->course->name }}</h5>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
@endslot
