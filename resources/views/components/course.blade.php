<section id="courses">
    <div class="courses">
        <div class="heading text-center">
            <h1 data-aos="slide-up" data-aos-anchor-placement="top-bottom"><b>Programs that we are providing</b></h1>
        </div>
        <div class="box mt-5">
            <div class="row owl-carousel owl-theme">
                @foreach ($courses as $course)
                    <div class="col cols mb-5" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                        <div class="course-col py-4 pl-5" id="course-column">
                            <h1 class="mt-4"><b>{{ $course->name }}</b></h1>
                            <div class="course-description mt-3">
                                @foreach (explode(',', $course->keywords) as $keyword)
                                    <p><i class="fa-solid fa-check"></i> {{ trim($keyword) }}</p>
                                @endforeach
                            </div>
                            <div class="enroll-btn mt-4 mb-2">
                                <a href="/programs/{{ $course->slug }}"><button>Enroll Now</button></a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
