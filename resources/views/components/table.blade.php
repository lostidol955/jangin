<div class="row d-flex align-items-center mt-3 mb-5">
    <div class="col table-responsive">
        <table class="table table-hover">
            {{ $slot }}
        </table>
    </div>
</div>
