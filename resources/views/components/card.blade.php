<!-- [ Main Content ] start -->
<div class="row">
    <!-- customer-section start -->
    <div class="col-xl-12 col-md-12">
        <div class="card">
            {{ $slot }}
        </div>
    </div>
</div>
