<!doctype html>
<html lang="en">

<head>
    {{ $headerSeo }}
    <!-- Required meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- External stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/overview.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/contact.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/about.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/botman.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/privacy.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/search-result.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/career.css') }}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Fontawesome link -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
        integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- Owl carousel link -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/owl.carousel2@2.2.2/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="{{ asset('assets/owl_carousel/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/owl_carousel/owl.theme.default.css') }}">
    {{-- Data aos for scroll animation --}}
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

</head>

<body>

    {{ $navbar }}
    <!-- Navigation section -->
    {{ $slot }}
    <!-- section footer -->
    {{ $footer }}
    <!-- section footer -->
    <!-- Optional JavaScript -->
    <!-- jquery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    {{-- To display seo footer --}}
    {{ $footerSeo }}
    <!-- owl carousel -->
    <script src="{{ asset('assets/owl_carousel/owl.carousel.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/owl.carousel2@2.2.2/dist/owl.carousel.min.js"></script>
    <script src="{{ asset('assets/js/slider.js') }}"></script>
    <script src="{{ asset('assets/js/scroll.js') }}"></script>
    <script src="{{ asset('assets/js/side-bar.js') }}"></script>
    <script src="{{ asset('assets/js/currenttime.js') }}"></script>
    <script src="{{ asset('assets/js/contactForm.js') }}"></script>
    <script src="{{ asset('assets/js/bookingForm.js') }}"></script>
    <script src="{{ asset('assets/js/enquiryForm.js') }}"></script>
    <script src="{{ asset('assets/js/searchForm.js') }}"></script>
    <script src="{{ asset('assets/js/contactUsForm.js') }}"></script>
    <script src="{{ asset('assets/js/careerBooking.js') }}"></script>
    <script>
        $(document).ready(function() {
            var flash = $('#flash-message');
            var flashError = $('#flash-error');

            // hide the div after 5 seconds
            setTimeout(function() {
                flash.hide();
                flashError.hide();
            }, 2000);
        });
    </script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
        AOS.init({
            offset: 400,
            duration: 1200,
            once: true,
        });
    </script>
    <script>
        var botmanWidget = {
            title: 'Rangin Tech',
            introMessage: 'Hello, I am a RanginDroid! I am here to assist you and answer all your questions about our products and services!',
            mainColor: '#add35d',
            aboutText: '',
            headerTextColor: '#fff',
        }
    </script>
    <script src='https://cdn.jsdelivr.net/npm/botman-web-widget@0/build/js/widget.js'></script>
    <!-- Include js plugin -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

</body>

</html>
