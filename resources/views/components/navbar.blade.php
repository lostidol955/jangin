@slot('navbar')
    <section class="sticky">
        <!-- Mobile design section -->
        <section id="mobile-header">
            <div class="mobile-header">
                <div class="row">
                    <div class="col-4 left-header">
                        <div class="time">
                            <h5 class="font-weight-bold" id="currentime"></h5>
                        </div>
                    </div>
                    <div class="col-8 right-header">
                        <div class="offer">
                            <div class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    @if (!empty($offers))
                                        @foreach ($offers as $index => $offer)
                                            <div class="carousel-item {{ $index === 0 ? 'active' : '' }}">
                                                <h5 class="text-white">{{ $offer->discount }}% OFF-
                                                    {{ $offer->course->name }}
                                                </h5>
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="carousel-item active">
                                            <p>Stay tuned for offers</p>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Mobile navbar section -->
        <!-- Mobile navbar section -->
        <section id="mobile-nav" class="position-relative">
            <div class="mobile-nav d-flex justify-content-between align-items-center position-relative">
                <div class="left-nav d-flex align-items-center">
                    <div class="bar">
                        <button class="bar-btn" id="bar-btn"><i class="fa-solid fa-bars-staggered"></i></button>
                    </div>
                    <div class="image ml-5">
                        <a href="{{ route('home') }}"> <img src="{{ asset('assets/images/favicon.png') }}" alt="mobile-logo"
                                class="w-100" /></a>
                    </div>
                </div>
                <div class="right-nav">
                    <a href="tel:+977-9802821147"><i class="fa-solid fa-phone"></i></a>
                    <a href="{{ route('help-center') }}" class="text-dark text-decoration-none ml-4 help">Help Center</a>
                </div>
            </div>
            <div class="mobile-links position-absolute">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <div class="title d-flex justify-content-between align-items-center px-2" data-toggle="collapse"
                                data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <p class="mb-0 {{ request()->segment(1) == 'web' ? 'active' : '' }}">
                                    Websites
                                </p>
                                <i class="fa-solid fa-chevron-down"></i>
                            </div>
                        </div>
                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <a href="{{ route('web') }}" class="overview mb-4 d-block text-black">Overview</a>
                                <p class="title">Web Design Services</p>
                                @if (!empty($webtypes))
                                    @foreach ($webtypes as $webtype)
                                        <a href="/service/{{ $webtype->slug }}"
                                            class="d-block text-black mt-4">{{ $webtype->name }}</a>
                                    @endforeach
                                @else
                                @endif

                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <div class="title d-flex justify-content-between align-items-center px-2" data-toggle="collapse"
                                data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <p class="mb-0 {{ request()->segment(1) == 'design' ? 'active' : '' }}">
                                    Design
                                </p>
                                <i class="fa-solid fa-chevron-down"></i>
                            </div>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                <a href="{{ route('design') }}" class="overview mb-4 d-block text-black">Overview</a>
                                <p class="title">Design Services</p>
                                @if (!empty($designtypes))
                                    @foreach ($designtypes as $designtype)
                                        <a href="/service/{{ $designtype->slug }}"
                                            class="d-block text-black mt-4">{{ $designtype->name }}</a>
                                    @endforeach
                                @else
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <div class="title d-flex justify-content-between align-items-center px-2" data-toggle="collapse"
                                data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <p class="mb-0 {{ request()->segment(1) == 'marketing' ? 'active' : '' }}">
                                    Marketing
                                </p>
                                <i class="fa-solid fa-chevron-down"></i>
                            </div>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                <a href="{{ route('marketing') }}" class="overview mb-4 d-block text-black">Overview</a>
                                <p class="title">Marketing Services</p>
                                @if (!empty($marketingtypes))
                                    @foreach ($marketingtypes as $marketingtype)
                                        <a href="/service/{{ $marketingtype->slug }}"
                                            class="d-block text-black mt-4">{{ $marketingtype->name }}</a>
                                    @endforeach
                                @else
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <div class="title d-flex justify-content-between align-items-center px-2"
                                data-toggle="collapse" data-target="#collapseFour" aria-expanded="false"
                                aria-controls="collapseFour">
                                <p class="mb-0 {{ request()->segment(1) == 'programs' ? 'active' : '' }}">
                                    Programs
                                </p>
                                <i class="fa-solid fa-chevron-down"></i>
                            </div>
                        </div>
                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="card-body">
                                <a href="{{ route('programs') }}" class="overview mb-4 d-block text-black">Overview</a>
                                <p class="title">Our Programs</p>
                                @if (!empty($courses))
                                    @foreach ($courses as $course)
                                        <a href="/programs/{{ $course->slug }}"
                                            class="d-block text-black mt-4">{{ $course->name }}</a>
                                    @endforeach
                                @else
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <div class="title d-flex justify-content-between align-items-center px-2"
                                data-toggle="collapse" data-target="#collapseFive" aria-expanded="false"
                                aria-controls="collapseFive">
                                <p class="mb-0 {{ request()->segment(1) == 'application' ? 'active' : '' }}">
                                    Applications
                                </p>
                                <i class="fa-solid fa-chevron-down"></i>
                            </div>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                            <div class="card-body">
                                <a href="{{ route('application') }}"
                                    class="overview mb-4 d-block text-black">Overview</a>
                                <p class="title">Our Applications</p>
                                @if (!empty($applicationtypes))
                                    @foreach ($applicationtypes as $application)
                                        <a href="/service/{{ $application->slug }}"
                                            class="d-block text-black mt-4">{{ $application->name }}</a>
                                    @endforeach
                                @else
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Mobile navbar section -->
        <!-- Mobile design section -->
        <!-- Header section -->
        <section id="header">
            <div class="header">
                <div class="logo">
                    <a href="{{ route('home') }}"><img src="{{ asset('assets/images/logo.png') }}"
                            alt="company-image"></a>
                </div>
                <div class="header-menus">
                    <a href="#">Blog</a>
                    <a href="{{ route('help-center') }}">Help Center</a>
                    <a href="{{ route('contacts') }}">Contact Us</a>
                    <a href="{{ route('career') }}">Career</a>
                </div>
            </div>
        </section>
        <!-- Header section -->
        <!-- Navigation section -->
        <section id="navigation">
            <div class="navigation mb-3">
                <div class="navigation-bar">
                    <div class="nav-link position-relative">
                        <a href="#" class="{{ request()->segment(1) == 'web' ? 'active' : '' }}">Websites <i
                                class="fa-sharp fa-solid fa-chevron-down"></i> </a>
                        <div class="sub-menu position-absolute">
                            <a href="{{ route('web') }}" class="d-block">Overview</a>
                            <p>Web Design Services</p>
                            @if (!empty($webtypes))
                                @foreach ($webtypes as $webtype)
                                    <a href="/service/{{ $webtype->slug }}" class="d-block">{{ $webtype->name }}</a>
                                @endforeach
                            @else
                            @endif
                        </div>
                    </div>
                    <div class="nav-link position-relative"><a href="#"
                            class="{{ request()->segment(1) == 'design' ? 'active' : '' }}">Design <i
                                class="fa-sharp fa-solid fa-chevron-down"></i></a>
                        <div class="sub-menu position-absolute">
                            <a href="{{ route('design') }}" class="d-block">Overview</a>
                            <p>Design Services</p>
                            @if (!empty($designtypes))
                                @foreach ($designtypes as $designtype)
                                    <a href="/service/{{ $designtype->slug }}"
                                        class="d-block">{{ $designtype->name }}</a>
                                @endforeach
                            @else
                            @endif

                        </div>
                    </div>
                    <div class="nav-link position-relative">
                        <a href="#" class="{{ request()->segment(1) == 'marketing' ? 'active' : '' }}">Marketing <i
                                class="fa-sharp fa-solid fa-chevron-down"></i></a>
                        <div class="sub-menu position-absolute">
                            <a href="{{ route('marketing') }}" class="d-block">Overview</a>
                            <p>Marketing Services</p>
                            @if (!empty($marketingtypes))
                                @foreach ($marketingtypes as $marketingtype)
                                    <a href="/service/{{ $marketingtype->slug }}"
                                        class="d-block">{{ $marketingtype->name }}</a>
                                @endforeach
                            @else
                            @endif
                        </div>
                    </div>
                    <div class="nav-link position-relative"><a href="#"
                            class="{{ request()->segment(1) == 'programs' ? 'active' : '' }}">Programs
                            <i class="fa-sharp fa-solid fa-chevron-down"></i></a>
                        <div class="sub-menu position-absolute">
                            <a href="{{ route('programs') }}" class="d-block">Overview</a>
                            <p>Our Programs</p>
                            @if (!empty($courses))
                                @foreach ($courses as $course)
                                    <a href="/programs/{{ $course->slug }}" class="d-block">{{ $course->name }}</a>
                                @endforeach
                            @else
                            @endif
                        </div>
                    </div>
                    <div class="nav-link position-relative"><a href="#"
                            class="{{ request()->segment(1) == 'application' ? 'active' : '' }}">Applications <i
                                class="fa-sharp fa-solid fa-chevron-down"></i></a>
                        <div class="sub-menu position-absolute">
                            <a href="{{ route('application') }}" class="d-block">Overview</a>
                            <p>Our Applications</p>
                            @if (!empty($applicationtypes))
                                @foreach ($applicationtypes as $application)
                                    <a href="/service/{{ $application->slug }}"
                                        class="d-block">{{ $application->name }}</a>
                                @endforeach
                            @else
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>

@endslot
