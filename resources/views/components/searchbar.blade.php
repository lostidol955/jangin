   <!-- Search section -->
   <section id="search">
       <div class="search-section">
           <form method="get" action="{{ route('course.search') }}" class="input-area position-relative" id="searchForm">
               <input type="search" id="searchBar" name="search" class="search-input addBorder"
                   placeholder="For eg: Courses">
               <button class="text-uppercase search-btn position-absolute">Search</button>
           </form>
       </div>
   </section>
   <!-- Search section -->
