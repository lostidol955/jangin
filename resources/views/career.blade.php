<x-homelayout>
    @slot('headerSeo')
        @if (empty($seoheader))
        @else
            @foreach ($seoheader as $header)
                {!! $header->code !!}
            @endforeach
        @endif
    @endslot
    <x-sweetalert />
    <x-navbar :webtypes="$webtypes" :designtypes="$designtypes" :marketingtypes="$marketingtypes" :courses="$courses" :applicationtypes="$applicationtypes"
        :offers="$offers" />
    <!-- Search section -->
    <x-searchbar />
    <!-- scroll up button -->
    <div class="scroll">
        <button class="scroll-btn">
            <i class="fa-solid fa-chevron-up"></i>
        </button>
    </div>
    <!-- scroll up button -->
    <div class="career">
        @if (!empty($careers))
            <div class="container py-5 mt-5">
                <div class="row">
                    @foreach ($careers as $career)
                        <a href="{{ route('career-details', $career->slug) }}" class="text-decoration-none text-black">
                            <div class="col-12 col-sm-12 col-lg-6 mb-4" data-aos="slide-up"
                                data-aos-anchor-placement="top-bottom">
                                <div class="card">
                                    <img class="card-img-top" src="{{ asset('storage/' . $career->image) }}"
                                        alt="Career image">
                                    <div class="card-body">
                                        <h5 class="card-title font-weight-bold">{{ $career->job_title }}</h5>
                                        <a href="{{ route('career-details', $career->slug) }}"
                                            class="apply pb-1 mt-3">Apply</a>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>
            @else
                <div class="container-fluid p-5 mt-5">
                    <div class="card py-5">
                        <div class="card-body">
                            <h5 class="card-title font-weight-bold">Nothing to show..</h5>
                        </div>
                    </div>
                </div>
        @endif
    </div>
    <!-- scroll up button -->
    <div class="scroll">
        <button class="scroll-btn">
            <i class="fa-solid fa-chevron-up"></i>
        </button>
    </div>
    <!-- scroll up button -->
    </div>
    <!--  section Footer -->
    <x-footer :courses="$courses" />
    @slot('footerSeo')
        @if (empty($seofooter))
        @else
            @foreach ($seofooter as $footer)
                {!! $footer->code !!}
            @endforeach
        @endif
    @endslot
</x-homelayout>
