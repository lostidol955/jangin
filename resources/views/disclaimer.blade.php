<x-homelayout>
    @slot('headerSeo')
        @if (empty($seoheader))
        @else
            @foreach ($seoheader as $header)
                {!! $header->code !!}
            @endforeach
        @endif
    @endslot
    <x-navbar :webtypes="$webtypes" :designtypes="$designtypes" :marketingtypes="$marketingtypes" :courses="$courses" :applicationtypes="$applicationtypes"
        :offers="$offers" />
    <section id="body-content">
        <div class="disclaimer body-content">
            <div class="title mt-4">
                <h1 class="text-center font-weight-bold" data-aos="fade-up" data-aos-anchor-placement="top-bottom">Disclaimer</h1>
            </div>
            <div class="accessibility-description px-5 mt-5">
                <div class="description my-5">
                    <p data-aos="fade-up" data-aos-anchor-placement="top-bottom">
                        On www.rangin.com.np, Rangin Technology makes information and materials available subject to the
                        Terms of Use and this Disclaimer. The use of our Website is governed by this Disclaimer; by
                        visiting this Website, you agree to the terms and conditions set forth in the Terms of Use and
                        this Disclaimer. Please do not use this Website if you disapprove. We retain the right to change
                        the Terms of Use and/or the Disclaimer at any time and at our sole discretion by publishing
                        revised Terms of Use and Disclaimer on this Website. You agree to accept the changes / revisions
                        by using this Website after they have been posted, whether or not you have reviewed them.
                    </p>
                </div>
            </div>
        </div>
        <!-- scroll up button -->
        <div class="scroll">
            <button class="scroll-btn">
                <i class="fa-solid fa-chevron-up"></i>
            </button>
        </div>
        <!-- scroll up button -->
    </section>
    <!--  section Footer -->
    <x-footer :courses="$courses" />
    @slot('footerSeo')
        @if (empty($seofooter))
        @else
            @foreach ($seofooter as $footer)
                {!! $footer->code !!}
            @endforeach
        @endif
    @endslot
</x-homelayout>
