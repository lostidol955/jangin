<x-homelayout>
    @slot('headerSeo')
        @if (empty($seoheader))
        @else
            @foreach ($seoheader as $header)
                {!! $header->code !!}
            @endforeach
        @endif
    @endslot
    <x-sweetalert />
    <x-navbar :webtypes="$webtypes" :designtypes="$designtypes" :marketingtypes="$marketingtypes" :courses="$courses" :applicationtypes="$applicationtypes"
        :offers="$offers" />
    <!-- Overview section -->
    <section id="overview-body">
        <div class="overview-body application mt-3">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-6 left-section mt-5" data-aos="fade-right"
                    data-aos-anchor-placement="top-bottom">
                    <div class="left-content py-5 pl-5">
                        <h5 class="pr-3 text-white slideInLeft">Overview</h5>
                        <h1 class="mt-4 mb-5  text-white mr-4">
                            {{ $applicationtagline->title ?? '' }}
                        </h1>
                        <div class="number">
                            <p class="text-white slideInLeft">Call us today at <span
                                    class="tel d-block text-black">{{ $contactInfo->mobile_no ?? '' }}</span></p>
                        </div>
                    </div>
                </div>
                @if (!empty($applicationtagline))
                    <div class="col-12 col-md-12 col-lg-6 right-section p-0" data-aos="fade-left"
                        data-aos-anchor-placement="top-bottom"
                        style="background: url({{ asset('storage/' . ($applicationtagline ? $applicationtagline->image : '')) }}) no-repeat;
                        background-position: center;
                        background-size: cover;">
                    </div>
                @else
                @endif

            </div>
            <!-- scroll up button -->
            <div class="scroll">
                <button class="scroll-btn">
                    <i class="fa-solid fa-chevron-up"></i>
                </button>
            </div>
            <!-- scroll up button -->
        </div>
    </section>
    <!-- Overview section -->
    <section id="business">
        <div class="business program-overview application-overview bg-white mt-5">
            <div class="row d-flex flex-lg-row-reverse" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                <div class="col-12 col-md-12 col-lg-6 left-section">
                    <div class="left-content">
                        <h1 class="pr-3 revealX font-weight-bold">{{ $applicationcontent->title ?? '' }}</h1>
                        <div class="list-items mt-4">
                            @if (!empty($applicationcontent->description))
                                @foreach (preg_split("/\r\n|\n|\r/", $applicationcontent->description) as $points)
                                    <p class="lists"><img src="assets/images/tick.png" width="30" class="mr-2"
                                            alt="tick">
                                        {{ trim($points) }}
                                    </p>
                                @endforeach
                            @else
                            @endif
                        </div>
                    </div>
                </div>
                @if (!empty($applicationcontent))
                    <div class="col-12 col-md-12 col-lg-6 right-section">
                        <img src="{{ asset('storage/' . ($applicationcontent ? $applicationcontent->image : '')) }}"
                            class="w-100" alt="application">
                    </div>
                @else
                @endif
            </div>
        </div>
    </section>
    <!-- Business need section -->
    <!-- WebTypes section -->
    <section id="webtypes">
        <div class="webtypes graphic-service mt-4">
            <div class="row">
                @if (empty($applicationtypes))
                    <div class="col-12 columns">
                        <h1 class="title text-center">No service available for now.</h1>
                    </div>
                @else
                    @foreach ($applicationtypes as $apptypes)
                        <div class="col-12 col-md-12 col-lg-6 columns" data-aos="slide-up"
                            data-aos-anchor-placement="top-bottom">
                            <h1 class="title ">{{ $apptypes->name }}</h1>
                            <div class="content">
                                <p class="description mt-4">{!! strip_tags($apptypes->description) !!}</p>
                            </div>
                            <div class="learn mt-4">
                                <a href="/service/{{ $apptypes->slug }}"><button class="learn-btn">Learn
                                        More</button></a>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
    <!-- WebTypes section -->
    {{-- Application lower content --}}
    @if (!empty($applicationLowerContent))
        <section id="business">
            <div class="business web mt-4">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-5 left-section">
                        <div class="left-content">
                            <h1 class="pr-3 text-white">{{ $applicationLowerContent->title ?? '' }}</h1>
                            <p class="my-4 description text-white">
                                {{ $applicationLowerContent->description ?? '' }}</p>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-7 right-image"
                        style="background: url({{ asset('storage/' . ($applicationLowerContent ? $applicationLowerContent->image : '')) }}) no-repeat;
                        background-position: center;
                        background-size: cover;">

                    </div>
                </div>
            </div>
        </section>
    @else
    @endif
    {{-- Application lower content --}}
    <!-- section contact -->
    <section id="contact">
        <div class="contact bg-white">
            <div class="heading text-center" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                <h1><b>Need help creating your website?</b></h1>
                <p class="mt-2 mb-5">Go pro services and support from our team.</p>
            </div>
            <div class="form mb-5" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                <form action="{{ route('message.submit') }}" id="contactForm" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-auto mb-4">
                            <input type="text" placeholder="Full Name*" class="form-input" name="name"
                                id="fname">
                        </div>
                        <div class="col-auto mb-4">
                            <input type="text" placeholder="Mobile*" class="form-input" name="phone"
                                id="phone_no">
                        </div>
                        <div class="col-auto mb-4">
                            <input type="text" placeholder="Email*" class="form-input" name="email"
                                id="user_email">
                            <input type="hidden" name="type" id="type" value="business">
                        </div>
                        <div class="col-auto sumit-btn">
                            <button class="submit" type="submit">Let's Talk</button>
                        </div>
                    </div>
                </form>
            </div>
            <h5 class="bottom-text text-center">Fill out our contact form, and we’ll give you a call.</h5>
        </div>
    </section>
    <!-- section contact -->
    <!-- section stories -->
    <x-story :stories="$stories" />
    <!-- section stories -->
    <!--  section Footer -->
    <x-footer :courses="$courses" />
    @slot('footerSeo')
        @if (empty($seofooter))
        @else
            @foreach ($seofooter as $footer)
                {!! $footer->code !!}
            @endforeach
        @endif
    @endslot
</x-homelayout>
