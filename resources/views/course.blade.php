<x-homelayout>
    @slot('headerSeo')
        @if (empty($seoheader))
        @else
            @foreach ($seoheader as $header)
                {!! $header->code !!}
            @endforeach
        @endif
    @endslot
    <x-sweetalert />
    <x-navbar :webtypes="$webtypes" :designtypes="$designtypes" :marketingtypes="$marketingtypes" :courses="$courses" :applicationtypes="$applicationtypes"
        :offers="$offers" />
    @if (!empty($tagline))
        <!-- Overview section -->
        <section id="overview-body">
            <div class="overview-body programs mt-3 course position-relative"
                style="background: {{ $tagline->color ?? '#05a6f0' }} !important">
                <div class="row" data-aos="fade-right" data-aos-anchor-placement="top-bottom">
                    <div class="col-12 col-md-12 col-lg-6 left-section course-content mt-5">
                        <div class="left-content pb-4 pl-5">
                            <h1 class="mt-5 mb-4 pr-5 text-white">
                                {{ $tagline->title ?? 'Rangin programs tagline here' }}
                            </h1>
                        </div>
                    </div>
                </div>
                <div class="buttons" data-aos="fade-right" data-aos-anchor-placement="top-bottom">
                    <a href="{{ $syllabus ? asset('storage/' . $syllabus->file) : '#' }}" target="_blank"
                        class="text-decoration-none">
                        <button class="download-btn">Download Syllabus</button>
                    </a>
                    <button class="enroll-btn" id="enroll-button">Enroll Now</button>
                </div>
                <div class="course-row bg-white position-absolute rounded d-flex justify-content-between">
                    <div class="columns" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                        <h5 class="text-center">{{ $details->duration ?? '1' }} Months</h5>
                        <p class="text-center">Total Duration</p>
                    </div>
                    <div class="columns" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                        <h5 class="text-center">{{ $details->format ?? 'Physical Session' }}</h5>
                        <p class="text-center">Format</p>
                    </div>
                    <div class="columns" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                        <h5 class="text-center">{{ $details->internship ?? '1' }} Month</h5>
                        <p class="text-center">Internship</p>
                    </div>
                    <div class="columns" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                        <h5 class="text-center">{{ $details->timing ?? 'flexible' }}</h5>
                        <p class="text-center">Timing</p>
                    </div>
                    <div class="columns" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                        <h5 class="text-center">Rs {{ $details->price ?? '0' }}</h5>
                        <p class="text-center">Price</p>
                    </div>
                    <div class="columns" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                        <h5 class="text-center">{{ $details->emis ?? '1' }}</h5>
                        <p class="text-center">EMIs</p>
                    </div>
                </div>
                <div class="mobile-course-row bg-white position-absolute">
                    <div class="upper-row">
                        <div class="d-flex justify-content-between">
                            <div class="columns">
                                <h5 class="text-center">{{ $details->duration ?? '1' }} Months</h5>
                                <p class="text-center">Total Duration</p>
                            </div>
                            <div class="columns">
                                <h5 class="text-center">{{ $details->format ?? 'Physical Session' }}</h5>
                                <p class="text-center">Format</p>
                            </div>
                            <div class="columns">
                                <h5 class="text-center">{{ $details->internship ?? '1' }} Month</h5>
                                <p class="text-center">Internship</p>
                            </div>

                        </div>
                    </div>
                    <div class="bottom-row">
                        <div class="d-flex justify-content-between">
                            <div class="columns">
                                <h5 class="text-center">{{ $details->timing ?? 'flexible' }}</h5>
                                <p class="text-center">Timing</p>
                            </div>
                            <div class="columns">
                                <h5 class="text-center">Rs {{ $details->duration ?? '0' }}</h5>
                                <p class="text-center">Price</p>
                            </div>
                            <div class="columns">
                                <h5 class="text-center">{{ $details->emis ?? '1' }}</h5>
                                <p class="text-center">EMIs</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- scroll up button -->
                <div class="scroll">
                    <button class="scroll-btn">
                        <i class="fa-solid fa-chevron-up"></i>
                    </button>
                </div>
                <!-- scroll up button -->
            </div>
        </section>
        <!-- Overview section -->
        <!-- section sidebar -->
        <section id="sidebar">
            <div class="sidebar">
                <div class="row enquiry-form d-flex flex-row-reverse">
                    <div class="col-12 col-md-12 col-lg-6 left-section pb-5">
                        <div class="left-content mt-5">
                            <div class="header pl-5 d-flex justify-content-between align-items-center">
                                <img src="{{ asset('assets/images/logo.png') }}" alt="company-logo" width="200">
                                <button class="close-btn mr-3" id="close-btn"><i
                                        class="fa-solid fa-circle-xmark"></i></button>
                            </div>
                            <form action="{{ route('course.book') }}" id="bookingForm" method="POST"
                                class="mt-5 px-4">
                                @csrf
                                <div class="input-field">
                                    <input class="w-100" type="text" name="name" id="name"
                                        placeholder="Full Name*" />
                                </div>
                                <div class="input-field">
                                    <input class="w-100" type="text" name="email" id="email"
                                        placeholder="Email*" />
                                </div>
                                <div class="input-field">
                                    <input class="w-100" type="text" name="phone" id="phone"
                                        placeholder="Mobile/Phone*" />
                                </div>
                                <div class="input-field">
                                    <input class="w-100" type="text" name="address" id="address"
                                        placeholder="Address*" />
                                </div>
                                <div class="input-field">
                                    <select class="w-100" name="qualification" id="qualification">
                                        <option value="see"> SEE Level</option>
                                        <option value="+2"> +2 Level</option>
                                        <option value="bachelor"> Bachelor Level</option>
                                    </select>
                                </div>
                                <div class="input-field">
                                    <input type="date" class="w-100" id="date" name="date"
                                        placeholder="Start Date*" />

                                    <input type="hidden" class="w-100" id="course_id" name="course_id"
                                        value="{{ $contents->course_id ?? '' }}" />
                                </div>
                                <div class="input-field">
                                    <input type="time" class="w-100" id="time" name="time"
                                        placeholder="Prefer time*" />
                                </div>

                                <button type="submit" class="submit-btn w-100">Submit</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-6 right-section"
                        @if (!empty($sideBanner)) style="background: url({{ asset('storage/' . $sideBanner->image) }}) no-repeat;
                            background-position: center;
                            background-size: cover;">
                        @else @endif
                        </div>
                    </div>
                </div>
        </section>
        <!-- section sidebar -->
        <!-- Business need section -->
        <section id="business">
            <div class="business program-overview bg-white course-section">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-7 left-section">
                        <div class="left-content" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                            <h1 class="pr-3 font-weight-bold">What you'll learn ?</h1>
                            @if (!empty($contents->learning_points))
                                <div class="list-items mt-4">
                                    @foreach (preg_split("/\r\n|\n|\r/", $contents->learning_points) as $points)
                                        <p class="lists"><img src="{{ asset('assets/images/tick.png') }}"
                                                width="30" class="mr-2" alt="tick">
                                            {{ trim($points) }}
                                        </p>
                                    @endforeach
                                </div>
                            @else
                                <div class="default-div mt-4">
                                    <p>No learning points available.</p>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-5 right-body " data-aos="slide-up"
                        data-aos-anchor-placement="top-bottom">
                        @if (!empty($contents->body_image))
                            <img src="{{ asset('storage/' . $contents->body_image) }}" class="w-100"
                                alt="programs">
                        @else
                        @endif

                    </div>
                </div>
            </div>
        </section>
        <!-- Business need section -->
        <section id="courses-info">
            <div class="courses-info">
                <div class="courses-overview mt-5">
                    <h1 class="font-weight-bold " data-aos="slide-up" data-aos-anchor-placement="top-bottom">Course
                        overview</h1>
                    <div class="description mt-4" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                        <p class="mb-0 "> {{ $contents->overview ?? '' }}</p>
                        <p class=" margin-top"> <span class="d-block font-weight-bold">Why is
                                {{ $contents->course->name ?? 'No title here' }} so
                                popular?</span>
                            {{ $contents->why_so_popular ?? '' }}
                        </p>
                    </div>
                </div>
                <div class="courses-outline mt-5">
                    <div class="mobile-boxes d-none ">
                        <div class="mobile-course my-5 row px-3">
                            <div class="col-6 box">
                                <div class="content">
                                    <h5 data-aos="slide-up" data-aos-anchor-placement="top-bottom"><span
                                            class="num" data-val="{{ $details->learning_hrs ?? '' }}"> </span>+
                                    </h5>
                                    <p class="text-uppercase">Learning Hours</p>
                                </div>
                            </div>
                            <div class="col-6 box">
                                <div class="content green">
                                    <h5 data-aos="slide-up" data-aos-anchor-placement="top-bottom"><span
                                            class="num" data-val="{{ $details->assignment ?? 2 }}"> </span>+
                                    </h5>
                                    <p class="text-uppercase">Assignments</p>
                                </div>
                            </div>
                            <div class="col-6 box">
                                <div class="content pink">
                                    <h5 data-aos="slide-up" data-aos-anchor-placement="top-bottom"><span
                                            class="num" data-val="{{ $details->internship ?? 2 }}"> </span>+
                                    </h5>
                                    <p class="text-uppercase">Month Internship</p>
                                </div>
                            </div>
                            <div class="col-6 box">
                                <div class="content black">
                                    <h5 data-aos="slide-up" data-aos-anchor-placement="top-bottom"><span
                                            class="num" data-val="{{ $details->enrollment ?? 2 }}"> </span>+
                                    </h5>
                                    <p class="text-uppercase">Enrollment</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="outline-content">
                        <h1 class="font-weight-bold" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                            Course Outline</h1>
                        <div class="outline-description  mt-4" data-aos="slide-up"
                            data-aos-anchor-placement="top-bottom">
                            <p>{{ $contents->outline ?? '' }}</p>
                        </div>
                        <div class="course-summary">
                            <div class="row">
                                <div class="col learning-points height-630 position-relative">
                                    <div class="learning-accordions height-95">
                                        <div id="accordion" data-aos="slide-up"
                                            data-aos-anchor-placement="top-bottom">
                                            @if (empty($formattedItems))
                                                <div class="card">
                                                    <div class="card-header" id="headingOne">
                                                        <div class="title d-flex justify-content-between align-items-center px-2"
                                                            data-toggle="collapse" data-target="#collapseOne"
                                                            aria-expanded="true" aria-controls="collapseOne">
                                                            <p class="mb-0">
                                                                No title found
                                                            </p>
                                                            <i class="fa-solid fa-chevron-down"></i>
                                                        </div>
                                                    </div>
                                                    <div id="collapseOne" class="collapse"
                                                        aria-labelledby="headingOne" data-parent="#accordion">
                                                        <div class="card-body">
                                                            <ol style="list-unstyles">
                                                                <li>
                                                                    No descriptions found
                                                                </li>
                                                            </ol>
                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                @foreach ($formattedItems as $index => $item)
                                                    <div class="card">
                                                        <div class="card-header" id="heading{{ $index }}">
                                                            <div class="title d-flex justify-content-between align-items-center px-2"
                                                                data-toggle="collapse"
                                                                data-target="#collapse{{ $index }}"
                                                                aria-expanded="true"
                                                                aria-controls="collapse{{ $index }}">
                                                                <p class="mb-0">
                                                                    {!! $item['title'] !!}
                                                                </p>
                                                                <i class="fa-solid fa-chevron-down"></i>
                                                            </div>
                                                        </div>
                                                        <div id="collapse{{ $index }}" class="collapse"
                                                            aria-labelledby="headingOne" data-parent="#accordion">
                                                            <div class="card-body">
                                                                <ol class="list-unstyled">
                                                                    <li>{!! $item['description'] !!}</li>
                                                                </ol>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                        <div class="see-more text-center position-absolute">
                                            <button><i class="fa-solid fa-arrow-down"></i> <span
                                                    class="toggle_text"></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col desktop-box">
                                    <div class="box-summary row px-3">
                                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 box">
                                            <div class="content">
                                                <h2><span class="num"
                                                        data-val="{{ $details->learning_hrs ?? 2 }}">
                                                    </span>+</h2>
                                                <p class="text-uppercase">Learning Hours</p>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 box">
                                            <div class="content green">
                                                <h2><span class="num" data-val="{{ $details->assignment ?? 2 }}">
                                                    </span>+</h2>
                                                <p class="text-uppercase">Assignments</p>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 box">
                                            <div class="content pink">
                                                <h2><span class="num" data-val="{{ $details->internship ?? 2 }}">
                                                    </span>+</h2>
                                                <p class="text-uppercase">Month Internship</p>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 box">
                                            <div class="content black">
                                                <h2><span class="num" data-val="{{ $details->enrollment ?? 2 }}">
                                                    </span>+</h2>
                                                <p class="text-uppercase">Enrollment</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="courses-overview mt-5">
                    <h1 class="font-weight-bold " data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                        Practicality</h1>
                    <div class="description  mt-4">
                        @if (!empty($contents->practicality))
                            <ul class="mb-0 list-unstyled" data-aos="slide-up"
                                data-aos-anchor-placement="top-bottom">
                                @foreach (preg_split("/\r\n|\n|\r/", $contents->practicality) as $list)
                                    <li class="mb-2">&#x2022; {{ trim($list) }}</li>
                                @endforeach
                            </ul>
                        @else
                            <p>No practicality information available.</p>
                        @endif

                    </div>
                </div>
                <div class="courses-overview mt-5">
                    <h1 class="font-weight-bold " data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                        Requirements</h1>
                    <div class="description  mt-4">
                        @if (!empty($contents->requirements))
                            <ul class="mb-0 list-unstyled" data-aos="slide-up"
                                data-aos-anchor-placement="top-bottom">
                                @foreach (preg_split("/\r\n|\n|\r/", $contents->requirements) as $list)
                                    <li class="mb-2 list">&#x2022; {{ trim($list) }}</li>
                                @endforeach
                            </ul>
                        @else
                        @endif
                    </div>
                </div>
                @if (!empty($contents->image))
                    <div class="courses-overview mt-5">
                        <h1 class="font-weight-bold " data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                            Tools you'll learn</h1>
                        <div class="description  mt-4" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                            <img src="{{ asset('storage/' . $contents->image) }}" class="w-100" />
                        </div>
                    </div>
                @else
                @endif
        </section>
        <!-- section contact -->
        <section id="contact">
            <div class="contact bg-white mt-5">
                <div class="heading text-center" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                    <h1><b>Need help creating your website?</b></h1>
                    <p class="mt-2 mb-5">Go pro services and support from our team.</p>
                </div>
                <div class="form mb-5">
                    <form action="{{ route('message.submit') }}" id="contactForm" method="POST"
                        data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                        @csrf
                        <div class="row">
                            <div class="col-auto mb-4">
                                <input type="text" placeholder="Full Name*" class="form-input" name="name"
                                    id="fname">
                            </div>
                            <div class="col-auto mb-4">
                                <input type="text" placeholder="Mobile*" class="form-input" name="phone"
                                    id="phone_no">
                            </div>
                            <div class="col-auto mb-4">
                                <input type="text" placeholder="Email*" class="form-input" name="email"
                                    id="user_email">
                                <input type="hidden" name="type" id="type" value="student">
                            </div>
                            <div class="col-auto sumit-btn">
                                <button class="submit" type="submit">Let's Talk</button>
                            </div>
                        </div>
                    </form>
                </div>
                <h5 class="bottom-text text-center" data-aos="slide-up" data-aos-anchor-placement="top-bottom">Fill
                    out our contact form, and we’ll give you a call.</h5>
            </div>
        </section>
        <!-- section contact -->
        <!-- section stories -->
        <x-story :stories="$stories" />
        <!-- section stories -->
    @else
        <div class="container py-5 my-5">
            <div class="title">
                <h1> This page is under maintainance...</h1>
            </div>
            <div class="description">
                <h5>Please be patience</h5>
            </div>
        </div>
    @endif
    <!--  section Footer -->
    <x-footer :courses="$courses" />
    @slot('footerSeo')
        @if (empty($seofooter))
        @else
            @foreach ($seofooter as $footer)
                {!! $footer->code !!}
            @endforeach
        @endif
    @endslot
</x-homelayout>
