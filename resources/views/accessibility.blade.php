<x-homelayout>
    @slot('headerSeo')
        @if (empty($seoheader))
        @else
            @foreach ($seoheader as $header)
                {!! $header->code !!}
            @endforeach
        @endif
    @endslot
    <x-navbar :webtypes="$webtypes" :designtypes="$designtypes" :marketingtypes="$marketingtypes" :courses="$courses" :applicationtypes="$applicationtypes"
        :offers="$offers" />
    <section id="body-content">
        <div class="accessibility body-content">
            <div class="title mt-4">
                <h1 class="text-center font-weight-bold" data-aos="fade-up" data-aos-anchor-placement="top-bottom">Accessibility</h1>
            </div>
            <div class="accessibility-description px-5 mt-5">
                <div class="description my-5" data-aos="fade-up" data-aos-anchor-placement="top-bottom">
                    <p>
                        Rangin Technology is dedicated to making our website accessible to the widest possible audience,
                        regardless of circumstances or skills. We have made some improvements to our website to meet
                        this criterion, and we will continue to improve it in the future. We strive to follow the World
                        Wide Web Consortium’s Web Content Accessibility Guidelines (WCAG) as closely as possible (W3C).
                        The Web Content Accessibility Guidelines (WCAG) papers describe how to make web content more
                        accessible to individuals with impairments. Following these standards will enable us to deliver
                        a more user-friendly site for everyone.
                    </p>
                    <p>
                        Some examples of features that we have provided are:
                    </p>
                    <ul class="ml-4">
                        <li> Screen readers and others with vision problems can utilize alternative text to find and
                            understand useful images.</li>
                        <li> Labels and controls on the form that are related to the prompt.</li>
                        <li> Standard browser settings can be used to magnify content.</li>
                    </ul>
                </div>
            </div>

        </div>
        <!-- scroll up button -->
        <div class="scroll">
            <button class="scroll-btn">
                <i class="fa-solid fa-chevron-up"></i>
            </button>
        </div>
        <!-- scroll up button -->
    </section>
    <!--  section Footer -->
    <x-footer :courses="$courses" />
    @slot('footerSeo')
        @if (empty($seofooter))
        @else
            @foreach ($seofooter as $footer)
                {!! $footer->code !!}
            @endforeach
        @endif
    @endslot
</x-homelayout>
