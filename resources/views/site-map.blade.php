<x-homelayout>
    @slot('headerSeo')
        @if (empty($seoheader))
        @else
            @foreach ($seoheader as $header)
                {!! $header->code !!}
            @endforeach
        @endif
    @endslot
    <x-navbar :webtypes="$webtypes" :designtypes="$designtypes" :marketingtypes="$marketingtypes" :courses="$courses" :applicationtypes="$applicationtypes"
        :offers="$offers" />
    <section id="body-content">
        <div class="site-map body-content">
            <div class="title mt-4">
                <h1 class="text-center font-weight-bold" data-aos="fade-up" data-aos-anchor-placement="top-bottom">Site Map
                </h1>
            </div>
            <div class="site-map-description mt-5">
                <div class="description my-5" data-aos="fade-up" data-aos-anchor-placement="top-bottom">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-6 col-lg-3 mb-4">
                            <h2 class="font-weight-bold" data-aos="fade-up" data-aos-anchor-placement="top-bottom">About
                            </h2>
                            <div class="content mt-4">
                                <h5 class="font-weight-normal">
                                    1. About Us
                                </h5>
                                <h5 class="font-weight-normal mt-4">
                                    2. Our Process
                                </h5>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-3 mb-4">
                            <h2 class="font-weight-bold" data-aos="fade-up" data-aos-anchor-placement="top-bottom">Our
                                Services
                            </h2>
                            <div class="content mt-4">
                                <h5 class="font-weight-normal">
                                    1. Graphic Design
                                </h5>
                                <h5 class="font-weight-normal mt-4">
                                    2. Development
                                </h5>
                                <h5 class="font-weight-normal mt-4">
                                    3. Digital Marketing
                                </h5>
                                <h5 class="font-weight-normal mt-4">
                                    4. Web Hosting
                                </h5>
                                <h5 class="font-weight-normal mt-4">
                                    5. Classes
                                </h5>
                                <h5 class="font-weight-normal mt-4">
                                    6. IT Support
                                </h5>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-3 mb-4">
                            <h2 class="font-weight-bold" data-aos="fade-up" data-aos-anchor-placement="top-bottom">News
                                & Blogs
                            </h2>
                            <div class="content mt-4">
                                <h5 class="font-weight-normal">
                                    1. News & Blogs
                                </h5>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-3 mb-4">
                            <h2 class="font-weight-bold" data-aos="fade-up" data-aos-anchor-placement="top-bottom">
                                Contact Us
                            </h2>
                            <div class="content mt-4">
                                <h5 class="font-weight-normal">
                                    1. Contact Us
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- scroll up button -->
        <div class="scroll">
            <button class="scroll-btn">
                <i class="fa-solid fa-chevron-up"></i>
            </button>
        </div>
        <!-- scroll up button -->
    </section>
    <!--  section Footer -->
    <x-footer :courses="$courses" />
    @slot('footerSeo')
        @if (empty($seofooter))
        @else
            @foreach ($seofooter as $footer)
                {!! $footer->code !!}
            @endforeach
        @endif
    @endslot
</x-homelayout>
