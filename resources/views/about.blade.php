<x-homelayout>
    @slot('headerSeo')
        @if (empty($seoheader))
        @else
            @foreach ($seoheader as $header)
                {!! $header->code !!}
            @endforeach
        @endif
    @endslot
    <x-sweetalert />
    <x-navbar :webtypes="$webtypes" :designtypes="$designtypes" :marketingtypes="$marketingtypes" :courses="$courses" :applicationtypes="$applicationtypes"
        :offers="$offers" />
    <!-- Overview section -->
    <section id="about-body">
        <div class="overview-body design-overview about-body mt-3">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-6 left-section mt-5" data-aos="fade-right"
                    data-aos-anchor-placement="top-bottom">
                    <div class="left-content pt-3 pb-5 pl-4">
                        <h5 class="pr-3 text-white ">About Us</h5>
                        <h1 class="mt-2 mb-5  text-white">{{ $tagline->title ?? '' }}</h1>
                    </div>
                </div>
                @if (!empty($tagline))
                    <div class="col-12 col-md-12 col-lg-6 right-section p-0" data-aos="fade-left"
                        data-aos-anchor-placement="top-bottom"
                        style="background: url({{ asset('storage/' . ($tagline ? $tagline->image : '')) }}) no-repeat;
                        background-position: center;
                        background-size: cover;">
                    </div>
                @else
                @endif
            </div>
            <!-- scroll up button -->
            <div class="scroll">
                <button class="scroll-btn">
                    <i class="fa-solid fa-chevron-up"></i>
                </button>
            </div>
            <!-- scroll up button -->
        </div>
        <div class="lower-content" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
            <h5>Rangin Technology, where dreams take flight, Crafting brands that shine, with digital might. From Web to
                Apps, we’ll guide
                your ascent, Embracing innovation, every moment well-spent, With top-notch training, skills you’ll
                acquire, A journey of growth,
                fueled by desire, Join our vibrant community, ignite your flame, Rangin Technology, where success finds
                its name</h5>
        </div>
    </section>
    <!-- Overview section -->
    <!-- section about description -->
    <section id="about-description">
        <div class="about-description">
            <div class="contents">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-6 p-0" data-aos="slide-up"
                        data-aos-anchor-placement="top-bottom">
                        <img src="assets/images/example.jpg" class="w-100 my-4 px-4" alt="about-img">
                    </div>
                    <div class="col-12 col-sm-12 col-lg-6 right-column">
                        <div class="vision">
                            <h1 class="title" data-aos="slide-up" data-aos-anchor-placement="top-bottom">Vision</h1>
                            <h5 class="description" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                                Our vision is to empower businesses and individuals with
                                cutting-edge digital solutions that transcend boundaries. We
                                strive to be a catalyst for innovation, pushing the boundaries
                                of technology to create extraordinary experiences.
                            </h5>
                        </div>
                        <div class="mission mt-4">
                            <h1 class="title" data-aos="slide-up" data-aos-anchor-placement="top-bottom">Mission</h1>
                            <h5 class="description" data-aos="slide-up" data-aos-anchor-placement="top-bottom">To create
                                transformative digital experiences that drive growth,
                                empower businesses, and inspire individuals.
                            </h5>
                            <h5 class="description" data-aos="slide-up" data-aos-anchor-placement="top-bottom">We are
                                dedicated to:</h5>
                            <div class="lists" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                                <li>Delivering Excellence.</li>
                                <li> Empowering Businesses.</li>
                                <li> Fostering Collaboration.</li>
                                <li> Inspiring Creativity.</li>
                                <li> Embracing Continuous Learning.</li>
                                <li> Cultivating Trust.</li>
                                <li> Making a Positive Impact</li>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- section about description -->
    <!-- section services -->
    <section id="services">
        <div class="services about-services">
            <div class="row pr-4">
                <div class="col-12 col-sm-12 col-lg-3 description-col">
                    <h1 class="title font-weight-bold" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                        What We Do
                    </h1>
                    <h5 class="description mt-3" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                        Step into a world where dreams come alive. At Rangin Technology, we are creators of digital
                        wonder, weaving artistry and innovation to craft extraordinary experiences that touch hearts and
                        inspire minds. We breathe life into brands, designing visually captivating websites that leave
                        visitors spellbound.
                    </h5>
                </div>
                <div class="col-12 col-sm-12 col-lg-9 multi-col">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-lg-4 cols shadows  px-4 pt-4">
                            <a href="#">
                                <h5 class="text-black font-weight-bold" data-aos="slide-up"
                                    data-aos-anchor-placement="top-bottom">Web Development</h5>
                                <p class="about-service-description" data-aos="slide-up"
                                    data-aos-anchor-placement="top-bottom">We utilize the latest technologies
                                    and design principles to develop
                                    custom websites
                                </p>
                            </a>
                        </div>
                        <div class="col-12 col-sm-12 col-lg-4 cols shadows  px-4 pt-4 ">
                            <a href="#">
                                <h5 class="text-black font-weight-bold" data-aos="slide-up"
                                    data-aos-anchor-placement="top-bottom">App Development</h5>
                                <p class="about-service-description" data-aos="slide-up"
                                    data-aos-anchor-placement="top-bottom">We crafts intuitive and feature-rich
                                    mobile applications for iOS and
                                    Android platforms.</p>
                            </a>
                        </div>
                        <div class="col-12 col-sm-12 col-lg-4 cols shadows  px-4 pt-4 ">
                            <a href="#">
                                <h5 class="text-black font-weight-bold" data-aos="slide-up"
                                    data-aos-anchor-placement="top-bottom">Digital Marketing</h5>
                                <p class="about-service-description" data-aos="slide-up"
                                    data-aos-anchor-placement="top-bottom">We use data-driven strategies to
                                    enhance brand visibility, drive
                                    targeted traffic, and generate
                                    leads.</p>
                            </a>
                        </div>
                        <div class="col-12 col-sm-12 col-lg-4 cols shadows px-4 pt-4">
                            <a href="#">
                                <h5 class="text-black font-weight-bold" data-aos="slide-up"
                                    data-aos-anchor-placement="top-bottom">UI/UX Design</h5>
                                <p class="about-service-description" data-aos="slide-up"
                                    data-aos-anchor-placement="top-bottom">We create UI visually appealing,
                                    intuitive, and optimized for user
                                    engagement, ensuring a delightful
                                    user journey.
                                </p>
                            </a>
                        </div>
                        <div class="col-12 col-sm-12 col-lg-4 cols shadows px-4 pt-4 ">
                            <a href="#">
                                <h5 class="text-black font-weight-bold" data-aos="slide-up"
                                    data-aos-anchor-placement="top-bottom">E-commerce Solution </h5>
                                <p class="about-service-description" data-aos="slide-up"
                                    data-aos-anchor-placement="top-bottom">Rangin Technology helps every
                                    businesses establish and optimize
                                    their online stores.
                                </p>
                            </a>
                        </div>
                        <div class="col-12 col-sm-12 col-lg-4 cols shadows px-4 pt-4">
                            <a href="#">
                                <h5 class="text-black font-weight-bold" data-aos="slide-up"
                                    data-aos-anchor-placement="top-bottom">Training and Consulation </h3>
                                    <p class="about-service-description" data-aos="slide-up"
                                        data-aos-anchor-placement="top-bottom">We offer comprehensive training
                                        programs and consultation services
                                        in digital marketing, web and app
                                        development.
                                    </p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- section services -->
    <!-- section about gallery -->
    <section id="gallery">
        <div class="gallery">
            <ul class="nav nav-tabs">
                <li class="nav-item row text-center" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                    <a class="col-sm-6 col-md-6 col-lg-3 mr-2 mb-3 nav-link active" data-toggle="tab"
                        href="#event">Events</a>
                    <a class="col-sm-6 col-md-6 col-lg-3 mr-2 mb-3 nav-link" data-toggle="tab"
                        href="#office">Office</a>
                    <a class="col-sm-6 col-md-6 col-lg-3 mr-2 mb-3 nav-link" data-toggle="tab"
                        href="#products">Products</a>
                    <a class="col-sm-6 col-md-6 col-lg-3 mb-3 nav-link mr-0" data-toggle="tab"
                        href="#team">Team</a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="event" class="tab-pane fade show active">
                    <div class="row">
                        @if ($events->count() == 0)
                            <div class="col-12 p-5 bg-light">
                                <h5 class="text-center">No image found.</h5>
                            </div>
                        @else
                            @foreach ($events as $event)
                                <div class="col-sm-12 col-md-4 col-lg-4 mb-4">
                                    <a href="{{ asset('storage/' . $event->image) }}" target="_blank"> <img
                                            src="{{ asset('storage/' . $event->image) }}" alt="event"
                                            class="w-100"></a>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div id="office" class="tab-pane fade">
                    <div class="row">
                        @if ($office->count() == 0)
                            <div class="col-12 p-5 bg-light">
                                <h5 class="text-center">No image found.</h5>
                            </div>
                        @else
                            @foreach ($office as $office)
                                <div class="col-sm-12 col-md-4 col-lg-4 mb-4">
                                    <a href="{{ asset('storage/' . $office->image) }}" target="_blank"> <img
                                            src="{{ asset('storage/' . $office->image) }}" alt="office"
                                            class="w-100"></a>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div id="products" class="tab-pane fade">
                    <div class="row">
                        @if ($products->count() == 0)
                            <div class="col-12 p-5 bg-light">
                                <h5 class="text-center">No image found.</h5>
                            </div>
                        @else
                            @foreach ($products as $product)
                                <div class="col-sm-12 col-md-4 col-lg-4 mb-4">
                                    <a href="{{ asset('storage/' . $product->image) }}" target="_blank"> <img
                                            src="{{ asset('storage/' . $product->image) }}" alt="products"
                                            class="w-100"></a>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div id="team" class="tab-pane fade">
                    <div class="row">
                        @if ($team->count() == 0)
                            <div class="col-12 p-5 bg-light">
                                <h5 class="text-center">No image found.</h5>
                            </div>
                        @else
                            @foreach ($team as $team)
                                <div class="col-sm-12 col-md-4 col-lg-4 mb-4">
                                    <a href="{{ asset('storage/' . $team->image) }}" target="_blank"> <img
                                            src="{{ asset('storage/' . $team->image) }}" alt="team"
                                            class="w-100"></a>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- section about gallery -->
    <!-- section contact -->
    <section id="contact">
        <div class="contact bg-white">
            <div class="heading text-center" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                <h1><b>Need help creating your website?</b></h1>
                <p class="mt-2 mb-5">Go pro services and support from our team.</p>
            </div>
            <div class="form mb-5" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                <form action="{{ route('message.submit') }}" id="contactForm" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-auto mb-4">
                            <input type="text" placeholder="Full Name*" class="form-input" name="name"
                                id="fname">
                        </div>
                        <div class="col-auto mb-4">
                            <input type="text" placeholder="Mobile*" class="form-input" name="phone"
                                id="phone_no">
                        </div>
                        <div class="col-auto mb-4">
                            <input type="text" placeholder="Email*" class="form-input" name="email"
                                id="user_email">
                            <input type="hidden" name="type" id="type" value="visitor">
                        </div>
                        <div class="col-auto sumit-btn">
                            <button class="submit" type="submit">Let's Talk</button>
                        </div>
                    </div>
                </form>
            </div>
            <h5 class="bottom-text text-center">Fill out our contact form, and we’ll give you a call.</h5>
        </div>
    </section>
    <!-- section contact -->
    <x-story :stories="$stories" />
    <x-footer :courses="$courses" />
    <!-- section stories -->
    @slot('footerSeo')
        @if (empty($seofooter))
        @else
            @foreach ($seofooter as $footer)
                {!! $footer->code !!}
            @endforeach
        @endif
    @endslot
</x-homelayout>
