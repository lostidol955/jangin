<x-homelayout>
    @slot('headerSeo')
        @if (empty($seoheader))
        @else
            @foreach ($seoheader as $header)
                {!! $header->code !!}
            @endforeach
        @endif
    @endslot
    <x-sweetalert />
    <x-navbar :webtypes="$webtypes" :designtypes="$designtypes" :marketingtypes="$marketingtypes" :courses="$courses" :applicationtypes="$applicationtypes"
        :offers="$offers" />
    <!-- Search section -->
    <x-searchbar />
    <!-- Overview section -->
    <section id="contact">
        <div class="contact-body">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-6 left-section">
                    <div class="left-content mt-3">
                        <div class="contact-info d-none" data-aos="fade-left" data-aos-anchor-placement="top-bottom">
                            <h1 class="title mt-3">Appointment</h1>
                            <p class="description mt-3 mb-4">Empower your brand with Rangin TECH - book an appointment
                                with us, we can build remarkable brands through collaboration and innovative
                                solutions. Schedule now and drive your success.</p>
                        </div>
                        <form action="{{ route('appointment.book') }}" method="POST" id="appointmentForm"
                            data-aos="fade-left" data-aos-anchor-placement="top-bottom">
                            @csrf
                            <div class="input-field">
                                <input class="w-100" type="text" name="name" id="contact_name"
                                    placeholder="Full Name*" />
                            </div>
                            <div class="input-field">
                                <input class="w-100" type="text" name="email" id="contact_email"
                                    placeholder="Email*" />
                                <input type="hidden" name="type" id="type" value="visitors" />
                            </div>
                            <div class="input-field">
                                <input class="w-100" type="text" name="phone" id="contact_phone"
                                    placeholder="Mobile/Phone*" />
                            </div>
                            <div class="input-field">
                                <input class="w-100" type="text" name="address" id="contact_address"
                                    placeholder="Address*" />
                            </div>
                            <div class="input-field">
                                <input class="w-100" type="datetime-local" name="date" id="date" />
                            </div>
                            <div class="input-field">
                                <textarea class="w-100 position-relative" name="description" id="description" rows="3"
                                    placeholder="What do you want to book for ?*"></textarea>
                                <p id="charCount" class="text-white d-flex justify-content-end font-weight-light">Max
                                    character 500</p>
                            </div>
                            <div class="form-check mt-5">
                                <input type="checkbox" class="form-check-input mt-2" id="terms">
                                <label class="form-check-label terms text-white" for="terms">I have read Privacy
                                    Policy
                                    and
                                    agree
                                    to
                                    the
                                    terms and conditions.*</label>
                            </div>
                            <button type="submit" class="submit-btn w-100">Submit</button>
                        </form>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-lg-6 right-column">
                    <div class="right-content" data-aos="fade-right" data-aos-anchor-placement="top-bottom">
                        <h1 class="title mt-3">Appointment</h1>
                        <p class="description mt-3">Empower your brand with Rangin TECH - book an appointment with us,
                            we can build remarkable brands through collaboration and innovative solutions.
                            Schedule now and drive your success.</p>
                        <div class="contact-details">
                            <div class="location mt-5 bg-white rounded">
                                <h5>Office Location</h5>
                                <p>33700 | {{ $contactInfo->location ?? '' }} | Kaski | Gandaki |
                                    Nepal</p>
                            </div>
                            <div class="map bg-white rounded box mt-5 d-flex justify-content-between">
                                <p class="mb-0">Find our location on Google Map</p>
                                <a href="{{ $contactInfo->map_link ?? '' }}" class="learn-more mt-1"
                                    target="_blank">Learn
                                    more</a>
                            </div>
                            <div class="career bg-white rounded box mt-3 d-flex justify-content-between">
                                <p class="mb-0">Learn about careers at Rangin TECH</p>
                                <a href="{{ route('career') }}" class="learn-more mt-1" target="_blank">Learn more</a>
                            </div>
                            <div class="more-details bg-white rounded box mt-3 d-flex justify-content-between">
                                <p class="mb-0">Learn more about Rangin TECH</p>
                                <a href="{{ route('about') }}" class="learn-more mt-1" target="_blank">Learn more</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- scroll up button -->
            <div class="scroll">
                <button class="scroll-btn">
                    <i class="fa-solid fa-chevron-up"></i>
                </button>
            </div>
            <!-- scroll up button -->
        </div>
    </section>
    <!-- Overview section -->
    <!-- section stories -->
    <x-story :stories="$stories" />
    <!-- section stories -->
    {{-- section footer  --}}
    <x-footer :courses="$courses" />
    {{-- section footer  --}}
    @slot('footerSeo')
        @if (empty($seofooter))
        @else
            @foreach ($seofooter as $footer)
                {!! $footer->code !!}
            @endforeach
        @endif
    @endslot
</x-homelayout>
