<x-homelayout>
    @slot('headerSeo')
        @if (empty($seoheader))
        @else
            @foreach ($seoheader as $header)
                {!! $header->code !!}
            @endforeach
        @endif
    @endslot
    <x-sweetalert />
    <x-navbar :webtypes="$webtypes" :designtypes="$designtypes" :marketingtypes="$marketingtypes" :courses="$courses" :applicationtypes="$applicationtypes"
        :offers="$offers" />
    <!-- Overview section -->

    <section id="overview-body">
        <div class="overview-body mt-3">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-6 left-section mt-5" data-aos="fade-right"
                    data-aos-anchor-placement="top-bottom">
                    <div class="left-content py-5 pl-5">
                        <h5 class="pr-3 text-white slideInLeft">Overview</h5>
                        <h1 class="mt-4 mb-5  text-white text-capitalize pr-5">
                            {{ $webtagline->title ?? '' }}
                        </h1>
                        <div class="number">
                            <p class="text-white slideInLeft">Call us today at <span
                                    class="tel d-block">{{ $contactInfo->mobile_no ?? '' }}</span></p>
                        </div>
                    </div>
                </div>
                @if (!empty($webtagline))
                    <div class="col-12 col-md-12 col-lg-6 right-section p-0" data-aos="fade-left"
                        data-aos-anchor-placement="top-bottom"
                        style="background: url({{ asset('storage/' . ($webtagline ? $webtagline->image : '')) }}) no-repeat;
                            background-position: center;
                            background-size: cover;">
                    </div>
                @else
                @endif
            </div>
            <!-- scroll up button -->
            <div class="scroll">
                <button class="scroll-btn">
                    <i class="fa-solid fa-chevron-up"></i>
                </button>
            </div>
            <!-- scroll up button -->
        </div>
    </section>
    <!-- Overview section -->
    <!-- section weboption -->
    @if (!empty($webcontent))
        <section id="weboption">
            <div class="weboption">
                <div class="heading">
                    <h1 data-aos="slide-up" data-aos-anchor-placement="top-bottom">Learn more about our <span
                            class="mid-text d-block">website development
                            options</span> </h1>
                </div>
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-6 left-body" data-aos="slide-up"
                        data-aos-anchor-placement="top-bottom">
                        <p class="description mb-2">
                            @if (!empty($webcontent->description))
                                @foreach (preg_split("/\r\n|\n|\r/", $webcontent->description) as $paragraph)
                                    <p class="description first mb-4">
                                        {{ trim($paragraph) }}
                                    </p>
                                @endforeach
                            @else
                            @endif
                        </p>
                        <img src="{{ asset('storage/' . ($webcontent ? $webcontent->image : '')) }}" class="w-100"
                            alt="web-image">
                    </div>
                    <div class="col-12 col-md-12 col-lg-6 right-body" data-aos="slide-up"
                        data-aos-anchor-placement="top-bottom">
                        @if (!empty($webcontent->description))
                            @foreach (preg_split("/\r\n|\n|\r/", $webcontent->description) as $paragraph)
                                <p class="description first mb-4">
                                    {{ trim($paragraph) }}
                                </p>
                            @endforeach
                        @else
                        @endif
                    </div>
                </div>
            </div>
        </section>
    @else
    @endif
    <!-- section weboption -->
    <!-- WebTypes section -->
    <section id="webtypes">
        <div class="webtypes mt-4">
            <div class="row">
                @if ($webtypes->count() == 0)
                    <div class="pb-5 px-3">
                        <h5>No website types found </h5>
                    </div>
                @else
                    @foreach ($webtypes as $webtype)
                        <div class="col-12 col-md-12 col-lg-6 columns" data-aos="slide-up"
                            data-aos-anchor-placement="top-bottom">
                            <h1 class="title ">{{ $webtype->name }}</h1>
                            <div class="content">
                                <p class="description mt-4">{!! strip_tags($webtype->description) !!}</p>
                            </div>
                            <div class="learn mt-4">
                                <a href="/service/{{ $webtype->slug }}">
                                    <button class="learn-btn">Learn More</button>
                                </a>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
    <!-- WebTypes section -->
    <!-- Business need section -->
    @if (!empty($webLowerContent))
        <section id="business">
            <div class="business web mt-4">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-5 left-section">
                        <div class="left-content">
                            <h1 class="pr-3 text-white revealX">{{ $webLowerContent->title ?? '' }}</h1>
                            <p class="my-4 description text-white revealX">{{ $webLowerContent->description ?? '' }}</p>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-7 right-section"
                        style="background: url({{ asset('storage/' . ($webLowerContent ? $webLowerContent->image : '')) }}) no-repeat;
                    background-position: center;
                    background-size: cover;">

                    </div>
                </div>
            </div>
        </section>
    @else
    @endif
    <!-- Business need section -->
    <!-- section contact -->
    <section id="contact">
        <div class="contact bg-white">
            <div class="heading text-center" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                <h1><b>Need help creating your website?</b></h1>
                <p class="mt-2 mb-5">Go pro services and support from our team.</p>
            </div>
            <div class="form mb-5" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                <form action="{{ route('message.submit') }}" id="contactForm" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-auto mb-4">
                            <input type="text" placeholder="Full Name*" class="form-input" name="name"
                                id="fname">
                        </div>
                        <div class="col-auto mb-4">
                            <input type="text" placeholder="Mobile*" class="form-input" name="phone"
                                id="phone_no">
                        </div>
                        <div class="col-auto mb-4">
                            <input type="text" placeholder="Email*" class="form-input" name="email"
                                id="user_email">
                            <input type="hidden" name="type" id="type" value="business">
                        </div>
                        <div class="col-auto sumit-btn">
                            <button class="submit" type="submit">Let's Talk</button>
                        </div>
                    </div>
                </form>
            </div>
            <h5 class="bottom-text text-center">Fill out our contact form, and we’ll give you a call.</h5>
        </div>
    </section>
    <!-- section contact -->
    <!-- section stories -->
    <x-story :stories="$stories" />
    <!-- section stories -->
    <!--  section Footer -->
    <x-footer :courses="$courses" />
    @slot('footerSeo')
        @if (empty($seofooter))
        @else
            @foreach ($seofooter as $footer)
                {!! $footer->code !!}
            @endforeach
        @endif
    @endslot
</x-homelayout>
