<x-homelayout>
    @slot('headerSeo')
        @if (empty($seoheader))
        @else
            @foreach ($seoheader as $header)
                {!! $header->code !!}
            @endforeach
        @endif
    @endslot
    <x-navbar :webtypes="$webtypes" :designtypes="$designtypes" :marketingtypes="$marketingtypes" :courses="$courses" :applicationtypes="$applicationtypes"
        :offers="$offers" />
    <section id="body-content">
        <div class="accessibility body-content">
            <div class="title mt-4">
                <h1 class="text-center font-weight-bold" data-aos="fade-up" data-aos-anchor-placement="top-bottom">Terms of
                    use</h1>
            </div>
            <div class="accessibility-description px-5 mt-5">
                <div class="description my-5" data-aos="fade-up" data-aos-anchor-placement="top-bottom">
                    <p>
                        Welcome to the Terms of Service for Rangin Technology’s website, www.rangin.com.np. These Terms
                        of Service explain the conditions under which you may access and browse this Website. You must
                        read and accept all of these Terms of Use, as well as the Disclaimer, in order to become a
                        member of this Website. Nothing in this document is intended to grant any third-party rights or
                        benefits. If you do not agree to be governed by these Terms of Use, you may not use or access
                        this Website, and you may opt out of participation in the online forum at any time after
                        becoming a member.
                    </p>
                    <p>
                        By posting revised Terms of Use on this Website, we reserve the right to change these Terms of
                        Use at any time and without notice. Your continued use of this Website means that you agree to
                        the revised Terms of Service.
                    </p>
                    <p>
                        As a result, you should review the Terms of Use on a regular basis for your own benefit.
                    </p>
                    <ul class="ml-4">
                        <li>This Website, known as “www.rangin.com.np,” is an online forum in the form of a medium that
                            was created to allow visitors, such as Current or Potential Customers, Alliance Partners or
                            Potential Alliance Partners, Financial and Industry Analysts, Investors, Vendors /
                            Suppliers, Media & Journalists, Job seekers, Current and Former employees, Researchers or
                            Academicians, and others, to gather information about Rangin Technology and interact with
                            Rangin Technology</li>
                        <li class="mt-4"> Anyone who fills out the website’s Contact Us, Event Registration, or Information Download
                            form can become a member (s). The website member / user who has certain privilege
                            permissions to download unique information such as White Papers. The membership’s privileges
                            are solely at the discretion of Rangin Technology and the site administrator. A user/member
                            can only have one active account. Users/Members also have no right to sell, exchange, or
                            otherwise transfer their membership to another person or business. If you do not meet the
                            requirements, you should not join this online forum.</li>
                        <li class="mt-4">On this Website, we may run surveys, polls, and contests from time to time. Members’
                            opinions and responses will be kept private. In the event of a contest, the winner’s
                            information and prize(s), if any, will be forwarded to the recipient’s registered address as
                            collected on this Website through post/courier. </li>
                        <li class="mt-4">These Terms of Use, as well as the Website’s Disclaimer, govern the use of this Website.
                            Rangin Technology has the right to refuse membership to any user for any reason at any time.
                        </li>
                        <li class="mt-4">Rangin Technology reserves the right to screen, filter, and/or monitor any information you
                            supply, as well as edit, refuse to distribute, or remove it. We cannot guarantee the truth,
                            integrity, or quality of content because we do not have the ability to manage or actively
                            monitor it. You may be exposed to content that you find offensive or objectionable because
                            community standards vary and individuals sometimes choose not to comply with our policies
                            and procedures while using our Website. You can notify us at mail@rangin.com.np if you find
                            any content inappropriate. </li>
                        <li class="mt-4">Any information you give must be personal and pertain solely to you. You hereby represent
                            and warrant that the information you submit and/or disseminate will not infringe on any
                            third party’s intellectual property, privacy, or other rights, and will not contain anything
                            that is libelous, defamatory, obscene, seditious, indecent, harassing, or threatening.
                            Rangin Technology reserves the right to screen, filter, and/or monitor any information you
                            supply, and to edit, refuse to disseminate, or remove it if necessary.</li>
                        <li class="mt-4">By accepting these terms of service, you also agree to hold Rangin Technology and its
                            parent, subsidiaries, affiliates, directors, agents, and employees harmless from any civil
                            or criminal claims arising from any posting attributed to your Subscriber identity and
                            arising out of your violation of the Terms of Service and policies or documents they
                            incorporate by reference, or your violation of any law, rule, or regulation. You further
                            acknowledge that, as more extensively stated in these Terms of Use, Rangin Technology is not
                            responsible for any objectionable material.
                        </li>
                        <li class="mt-4">Rangin Technology cannot be held liable for any error, omission, interruption, deletion,
                            defect, delay in operation or transmission, communications line failure, theft, destruction,
                            alteration, or unauthorized access to entries, or entries lost or delayed, whether or not
                            caused by server functions, virus, worms, or other causes beyond its control.</li>
                    </ul>
                </div>
            </div>

        </div>
        <!-- scroll up button -->
        <div class="scroll">
            <button class="scroll-btn">
                <i class="fa-solid fa-chevron-up"></i>
            </button>
        </div>
        <!-- scroll up button -->
    </section>
    <!--  section Footer -->
    <x-footer :courses="$courses" />
    @slot('footerSeo')
        @if (empty($seofooter))
        @else
            @foreach ($seofooter as $footer)
                {!! $footer->code !!}
            @endforeach
        @endif
    @endslot
</x-homelayout>
