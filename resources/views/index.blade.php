<x-homelayout>
    @slot('headerSeo')
        @if (empty($seoheader))
        @else
            @foreach ($seoheader as $header)
                {!! $header->code !!}
            @endforeach
        @endif
    @endslot
    <x-sweetalert />
    <x-navbar :webtypes="$webtypes" :designtypes="$designtypes" :marketingtypes="$marketingtypes" :courses="$courses" :applicationtypes="$applicationtypes"
        :offers="$offers" />
    <!-- Search section -->
    <x-searchbar />
    <!-- Search section -->
    <!-- section body-content -->
    <section id="body-content">
        <div class="body-content">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-7 left-body">
                    <div id="carousel" class="carousel slide" data-ride="carousel" data-aos="fade-right">
                        <div class="carousel-inner">
                            @foreach ($courses as $index => $course)
                                <div class="carousel-item {{ $index === 0 ? 'active' : '' }}">
                                    <div class="left-carousel">
                                        <p class="upper-text">Get trained in</p>
                                        <h1><b>{{ $course->name }}</b></h1>
                                        <p class="description">{{ $course->description }}
                                        </p>
                                        <div class="enroll-section">
                                            <a href="/programs/{{ $course->slug }}" class="text-decoration-none">
                                                <button class="enroll-btn text-uppercase">Enroll Now</button></a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div id="dot">
                            <span class="dot" class="carousel-control-prev" type="button" data-target="#carousel"
                                data-slide="prev"></span>
                            <span class="dot circle" class="carousel-control-next" type="button"
                                data-target="#carousel" data-slide="next"></span>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-lg-5 right-body">
                    <div class="right-banner" data-aos="fade-left" data-aos-anchor-placement="top-bottom">
                        <h5 class="upper-text">Digital Agency</h5>
                        <h1 class="drop-in">{{ $indextagline->title ?? '' }}</h1>
                        <p class="description drop-in-2">We bring ideas into reality through design, development &
                            digital marketing</p>
                        <div class="start-project">
                            <button class="start-btn" id="start-btn">Start a Project</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- scroll up button -->
            <div class="scroll">
                <button class="scroll-btn">
                    <i class="fa-solid fa-chevron-up"></i>
                </button>
            </div>
            <!-- scroll up button -->
        </div>
    </section>
    <!-- section body-content -->
    <!-- section sidebar -->
    <section id="sidebar">
        <div class="sidebar">
            <div class="row enquiry-form d-flex flex-row-reverse">
                <div class="col-12 col-md-12 col-lg-6 left-section pb-5">
                    <div class="left-content mt-5">
                        <div class="header pl-5 d-flex justify-content-between align-items-center">
                            <img src="assets/images/logo.png" alt="company-logo" width="200">
                            <button class="close-btn mr-3" id="close-btn"><i
                                    class="fa-solid fa-circle-xmark"></i></button>
                        </div>
                        <form action="{{ route('enquiry.submit') }}" method="POST" id="enquiryForm" class="mt-5 px-4">
                            @csrf
                            <div class="input-field">
                                <input class="w-100" type="text" name="name" id="full_name"
                                    placeholder="Full Name*" />
                            </div>
                            <div class="input-field">
                                <input class="w-100" type="text" name="email" id="email_address"
                                    placeholder="Email*" />
                            </div>
                            <div class="input-field">
                                <input class="w-100" type="text" name="phone" id="phone_number"
                                    placeholder="Mobile/Phone*" />
                                <input type="hidden" name="type" id="type" value="business" />
                            </div>
                            <div class="input-field">
                                <input class="w-100" type="text" name="country" id="country"
                                    placeholder="Address*" />
                            </div>
                            <div class="input-field">
                                <textarea class="w-100 position-relative" name="description" id="description" rows="5"
                                    placeholder="Explain the project you want*"></textarea>
                                <p id="charCount" class="text-black d-flex justify-content-end">Max character 500
                                </p>
                            </div>
                            <div class="form-check mt-3">
                                <input type="checkbox" class="form-check-input mt-1" id="terms_condition">
                                <label class="form-check-label terms text-black" for="terms_condition">I have read
                                    Privacy
                                    Policy
                                    and
                                    agree
                                    to
                                    the
                                    terms and conditions.*</label>
                            </div>
                            <button type="submit" class="submit-btn w-100">Submit</button>
                        </form>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-lg-6 right-section"
                    style="background: url({{ $indexBanner ? asset('storage/' . $indexBanner->image) : '' }}) no-repeat;
                    background-position: center;
                    background-size: cover;">
                </div>
            </div>
            <input type="hidden" value="Website Visitor">
        </div>
    </section>
    <!-- section sidebar -->
    <!-- section services -->
    <section id="services">
        <div class="services">
            <div class="row px-4">
                <div
                    class="col-12 col-sm-12 col-lg-3 single-col shadows d-flex py-3 justify-content-center align-items-center">
                    <h1 class="title  text-center" data-aos="slide-up" data-aos-anchor-placement="top-bottom"><b>One
                            Stop <span class="span-text d-block">All
                                Done</span></b>
                    </h1>
                </div>
                <div class="col-12 col-sm-12 col-lg-9 multi-col">
                    <div class="row">
                        <div class="col-6 col-sm-6 col-lg-4 cols shadows px-4 py-3">
                            <a href="#">
                                <h3 class="service-title" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                                    Websites</h3>
                                <p class="service-description" data-aos="slide-up"
                                    data-aos-anchor-placement="top-bottom">Let us build a perfect website for your
                                    business.
                                </p>
                            </a>
                        </div>
                        <div class="col-6 col-sm-6 col-lg-4 cols shadows  px-4 py-3 ">
                            <a href="#">
                                <h3 class="service-title" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                                    Web Apps</h3>
                                <p class="service-description" data-aos="slide-up"
                                    data-aos-anchor-placement="top-bottom">Bring your ideas into reality. We can help
                                    you
                                    make it happen.</p>
                            </a>
                        </div>
                        <div class="col-6 col-sm-6 col-lg-4 cols shadows  px-4 py-3 ">
                            <a href="#">
                                <h3 class="service-title" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                                    Mobile App</h3>
                                <p class="service-description" data-aos="slide-up"
                                    data-aos-anchor-placement="top-bottom">Bring your ideas into reality. We can help
                                    you
                                    make it happen.</p>
                            </a>
                        </div>
                        <div class="col-6 col-sm-6 col-lg-4 cols shadows px-4 py-3">
                            <a href="#">
                                <h3 class="service-title" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                                    Marketing</h3>
                                <p class="service-description" data-aos="slide-up"
                                    data-aos-anchor-placement="top-bottom">All in one solution for digital maketing
                                    service.
                                </p>
                            </a>
                        </div>
                        <div class="col-6 col-sm-6 col-lg-4 cols shadows px-4 py-3 ">
                            <a href="#">
                                <h3 class="service-title" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                                    Training </h3>
                                <p class="service-description" data-aos="slide-up"
                                    data-aos-anchor-placement="top-bottom">Become a professional IT Experts with our
                                    comprehensive courses.
                                </p>
                            </a>
                        </div>
                        <div class="col-6 col-sm-6 col-lg-4 cols shadows px-4 py-3">
                            <a href="#">
                                <h3 class="service-title" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                                    Support </h3>
                                <p class="service-description" data-aos="slide-up"
                                    data-aos-anchor-placement="top-bottom">Get a pro IT support from industry pro
                                    experts.
                                </p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- section services -->
    <!-- section start-project -->
    @if (!empty($indexLowerContent))
        <section id="start-project">
            <div class="start-project mt-4">
                <div class="row">
                    <div class="col-12 col-lg-6 left-section">
                        <div class="left-content p-5">
                            <h1 class="pr-3" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                                <b>{{ $indexLowerContent->title }} </b>
                            </h1>
                            <h5 class="mt-4 pr-5" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                                {{ $indexLowerContent->description }}</h5>
                            <div class="start-project" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                                <button class="start-btn" id="start-btn-2">Start a Project</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 right-section p-0"
                        style="background: url({{ asset('storage/' . ($indexLowerContent ? $indexLowerContent->image : '')) }}) no-repeat;
                    background-position: center;
                    background-size: cover;">
                    </div>
                </div>
            </div>
        </section>
    @else
    @endif
    <!-- section start-project -->
    <!-- section courses -->
    <x-course :courses="$courses" />
    <!-- section courses -->
    <!-- section contact -->
    <section id="contact">
        <div class="contact index">
            <div class="heading text-center" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                <h1><b>Need help creating your website?</b></h1>
                <p class="mt-2 mb-5">Go pro services and support from our team.</p>
            </div>
            <div class="form mb-5" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                <form action="{{ route('message.submit') }}" id="contactForm" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-auto mb-4">
                            <input type="text" placeholder="Full Name*" class="form-input" name="name"
                                id="fname">
                        </div>
                        <div class="col-auto mb-4">
                            <input type="text" placeholder="Mobile*" class="form-input" name="phone"
                                id="phone_no">

                        </div>
                        <div class="col-auto mb-4">
                            <input type="text" placeholder="Email*" class="form-input" name="email"
                                id="user_email">
                            <input type="hidden" name="type" id="type" value="visitor">

                        </div>
                        <div class="col-auto sumit-btn">
                            <button class="submit" type="submit">Let's Talk</button>
                        </div>
                    </div>
                </form>
            </div>
            <h5 class="bottom-text text-center" data-aos="slide-up" data-aos-anchor-placement="top-bottom">Fill out
                our contact form, and we’ll give you a call.</h5>
        </div>
    </section>
    <!-- section contact -->
    <!-- section testimonials -->
    <section id="testimonials">
        <div class="testimonials">
            <div class="heading">
                <p class="text-center" data-aos="slide-up" data-aos-anchor-placement="top-bottom">Testimonials</p>
                <h1 class="text-center" data-aos="slide-up" data-aos-anchor-placement="top-bottom">But don't take our
                    words for it</h1>
            </div>
            <div id="carousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                    @foreach ($testimonials as $index => $testimonial)
                        <div class="carousel-item {{ $index === 0 ? 'active' : '' }}">
                            <div class="content d-flex justify-content-between mt-3">
                                <div class="left-body mr-5">
                                    <img src="{{ asset('storage/' . $testimonial->image) }}" width="300"
                                        alt="testimonials">
                                </div>
                                <div class="right-body">
                                    <div class="description">
                                        <h5>"{{ $testimonial->description }}"
                                        </h5>
                                    </div>
                                    <div class="name mt-4">
                                        <h5><b>{{ $testimonial->name }}</b></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
    </section>
    <!-- section testimonials -->
    <!-- section stories -->
    <x-story :stories="$stories" />
    <!-- section stories -->
    <!-- section FAQs -->
    <section class="faq-section">
        <div class="fix-wrap">
            <div class="section-header">
                <p class="section-title" data-aos="slide-up" data-aos-anchor-placement="top-bottom">FAQs</p>
                <h1 class="mt-3" data-aos="slide-up" data-aos-anchor-placement="top-bottom">Our Frequently Asked
                    Queries</h1>
            </div>
            <div class="faq-accordions my-5">
                @if (empty($faqs))
                    <div class="pb-5 px-3">
                        <h5 data-aos="slide-up" data-aos-anchor-placement="top-bottom">Frequently asked question not
                            found </h5>
                    </div>
                @else
                    @foreach ($faqs as $faq)
                        <div class="accordion-row " data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                            <div class="title">
                                <h5>{{ $faq->title }}</h5>
                            </div>
                            <div class="content">
                                <p>{{ $faq->description }}</p>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
    <!-- section FAQs -->
    <!--  section Footer -->
    <x-footer :courses="$courses" />
    @slot('footerSeo')
        @if (empty($seofooter))
        @else
            @foreach ($seofooter as $footer)
                {!! $footer->code !!}
            @endforeach
        @endif
    @endslot
</x-homelayout>
