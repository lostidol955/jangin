<x-homelayout>
    @slot('headerSeo')
        @if (empty($seoheader))
        @else
            @foreach ($seoheader as $header)
                {!! $header->code !!}
            @endforeach
        @endif
    @endslot
    <x-sweetalert />
    <x-navbar :webtypes="$webtypes" :designtypes="$designtypes" :marketingtypes="$marketingtypes" :courses="$courses" :applicationtypes="$applicationtypes"
        :offers="$offers" />
    <!-- Overview section -->
    <section id="overview-body">
        <div class="overview-body marketing mt-3">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-6 left-section mt-5">
                    <div class="left-content pt-5 pb-4 pl-5" data-aos="fade-right" data-aos-anchor-placement="top-bottom">
                        <h5 class="pr-3 text-white">Overview</h5>
                        <h1 class="mt-4 mb-5  text-white">
                            {{ $marketingtagline->title ?? '' }}
                        </h1>
                        <div class="number">
                            <p class="text-white">Call us today at <span
                                    class="tel d-block text-black">{{ $contactInfo->mobile_no ?? '' }}</span></p>
                        </div>
                    </div>
                </div>
                @if (!empty($marketingtagline))
                    <div class="col-12 col-md-12 col-lg-6 right-section p-0" data-aos="fade-left"
                        data-aos-anchor-placement="top-bottom"
                        style="background: url({{ asset('storage/' . ($marketingtagline ? $marketingtagline->image : '')) }}) no-repeat;
                        background-position: center;
                        background-size: cover;">
                    </div>
                @else
                @endif
            </div>
            <!-- scroll up button -->
            <div class="scroll">
                <button class="scroll-btn">
                    <i class="fa-solid fa-chevron-up"></i>
                </button>
            </div>
            <!-- scroll up button -->
        </div>
    </section>
    <!-- Overview section -->
    <!-- Business need section -->
    <section id="business">
        <div class="business marketing-overview bg-white mt-5">
            <div class="row" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                @if (!empty($marketingcontent))
                    <div class="col-12 col-md-12 col-lg-5 right-body">
                        <img src="{{ asset('storage/' . ($marketingcontent ? $marketingcontent->image : '')) }}"
                            class="w-100" alt="marketing">
                    </div>
                @else
                @endif
                <div class="col-12 col-md-12 col-lg-7 left-section">
                    <div class="left-content">
                        <h1 class="pr-3 revealX font-weight-bold">{{ $marketingcontent->title ?? '' }}</h1>
                        <div class="list-items mt-4">
                            @if (!empty($marketingcontent->description))
                                @foreach (preg_split("/\r\n|\n|\r/", $marketingcontent->description) as $points)
                                    <p class="lists"><img src="assets/images/tick.png" width="30" class="mr-2"
                                            alt="tick">
                                        {{ trim($points) }}
                                    </p>
                                @endforeach
                            @else
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Business need section -->
    <!-- WebTypes section -->
    <section id="webtypes">
        <div class="webtypes graphic-service">
            <div class="row">
                @if (!empty($marketingtypes))
                    @foreach ($marketingtypes as $marketingtype)
                        <div class="col-12 col-md-12 col-lg-6 columns" data-aos="slide-up"
                            data-aos-anchor-placement="top-bottom">
                            <h1 class="title ">{{ $marketingtype->name }}</h1>
                            <div class="content">
                                <p class="description mt-4">{!! strip_tags($marketingtype->description) !!}</p>
                            </div>
                            <div class="learn mt-4">
                                <a href="/service/{{ $marketingtype->slug }}">
                                    <button class="learn-btn">Learn More</button>
                                </a>
                            </div>
                        </div>
                    @endforeach
                @else
                @endif
            </div>
        </div>
    </section>
    <!-- WebTypes section -->
    {{-- Marketing lower content --}}
    @if (!empty($marketingLowerContent))
        <section id="business">
            <div class="business web mt-4">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-5 left-section">
                        <div class="left-content">
                            <h1 class="pr-3 text-white revealX">{{ $marketingLowerContent->title ?? '' }}</h1>
                            <p class="my-4 description text-white revealX">
                                {{ $marketingLowerContent->description ?? '' }}</p>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-7 right-section"
                        style="background: url({{ asset('storage/' . ($marketingLowerContent ? $marketingLowerContent->image : '')) }}) no-repeat;
                        background-position: center;
                        background-size: cover;">
                    </div>
                </div>
            </div>
        </section>
    @else
    @endif
    {{-- Marketing lower content --}}
    <!-- section contact -->
    <section id="contact">
        <div class="contact bg-white">
            <div class="heading text-center" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                <h1><b>Need help creating your website?</b></h1>
                <p class="mt-2 mb-5">Go pro services and support from our team.</p>
            </div>
            <div class="form mb-5" data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                <form action="{{ route('message.submit') }}" id="contactForm" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-auto mb-4">
                            <input type="text" placeholder="Full Name*" class="form-input" name="name"
                                id="fname">
                        </div>
                        <div class="col-auto mb-4">
                            <input type="text" placeholder="Mobile*" class="form-input" name="phone"
                                id="phone_no">
                        </div>
                        <div class="col-auto mb-4">
                            <input type="text" placeholder="Email*" class="form-input" name="email"
                                id="user_email">
                            <input type="hidden" name="type" id="type" value="business">
                        </div>
                        <div class="col-auto sumit-btn">
                            <button class="submit" type="submit">Let's Talk</button>
                        </div>
                    </div>
                </form>
            </div>
            <h5 class="bottom-text text-center" data-aos="slide-up" data-aos-anchor-placement="top-bottom">Fill out our
                contact form, and we’ll give you a call.</h5>
        </div>
    </section>
    <!-- section contact -->
    <!-- section stories -->
    <x-story :stories="$stories" />
    <!-- section stories -->
    <!--  section Footer -->
    <x-footer :courses="$courses" />
    @slot('footerSeo')
        @if (empty($seofooter))
        @else
            @foreach ($seofooter as $footer)
                {!! $footer->code !!}
            @endforeach
        @endif
    @endslot
</x-homelayout>
