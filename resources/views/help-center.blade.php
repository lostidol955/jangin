<x-homelayout>
    @slot('headerSeo')
        @if (empty($seoheader))
        @else
            @foreach ($seoheader as $header)
                {!! $header->code !!}
            @endforeach
        @endif
    @endslot
    <x-navbar :webtypes="$webtypes" :designtypes="$designtypes" :marketingtypes="$marketingtypes" :courses="$courses" :applicationtypes="$applicationtypes"
        :offers="$offers" />
    <section id="body-content">
        <div class="accessibility body-content">
            <div class="title mt-4">
                <h1 class="text-center font-weight-bold" data-aos="fade-up" data-aos-anchor-placement="top-bottom">Help Center</h1>
            </div>
            <section class="faq-section pt-2">
                <div class="fix-wrap">
                    <div class="faq-accordions my-3">
                        @if (empty($faqs))
                        @else
                            @foreach ($faqs as $faq)
                                <div class="accordion-row " data-aos="slide-up" data-aos-anchor-placement="top-bottom">
                                    <div class="title">
                                        <h5>{{ $faq->title }}</h5>
                                    </div>
                                    <div class="content">
                                        <p>{{ $faq->description }}</p>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </section>

        </div>
        <!-- scroll up button -->
        <div class="scroll">
            <button class="scroll-btn">
                <i class="fa-solid fa-chevron-up"></i>
            </button>
        </div>
        <!-- scroll up button -->
    </section>
    <!--  section Footer -->
    <x-footer :courses="$courses" />
    @slot('footerSeo')
        @if (empty($seofooter))
        @else
            @foreach ($seofooter as $footer)
                {!! $footer->code !!}
            @endforeach
        @endif
    @endslot
</x-homelayout>
